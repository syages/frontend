library syages.$lowlevel_controller;

import 'dart:async';
import 'dart:html';
import 'dart:js';
import 'package:paper_elements/paper_toast.dart';
import 'package:polymer/polymer.dart';
import 'Context.dart';
import 'ContextFactory.dart';
import 'Instance.dart';
import 'Session.dart';
import 'Stack.dart';
import 'ViewController.dart';

@CustomTag("syages-app")
/// Le cœur de l'application (bas-niveau) et controlleur de l'élement syages-app
class $SyagesAppController extends PolymerElement {
	@observable String bubble = "";
	Stack<Context> _contextsHistory = new Stack<Context>(); // Historique des contextes précédants
	Stack<Context> _repushableContexts = new Stack<Context>(); // Historique des contextes suivants
	ViewController _controller = null;
	bool _ignoreHashChanges = false;
	StreamController<SyagesAppEvent> _onContextPopController, _onContextPushController, _onContextReplacementController;
	bool _userWantsNavigation = false;
	Element _view = null;

	$SyagesAppController.created() : super.created()
	{
		_onContextPopController = new StreamController<SyagesAppEvent>();
		_onContextPushController = new StreamController<SyagesAppEvent>();
		_onContextReplacementController = new StreamController<SyagesAppEvent>();
		new SyagesInstance().configure(
				[_onContextPopController.stream,
				 _onContextPushController.stream,
				 _onContextReplacementController.stream
				 ] as List<Stream<SyagesAppEvent>>);
		
		_toaster.autoCloseDisabled = true;
		_toaster.duration = 5000;
		_toaster.responsiveWidth = '767px';
		_toaster.swipeDisabled = true;
		_toaster.on["core-overlay-close-completed"].listen((a) {
			print("toast disappeared");
			if (_toastQueue.length > 0) {
				_toastQueue.removeAt(0);
			}
			_toastUpdate();
		});
		
		_actionToaster.autoCloseDisabled = true;
		_actionToaster.duration = 5000;
		_actionToaster.responsiveWidth = '767px';
		_actionToaster.swipeDisabled = true;
		_actionToaster.on["core-overlay-close-completed"].listen((a) {
			print("toast disappeared");
			if (_toastQueue.length > 0) {
				_toastQueue.removeAt(0);
			}
			_toastUpdate();
		});
	}

	ready() {
		SyagesInstance.shared.applicationController = this;
		SyagesInstance.shared.launch();
		window.onHashChange.listen((HashChangeEvent e) {
			if (_ignoreHashChanges) return;
			Uri u = Uri.parse(e.newUrl);
			if (u.fragment != currentContext.route) {
				SyagesInstance.shared.pushRoute(u.fragment);
			}
		});
	}

	// DOM helpers
    Node get containerNode => ($['container'] as Node);

	// Contexts managements
    Stack<Context> get historyStack => _contextsHistory;
    Stack<Context> get repushStack => _repushableContexts;
	Context get currentContext => _contextsHistory.length == 0 ? null : _contextsHistory.top;
	Stream<SyagesAppEvent> get onContextPopped => _onContextPopController.stream;
	Stream<SyagesAppEvent> get onContextPushed => _onContextPushController.stream;
	Stream<SyagesAppEvent> get onContextReplaced => _onContextReplacementController.stream;

	Context popContext() {
		var ctx = _contextsHistory.pop();
		_repushableContexts.push(ctx);
    	_onContextPopController != null ? _onContextPopController.add(new SyagesAppEvent(this, ctx)) : 0;
		_refreshContext();
		_refreshNavigationClass();
		return ctx;
	}

	void pushContext(Context ctx) {
		if (_controller != null && _controller.shouldSerializeState())
		{
			currentContext.state = _controller.serializedState();
		}
		_contextsHistory.push(ctx);
		_repushableContexts.clear();
    	_onContextPushController != null ? _onContextPushController.add(new SyagesAppEvent(this, ctx)) : 0;
    	_refreshContext();
    	_refreshNavigationClass();
    }

	void replaceContext(Context ctx) {
		_contextsHistory.replace(ctx);
		_repushableContexts.clear();
    	_onContextReplacementController != null ? _onContextReplacementController.add(new SyagesAppEvent(this, ctx)) : 0;
    	_refreshContext();
    	_refreshNavigationClass();
    }

	void repushContext() {
		var ctx = _repushableContexts.pop();
		if (_controller != null && _controller.shouldSerializeState())
		{
			currentContext.state = _controller.serializedState();
		}
		_contextsHistory.push(ctx);
    	_onContextPushController != null ? _onContextPushController.add(new SyagesAppEvent(this, ctx)) : 0;
    	_refreshContext();
    	_refreshNavigationClass();
	}

	void _refreshContext()
	{
	    while (containerNode.childNodes.isNotEmpty)
	    	containerNode.childNodes.first.remove();

	    if (currentContext == null) {
	    	window.location.href = "null";
	    	return;
	    }

	    if (currentContext.configuration.requireAuth && SyagesSession.sharedInstance.isConnected == false)
	    {
	    	pushContext(ContextFactory.loginThenPopContext());
	    	return;
	    }

	    _ignoreHashChanges = true;
	    window.history.go(window.history.length-1);
	    window.location.hash = currentContext.route;
	    _ignoreHashChanges = false;

	    view = document.createElement(currentContext.configuration.controller);
	    containerNode.append(view);
	}

	// Media queries
	void mediaChanged(Event e) {
		var detail = new JsObject.fromBrowserObject(e)['detail'];
		isPhone = detail['matches'];
		_refreshNavigationClass();
	}

	// Navigation bar status
	@observable bool isPhone = false;
	@observable String navigationClass = "";
	bool get isNavigationVisible
	   => shouldNavigationDisplay && ((isPhone && userWantsNavigation) || !isPhone);

	bool get shouldNavigationDisplay => currentContext.configuration.withNavbar;

	bool get userWantsNavigation => _userWantsNavigation;
	void set userWantsNavigation(bool val) {
		_userWantsNavigation = val;
		_refreshNavigationClass();
	}
	void userToggledNavigation() { userWantsNavigation = !userWantsNavigation; }

	void _refreshNavigationClass() {
		navigationClass = isNavigationVisible ? (isPhone?"mobile navEnabled":"navEnabled") : (isPhone&&shouldNavigationDisplay ?"mobile":"");
	}

	// Current controller and view
	ViewController get controller => _controller;
	void set controller(ViewController controller) {
		_controller = controller;
		_controller.deserializeState(currentContext.state);
	}
	Element get view => _view;
	void set view(Element el) {
		_view = el;
	}
	
	// Toasts
	@observable String toastActionLabel = null;
	PaperToast get _actionToaster => this.$["actionToaster"];
	PaperToast get _toaster => this.$["toaster"];
	List<Toast> _toastQueue = new List<Toast>();
	
	porterUnToast(Toast t) {
		if (t == null) return;
		_toastQueue.add(t);
		_toastUpdate();
	}
	
	tchin() {
		if (_toastQueue.length == 0) return;
		Toast currentToast = _toastQueue.first;
		if (currentToast.runtimeType != ActionToast) return;
		ActionToast currentActionToast = currentToast;
		
		_toaster.dismiss();
		
		if (currentActionToast.action == null) return;
		currentActionToast.action();
	}
	
	_toastUpdate() {
		if (_toastQueue.length == 0) {
			_toaster.dismiss();
			return;
		}
		Toast currentToast = _toastQueue.first;
		
		if (currentToast.runtimeType == ActionToast) {
			toastActionLabel = (currentToast as ActionToast).actionLabel;
			_actionToaster.text = currentToast.message;
			if (_toaster.opened == false && _actionToaster.opened == false) _actionToaster.show();
			return;
		}
		
		_toaster.text = currentToast.message;
		if (_toaster.opened == false && _actionToaster.opened == false) _toaster.show();
	}
}

class Toast {
	final String message;
	
	Toast(String message) : this.message = message != null ? message : "";
}

class ActionToast extends Toast {
	final String actionLabel;
	final Function action;
	
	ActionToast(String message, String actionLabel, Function action) : super(message),
	this.actionLabel = actionLabel,
	this.action = action;
}

class SyagesAppEvent
{
	final $SyagesAppController appController;
	final Context context;

	SyagesAppEvent($SyagesAppController appController, Context context) :
		this.appController = appController,
		this.context = context;
}