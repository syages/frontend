library syages.ExprFilters;

import 'dart:convert';
import "i18n.dart";
import 'package:polymer_expressions/filter.dart' show Transformer;


abstract class filters {
	static get date => () => (DateTime dt) {
        	return '${i18n.WEEKDAYS[dt.weekday-1]} ${dt.day} ${i18n.MONTHS[dt.month-1]} ${dt.year}';
        };
	
	static get datetime => () => (DateTime dt) {
    	var hour = dt.hour < 10 ? "0${dt.hour}" : dt.hour.toString();
    	var minute = dt.minute < 10 ? "0${dt.minute}" : dt.minute.toString();

    	return '${i18n.WEEKDAYS[dt.weekday-1]} ${dt.day} ${i18n.MONTHS[dt.month-1]} ${dt.year} à ${hour}:${minute}';
    };
    
    static get time => () => (DateTime dt) {
        	var hour = dt.hour < 10 ? "0${dt.hour}" : dt.hour.toString();
        	var minute = dt.minute < 10 ? "0${dt.minute}" : dt.minute.toString();

        	return '${hour}:${minute}';
        };

    static get htmlentities => () => (String string) => new HtmlEscape().convert(string);
    
    static get average => () => (double d) {
    	return d.toStringAsFixed(2);
    };
    
}

class StringToInt extends Transformer<String, int> {
  String forward(int i) => '$i';
  int reverse(String s) => int.parse(s);
}

class StringToDouble extends Transformer<String, double> {
  String forward(double i) => '$i';
  double reverse(String s) => s.isEmpty ? 0.0 : double.parse(s);
}