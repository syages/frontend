library syages.Router;

import 'dart:html';
import 'Helpers.dart';
import 'Routes.dart';
import 'Session.dart';

class RouteParameter {
	static final List<String> PARAM_TYPES = ["double", "integer", "string"];
	static const int PARAM_TYPE_DOUBLE = 0;
	static const int PARAM_TYPE_INTEGER = 1;
	static const int PARAM_TYPE_STRING = 2;

	String name;
	String type;

	RouteParameter(String name, [String type = null]) :
		name = name,
		type = type == null || !PARAM_TYPES.contains(type) ? PARAM_TYPES[PARAM_TYPE_STRING] : type;
}

class RouteConfiguration {
	final String controller;
	final RegExp matchingExpression;
	final Map<int, RouteParameter> paramCaptures;
	final bool requireAuth;
	final bool withNavbar;

	RouteConfiguration(String regexp, String controller, {Map<int, RouteParameter> paramCaptures: null, bool requireAuth: false, bool withNavbar: false}) :
		controller = controller,
		matchingExpression = new RegExp(regexp),
		paramCaptures = paramCaptures,
		requireAuth = requireAuth,
		withNavbar = withNavbar;

	bool operator==(RouteConfiguration conf) {
		if (conf == null) return false;
		if (this.controller != conf.controller) return false;
		if (this.matchingExpression != conf.matchingExpression) return false;
		if (this.paramCaptures != conf.paramCaptures) return false;
		if (this.requireAuth != conf.requireAuth) return false;
		if (this.withNavbar != conf.withNavbar) return false;
		return true;
	}

	String toString() {
		return (this.requireAuth ? "Secured c" : "C") + "ontroller " + this.controller +
				" with" + (this.withNavbar ? "" : "out") + " navigation bar.";
	}
}

abstract class SyagesRouter {
	static final List<RouteConfiguration> routes = [
		Routes.contactDirectory, Routes.contextTester, Routes.dashboard, Routes.newsFeed, Routes.profileDetails, Routes.profileSettings, Routes.forgotPassword, Routes.forgotPasswordForm, Routes.signupForm,

		// Administrateurs
		Routes.a_bugReports,
		Routes.a_bugReportDetail,
		Routes.aps_administratorForm,
		Routes.aps_administrators,
		Routes.aps_courseForm,
		Routes.aps_courses,
		Routes.aps_diplomas,
		Routes.aps_diplomaForm,
		Routes.aps_domainForm,
		Routes.aps_domains,
		Routes.aps_internForm,
		Routes.aps_interns,
		Routes.aps_newsForm,
		Routes.aps_presidentForm,
		Routes.aps_presidents,
		Routes.aps_staffForm,
		Routes.aps_staffs,
		Routes.aps_teacherForm,
		Routes.aps_teachers,
		Routes.apst_evaluationForm,
		Routes.apst_lectureForm,

		// President
		Routes.ps_diplomas,

		// Staff members

		// Teachers
		Routes.t_lectures,
		Routes.t_lessons,
		Routes.t_classroom,

		// Intern
		Routes.i_absences,
		Routes.i_firstLogin,
		Routes.i_marks
	];
	static final RouteConfiguration notFoundConfiguration = Routes.notFound;
	static final RouteConfiguration notFoundConfigurationWhenLogged = Routes.notFoundWhenLogged;

	static RouteConfiguration configurationForRoute(String path)
	{
		path = _strippedDownUri(path);
		if (path.trim().isEmpty == true) return Routes.root;

		for (int i = 0; i < routes.length; i++)
		{
			var testedRoute = routes[i];
            if (testedRoute != null && testedRoute.matchingExpression.hasMatch(path)) {
            	return testedRoute;
            }
		}

		return SyagesSession.sharedInstance.isConnected ? notFoundConfigurationWhenLogged : notFoundConfiguration;
	}

	static Map<String, dynamic> captureParameters(RouteConfiguration conf, String uri) {
		Map<String, dynamic> params = new Map<String, dynamic>();
		if (conf.paramCaptures == null || conf.paramCaptures.isEmpty) return params;
		uri = _strippedDownUri(uri);

		Iterable<Match> matches = conf.matchingExpression.allMatches(uri);
		if (matches.length == 0)
		{
			return params;
		}
		Match match = matches.elementAt(0);
		conf.paramCaptures.forEach((int captureGroup, RouteParameter paramMetadata) {
			if (match.groupCount <= captureGroup) {
				warn("${conf.matchingExpression.pattern} has ${match.groupCount} capture group(s), can't set parameter ${paramMetadata.name}:${paramMetadata.type} from capture group $captureGroup");
				return;
			}
			String value = match.group(captureGroup+1);

			switch(RouteParameter.PARAM_TYPES.indexOf(paramMetadata.type)) {
				case RouteParameter.PARAM_TYPE_DOUBLE:
					params = _mapWithParam(params, paramMetadata.name, double.parse(value));
					break;
				case RouteParameter.PARAM_TYPE_INTEGER:
					params = _mapWithParam(params, paramMetadata.name, int.parse(value));
					break;
				default: {
					params = _mapWithParam(params, paramMetadata.name, value);
				}
			}
		});

		return params;
	}

	static RouteConfiguration configurationForCurrentRoute() => configurationForRoute(window.location.hash);

	static Map<String, dynamic> _mapWithParam(Map<String, dynamic> map, String key, dynamic value) {
		List<String> path = key.split(".");
		if (path.length > 1) {
			String subKey = path[0];
			path.removeAt(0);
			if (map.containsKey(subKey) == false)
				map[subKey] = new Map<String, dynamic>();
			map[subKey] = _mapWithParam(map[subKey], path.join("."), value);
		} else {
			map[key] = value;
		}
		return map;
	}

	static String _strippedDownUri(String uri) {
		if (uri.startsWith("#")) uri = uri.substring(1, uri.length);
		if (uri.startsWith("!")) uri = uri.substring(1, uri.length);
		if (uri.startsWith("/")) uri = uri.substring(1, uri.length);
		return uri;
    }
}