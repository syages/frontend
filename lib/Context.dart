library syages.Context;

import 'dart:html';
import 'Helpers.dart';
import 'Router.dart';

class Context {
	final RouteConfiguration _conf;
	final String _route;
	Map<String, dynamic> _state;

	Context(RouteConfiguration config, String route, [Map<String, dynamic> state]) :
	_conf = config,
	_route = route == null ? "" : route,
	_state = state == null ? new Map<String, dynamic>() : state
	{
		if (config == null)
			warn("A Context must have a valid configuration!");
	}

	Context.forCurrentRoute([Map<String, dynamic> state = null])
      : this(SyagesRouter.configurationForCurrentRoute(), window.location.hash, state);

	RouteConfiguration get configuration => _conf;
    String get route => _route;
	Map<String, dynamic> get state => _state;
	void set state(Map<String, dynamic> s) { _state = s; }
}