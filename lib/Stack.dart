library syages.Stack;

class Stack<T> {
	final int _capacity = -1;
	List<T> _container;

	Stack() : _container = new List<T>();

	List<T> get asList => _container;
	int get capacity => _capacity;
	bool get isEmpty => _container.isEmpty;
	int get length => _container.length;
	T get top => _container.first;

	clear() {
		_container.clear();
	}

	push(T val) {
		if (_container.length == _capacity && _capacity > 1)
			_container.removeAt(_capacity-1);
		_container.insert(0, val);
	}

	pop() {
		T keep = _container.first;
		_container.removeAt(0);
		return keep;
	}

	replace(T val) {
		if (!isEmpty) _container[0] = val;
		else push(val);
	}
}