library syages.SerializableContext;

import 'package:polymer/polymer.dart';
import 'Helpers.dart';

/// Interface pour la sérialisation des contextes
///
///
abstract class SerializableElement extends PolymerElement {
	@observable SerializableElement controller;

	SerializableElement.created() : super.created()
	{
		controller = this;
	}
	
	errorHandler(e) { warn (e); }

	void deserializeState(Map<String, dynamic> state);
	Map<String, dynamic> serializedState();
	bool shouldSerializeState() { return true; }
}