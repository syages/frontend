@documented("f0e21a2e")
library syages.Helpers;

import "dart:convert";
import "dart:html";
import "Annotations.dart";
import "Instance.dart";
import "Session.dart";

/// Permet d'ajouter les couples clé/valeur d'une Map en en-tête d'une requete HTTP
HttpRequest addHeadersToRequest(HttpRequest req, Map<String, String> headers) {
	headers.forEach((String k, String v) {
		req.setRequestHeader(k, v);
	});
	return req;
}

/// Permet de récuperer des informations sur la machine de l'utilisateur
///
/// Cette fonction permet de récupérer des informations sur la machine de
/// l'utilisateur de manière complètement anonyme. Ces informations peuvent
/// permettre au support de résoudre les bugs plus rapidement.
Map<String, dynamic> gatherUserConfiguration() => {
	"navigator": window.navigator.userAgent,
	"screen": {
		"height": window.innerHeight,
		"ratio": window.devicePixelRatio,
		"width": window.innerWidth
	}
} as Map<String, dynamic>;

/// Sérialise un objet au format JSON
String toJsonString(dynamic object, {bool pretty: true}) {
	return pretty ? new JsonEncoder.withIndent("\t").convert(object) : JSON.encode(object);
}

/// Génère les en-têtes commun à la grande majorité des requêtes
Map<String, String> requestHeaders([String contentType = "application/json", SyagesSession session = null]) {
	Map<String, String> map = new Map<String, String>();

	if (contentType != null)
		map.putIfAbsent("Content-Type", () => contentType);

	session = session == null ? SyagesSession.sharedInstance : session;
	if (SyagesSession.sharedInstance.token != null)
		map.putIfAbsent("Authorization", () => session.token);

	return map;
}

/// Affiche une information dans la console si le mode debug est activé
///
/// @see SyagesInstance#debugMode
void warn(dynamic msg) {
	if (SyagesInstance.shared.debugMode) print("Warning: $msg");
}