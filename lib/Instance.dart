@documented("f0e21a2e")
library syages.Instance;

import "dart:async";
import "dart:html";
import 'Annotations.dart';
import 'Context.dart';
import 'ContextFactory.dart';
import 'Exceptions.dart';
import 'Router.dart';
import 'syages_app.dart';
import 'Session.dart';
import 'Stack.dart';

/// Le cœur de l'application (haut-niveau)
class SyagesInstance {
	/// UTILISEZ CETTE METHODE POUR FAIRE VOS TESTS. NE METTEZ PAS LE BORDEL PARTOUT.
	///
	/// SI VOUS AVEZ DES QUESTIONS SUR L'ARCHITECTURE DU CODE
	/// ÉXISTANT OU SUR LA MANIÈRE DONT VOUS DEVEZ IMPLÉMENTER UNE FONCTION,
   	/// DEMANDEZ MOI ;) MERCI D'AVANCE, AURÉLIEN
	@debugOnly() void iWannaTestSomething() {
	
		SyagesSession.sharedInstance.onLogin.listen((sess)
		{
			
		});
	}

	/// Spécifie le nom d'hote/adresse IP à utiliser pour contacter l'API
	@config() final String apiHost = "syages.ovh";
	/// Spécifie le port sur lequel l'API attend les requêtes
    @config() final int apiPort = 443;
    /// Contrôle l'affichage de message de debug dans la console
	@config() final bool debugMode = true;

    /// Le cœur de l'application (bas-niveau) avec lequel cette instance interagi
	$SyagesAppController _applicationController = null;

	/// Controlleur de flux utilisé lors d'un changement de contexte (aggrégateur de onContextPop/Push/Replacement)
	StreamController<SyagesAppEvent> _onContextChangeController;
	/// Controlleur de flux utilisé lorsque le contexte courant est éjecté de la pile
	StreamController<SyagesAppEvent> _onContextPopController;
	/// Controlleur de flux utilisé lorsqu'un nouveau contexte est ajouté à la pile
	StreamController<SyagesAppEvent> _onContextPushController;
	/// Controlleur de flux utilisé lorsque le contexte courant est remplacé dans la pile par un nouveau contexte
	StreamController<SyagesAppEvent> _onContextReplacementController;

	/// Variable privée contenant l'unique instance de cette classe
	static SyagesInstance _sharedInstance = new SyagesInstance._newSharedInstance();
	/// Constructeur retournant l'unique instance de cette classe (`new SyagesInstance() == new SyagesInstance()`)
	factory SyagesInstance() => _sharedInstance;
	/// Getter statique pour récupérer l'instance unique
	static SyagesInstance get shared => _sharedInstance;

	/// Constructeur privé utiliser pour instancier le singleton
	SyagesInstance._newSharedInstance()
	{
		// Création des controlleurs de flux en mode diffusion
		_onContextChangeController = new StreamController<SyagesAppEvent>.broadcast();
		_onContextPopController = new StreamController<SyagesAppEvent>.broadcast();
		_onContextPushController = new StreamController<SyagesAppEvent>.broadcast();
        _onContextReplacementController = new StreamController<SyagesAppEvent>.broadcast();
	}

	// Helper methods
	/// Génère l'URL complète pointant vers l'`endpoint` spécifié de l'API
	String apiUrl(String endpoint) {
		if (endpoint == null) return this.apiBaseUrl() + '/api/1';
		return endpoint.startsWith("/") ? this.apiBaseUrl() + '/api/1$endpoint' : this.apiBaseUrl() + '/api/1/$endpoint';
	}

	/// Génère l'URL complète pointant vers la resource spécifiée
	String resourceUrl(String resource) {
		if (resource == null) return this.baseUrl();
		String url = this.baseUrl();
		if (resource.startsWith("/") == false)
			url += "/";
		url += resource;
		return url;
	}

	/// Génère l'URL de base pointant vers l'API
	String apiBaseUrl() => "https://" + this.apiHost + (this.apiPort != 443 ? (":"+this.apiPort.toString()) : "");
	/// Génère l'URL de base pointant vers le domaine courant
	String baseUrl() => window.location.protocol + "//"
			+ window.location.hostname
			+ (window.location.port == null || window.location.port.isEmpty ? '' : window.location.port);

	// Application lifecycle
	/// Configure la surcouche du cœur de l'application
	///
	/// Cette fonction est appellée automatiquement au bootstrap, elle ne doit
	/// pas être appellée manuellement.
	@doNotCall() void configure(List<Stream<SyagesAppEvent>> listenables)
	{
		// Configure le(s) stream(s) d'écoute des changements de contextes
		if (listenables.length < 3)
			throw new RuntimeException("SyagesAppController passed less than 3 listenables items. Can't run the application");

		listenables[0].listen((SyagesAppEvent ev) { _onContextChangeController.add(ev); _onContextPopController.add(ev); });
		listenables[1].listen((SyagesAppEvent ev) { _onContextChangeController.add(ev); _onContextPushController.add(ev); });
		listenables[2].listen((SyagesAppEvent ev) { _onContextChangeController.add(ev); _onContextReplacementController.add(ev); });

		// Configure le(s) stream(s) d'écoute des changement de session
		SyagesSession.sharedInstance.onLogout.listen((SyagesSession session)
        {
        	new SyagesInstance().replaceContext(ContextFactory.logout());
        });
	}

	/// Point d'entrée haut-niveau
	///
	/// Cette fonction est appellée automatiquement par la couche inférieure
	/// lorsque le bootstraping est fini. Elle ne doit pas être appellée
	/// manuellement.
	@doNotCall() void launch()
	{
		iWannaTestSomething();
	}

	// Interface to low-level code
	/// Recupère l'instance de la couche inférieure du cœur de l'application
	$SyagesAppController get applicationController => _applicationController;

	/// Modifie l'instance de la couche inférieure du cœur de l'application
	@doNotCall() void set applicationController($SyagesAppController ac) {
		$SyagesAppController old = _applicationController;
		_applicationController = ac;
		if (applicationController == null)
			throw new Exception("Application controller can't be set to null");
		pushRoute(window.location.hash);
	}

	/// Récupère la pile des contextes
	///
	/// Le contexte en haut de la pile est le contexte courant, les éventuels
	/// contextes en profondeur sont utilisés pour revenir en arrière.
	Stack<Context> get historyStack => applicationController == null ? new Stack<Context>() : applicationController.historyStack;

	/// Récupère le contexte courant
	///
	/// Identique à `historyStack.top`
	Context get currentContext => applicationController == null ? null : applicationController.currentContext;

	/// Récupère la pile des contextes suivants
	///
	/// Les contextes dans cette pile sont utilisés pour retourner en avant.
	Stack<Context> get repushStack => applicationController == null ? new Stack<Context>() : applicationController.repushStack;

	/// Récupère le flux utilisé lorsque le contexte change (aggrégateur de onContextPop/Push/Replacement)
	///
	/// @see onContextPop
	/// @see onContextPush
	/// @see onContextReplacement
	Stream<SyagesAppEvent> get onContextChange => _onContextChangeController.stream;
	/// Récupère le flux utilisé lorsque le contexte courant est éjecté de la pile
	///
	/// @see onContextChange
	/// @see onContextPush
	/// @see onContextReplacement
	Stream<SyagesAppEvent> get onContextPop => _onContextPopController.stream;
	/// Récupère le flux utilisé lorsqu'un nouveau contexte est ajouté à la pile
	///
	/// @see onContextChange
	/// @see onContextPop
	/// @see onContextReplacement
	Stream<SyagesAppEvent> get onContextPush => _onContextPushController.stream;
	/// Récupère le flux utilisé lorsque le contexte courant est remplacé dans la pile par un nouveau contexte
	///
	/// @see onContextChange
	/// @see onContextPop
	/// @see onContextPush
	Stream<SyagesAppEvent> get onContextReplacement => _onContextReplacementController.stream;

	/// Ejecte le contexte courant de la pile.
	///
	/// Si un autre contexte est présent dans la pile, celui-ci est mis en place.
	/// Dans le cas contraire une exception est levée.
	/// @see pushContext
	/// @see replaceContext
	void popContext() {
		_applicationController.popContext();
	}

	/// Ajoute un contexte à la pile.
	///
	/// Si un contexte est en place, celui-ci est sauvegardé conformément à
	/// l'interface SerializableElement.
	/// @see pushRoute
	/// @see replaceContext
	void pushContext(Context ctx) {
        _applicationController.pushContext(ctx);
	}

	/// Ajoute un contexte à la pile par sa route et d'éventuels paramètres.
	///
	/// Si la route ne correspond à aucun Contexte, un contexte 404 est mis en
	/// place. Si un contexte est en place, celui-ci est sauvegardé conformément
	/// à l'interface SerializableElement.
	/// @see pushContext
	/// @see replaceRoute
	/// @see ContextFactory
	/// @see Router
	void pushRoute(String route, [Map<String, dynamic> params])
	{
        RouteConfiguration conf = SyagesRouter.configurationForRoute(route);
        Map<String, dynamic> capturedParameters = SyagesRouter.captureParameters(conf, route);
        if (params != null)
        	capturedParameters.addAll(params);
        Context ctx = new Context(conf, route, capturedParameters);
        pushContext(ctx);
	}

	/// Remplace le contexte courant par le contexte spécifié
	///
	/// Si un contexte est en place, celui-ci est éjecté de la pile.
	/// Sinon, l'appel a cette fonction équivaut à `pushContext`.
	/// @see pushContext
	/// @see replaceRoute
	void replaceContext(Context ctx)
	{
        _applicationController.replaceContext(ctx);
	}

	/// Remplace le contexte courant par une route et d'éventuels paramètres.
	///
	/// Si la route ne correspond à aucun Contexte, un contexte 404 est mis en
	/// place. Si un contexte est en place, celui-ci est éjecté de la pile.
	/// Sinon, l'appel a cette fonction équivaut à `pushContext`.
	/// @see pushRoute
	/// @see replaceContext
	/// @see ContextFactory
	/// @see Router
	void replaceRoute(String route, [Map<String, dynamic> params])
    {
        RouteConfiguration conf = SyagesRouter.configurationForRoute(route);
        Context ctx = new Context(conf, route, params);
        replaceContext(ctx);
    }

	/// Réajoute un contexte dans la pile
	///
	/// Lors de l'éjéction d'un contexte de la pile, celui-ci est sauvegardé et
	/// peut éventuellement être réinséré dans la pile.
	void repushContext()
	{
		_applicationController.repushContext();
	}
}