library syages.proxys.markdown;

import 'dart:html';
import 'package:web_components/interop.dart';
import 'package:polymer/polymer.dart';
import 'package:custom_element_apigen/src/common.dart';

class Markdown extends HtmlElement with DomProxyMixin, PolymerProxyMixin {
	Markdown.created() : super.created();
	factory Markdown() => new Element.tag('mark-down');
	
	String get text => jsElement[r'text'];
    set text(String value) { jsElement[r'text'] = value; }
}
@initMethod
upgradeMarkdown() {registerDartType('mark-down', Markdown); registerDartType("mark-down-editor", Markdown);}
