library syages.proxys.paper_year_picker;

import 'dart:html';
import 'dart:js';
import 'package:web_components/interop.dart';
import 'package:polymer/polymer.dart';
import 'package:custom_element_apigen/src/common.dart';

class PaperYearPicker extends HtmlElement with DomProxyMixin, PolymerProxyMixin {
	PaperYearPicker.created() : super.created();
	factory PaperYearPicker() => new Element.tag('paper-year-picker');
	
	int get year => jsElement[r'year'];
	set year(int value) { jsElement[r'year'] = value; }
	
	bool get infiniteScrolling {
		if (jsElement[r'infiniteScrolling'].runtimeType == String && jsElement[r'infiniteScrolling'] == "auto") return false;
		return jsElement[r'infiniteScrolling'] as bool;
	}
    set infiniteScrolling(bool value) { jsElement[r'infiniteScrolling'] = value; }
	
	JsArray get locale => jsElement[r'max'];
    set locale(List<String> value) { jsElement[r'max'] = new JsArray.from(value); }
	
	DateTime get max => jsElement[r'max'];
    set max(DateTime value) { jsElement[r'max'] = value; }
    	
	DateTime get min => jsElement[r'min'];
	set min(DateTime value) { jsElement[r'min'] = value; }
	
	refreshScrollPosition() => jsElement.callMethod("refreshScrollPosition", []);
	setYearArray() => jsElement.callMethod("setYearArray", []);
	yearSelected(e) => jsElement.callMethod("yearSelected", [e]);
}
@initMethod
upgradePaperDatePicker() => registerDartType('paper-year-picker', PaperYearPicker);
