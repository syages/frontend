library syages.proxys.paper_date_picker_dialog;

import 'dart:html';
import 'dart:js';
import 'package:web_components/interop.dart';
import 'package:polymer/polymer.dart';
import 'package:custom_element_apigen/src/common.dart';

class PaperDatePickerDialog extends HtmlElement with DomProxyMixin, PolymerProxyMixin {
	PaperDatePickerDialog.created() : super.created();
	factory PaperDatePickerDialog() => new Element.tag('paper-date-picker-dialog');
	
	//DateTime get date => jsElement[r'date'];
	//set date(DateTime value) { jsElement[r'date'] = value; }
	//surroundDate(DateTime date) => jsElement.callMethod("surroundDate", [date]);

	DateTime get immediateDate => jsElement[r'immediateDate'];
    set immediateDate(DateTime value) { jsElement[r'immediateDate'] = value; }
	
	bool get infiniteScrolling {
		if (jsElement[r'infiniteScrolling'].runtimeType == String && jsElement[r'infiniteScrolling'] == "auto") return false;
		return jsElement[r'infiniteScrolling'] as bool;
	}
    set infiniteScrolling(bool value) { jsElement[r'infiniteScrolling'] = value; }
	
	JsArray get locale => jsElement[r'max'];
    set locale(List<String> value) { jsElement[r'max'] = new JsArray.from(value); }
	
	DateTime get max => jsElement[r'max'];
    set max(DateTime value) { jsElement[r'max'] = value; }
    	
	DateTime get min => jsElement[r'min'];
	set min(DateTime value) { jsElement[r'min'] = value; }
	
	int get startDayOfWeek => jsElement[r'startDayOfWeek'];
	set startDayOfWeek(int value) { jsElement[r'startDayOfWeek'] = value; }
    
	String get value => jsElement[r'value'];
    set value(String value) { jsElement[r'value'] = value; }
    
    open() => jsElement.callMethod("open", []);
    setDate() => jsElement.callMethod("setDate", []);
    showDatePicker() => jsElement.callMethod("showDatePicker", []);
    showYearPicker() => jsElement.callMethod("showYearPicker", []);
}
@initMethod
upgradePaperDatePickerDialog() => registerDartType('paper-date-picker-dialog', PaperDatePickerDialog);
