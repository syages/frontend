library syages.proxys.favicon_bubble;

import 'dart:html';
import 'package:web_components/interop.dart';
import 'package:polymer/polymer.dart';
import 'package:custom_element_apigen/src/common.dart';

class FavIconBubble extends HtmlElement with DomProxyMixin, PolymerProxyMixin {
	FavIconBubble.created() : super.created();
	factory FavIconBubble() => new Element.tag('favicon-bubble');
	
	String get background => jsElement[r'background'];
	set background(String value) { jsElement[r'background'] = value; }
	
	String get color => jsElement[r'colour'];
	set color(String value) { jsElement[r'colour'] = value; }
	
	String get font => jsElement[r'font'];
    set font(String value) { jsElement[r'font'] = value; }
	
	int get height => int.parse(jsElement[r'height']);
	set height(int value) { jsElement[r'height'] = value.toString(); }
	
	String get label => jsElement[r'label'];
    set label(String value) { jsElement[r'label'] = value; }
    
	int get width => int.parse(jsElement[r'width']);
	set width(int value) { jsElement[r'width'] = value.toString(); }

	update() => jsElement.callMethod('updateOptions', []);
}
@initMethod
upgradeFavIconBubble() => registerDartType('favicon-bubble', FavIconBubble);
