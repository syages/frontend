library syages.proxys.prism;

import 'dart:html';
import 'package:web_components/interop.dart';
import 'package:polymer/polymer.dart';
import 'package:custom_element_apigen/src/common.dart';

class Prism extends HtmlElement with DomProxyMixin, PolymerProxyMixin {
	Prism.created() : super.created();
	factory Prism() => new Element.tag('prism-js');
	
	String get language => jsElement[r'language'];
    set language(String value) { jsElement[r'language'] = value; }

    String get lineNumbers => jsElement[r'linenumbers'];
    set lineNumbers(String value) { jsElement[r'linenumbers'] = value; }

    bool get escape => jsElement[r'escape'];
    set escape(bool value) { jsElement[r'escape'] = value; }

    bool get lineHighlight => jsElement[r'linehighlight'];
    set lineHighlight(bool value) { jsElement[r'linehighlight'] = value; }

    String get theme => jsElement[r'theme'];
    set theme(String value) { jsElement[r'theme'] = value; }

    String get inputValue => jsElement[r'inputValue'];
    set inputValue(String value) { jsElement[r'inputValue'] = value; }

    highlight() => jsElement.callMethod('highlight', []);
    injectTheme() => jsElement.callMethod('injectTheme', []);
    injectPluginStylesheets() => jsElement.callMethod('injectPluginStylesheets', []);
    injectStylesheet() => jsElement.callMethod('injectStylesheet', []);
}
@initMethod
upgradePrism() => registerDartType('prism-js', Prism);
