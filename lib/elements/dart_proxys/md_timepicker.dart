library syages.proxys.md_timepicker;

import 'dart:html';
import 'package:web_components/interop.dart';
import 'package:polymer/polymer.dart';
import 'package:custom_element_apigen/src/common.dart';

class MdTimePicker extends HtmlElement with DomProxyMixin, PolymerProxyMixin{
	MdTimePicker.created() : super.created();
    factory MdTimePicker() => new Element.tag('md-timepicker');
    
    String get color => jsElement[r'color'];
	set color(String value) { jsElement[r'color'] = value.toString(); }
    	
	DateTime get time => DateTime.parse(jsElement[r'time']);
	set time(DateTime value) { jsElement[r'time'] = value.toString(); }
	
	open() => jsElement.callMethod("open", []);
	
}
@initMethod
upgradeMdTimePicker() => registerDartType("md-timepicker", MdTimePicker);