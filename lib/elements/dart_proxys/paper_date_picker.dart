library syages.proxys.paper_date_picker;

import 'dart:html';
import 'dart:js';
import 'package:web_components/interop.dart';
import 'package:polymer/polymer.dart';
import 'package:custom_element_apigen/src/common.dart';

class PaperDatePicker extends HtmlElement with DomProxyMixin, PolymerProxyMixin {
	PaperDatePicker.created() : super.created();
	factory PaperDatePicker() => new Element.tag('paper-date-picker');
	
	DateTime get date => jsElement[r'date'];
	set date(DateTime value) { jsElement[r'date'] = value; }
	
	bool get infiniteScrolling {
		if (jsElement[r'infiniteScrolling'].runtimeType == String && jsElement[r'infiniteScrolling'] == "auto") return false;
		return jsElement[r'infiniteScrolling'] as bool;
	}
    set infiniteScrolling(bool value) { jsElement[r'infiniteScrolling'] = value; }
	
	JsArray get locale => jsElement[r'max'];
    set locale(List<String> value) { jsElement[r'max'] = new JsArray.from(value); }
	
	DateTime get max => jsElement[r'max'];
    set max(DateTime value) { jsElement[r'max'] = value; }
    	
	DateTime get min => jsElement[r'min'];
	set min(DateTime value) { jsElement[r'min'] = value; }
	
	int get startDayOfWeek => jsElement[r'startDayOfWeek'];
	set startDayOfWeek(int value) { jsElement[r'startDayOfWeek'] = value; }
	
	pickDate(e) => jsElement.callMethod("pickDate", [e]);
	refreshScrollPosition() => jsElement.callMethod("refreshScrollPosition", []);
	renderMonths(int start, int end) => jsElement.callMethod("renderMonths", [start, end]);
	scrollFinished() => jsElement.callMethod("scrollFinished", []);
	setCurrentDateValues() => jsElement.callMethod("setCurrentDateValues", []);
	surroundDate(DateTime date) => jsElement.callMethod("surroundDate", [date]);
}
@initMethod
upgradePaperDatePicker() => registerDartType('paper-date-picker', PaperDatePicker);
