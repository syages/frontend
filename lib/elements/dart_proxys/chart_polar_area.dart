library syages.proxys.chart_polar_area;

import 'dart:html';
import 'dart:js';
import 'package:web_components/interop.dart';
import 'package:polymer/polymer.dart';
import 'package:custom_element_apigen/src/common.dart';

class ChartPolarArea extends HtmlElement with DomProxyMixin, PolymerProxyMixin {
	ChartPolarArea.created() : super.created();
	factory ChartPolarArea() => new Element.tag('chart-polar-area');

	int get height => int.parse(jsElement[r'height']);
	set height(int value) { jsElement[r'height'] = value.toString(); }
	int get width => int.parse(jsElement[r'width']);
	set width(int value) { jsElement[r'width'] = value.toString(); }

	List<String> get colors => jsElement[r'colors'];
	set colors(List<String> value) { jsElement[r'colors'] = new JsArray<String>.from(value); }

	List<num> get values => jsElement[r'values'] as JsArray<num>;
	set values(List<num> value) { jsElement[r'values'] = new JsArray<num>.from(value); }
	
	updateChart() => jsElement.callMethod('updateChart', []);
}
@initMethod
upgradeChartPolarArea() => registerDartType('chart-polar-area', ChartPolarArea);
