library syages.proxys.file_drop;

import 'dart:html';
import 'package:web_components/interop.dart';
import 'package:polymer/polymer.dart';
import 'package:custom_element_apigen/src/common.dart';

class FileDrop extends HtmlElement with DomProxyMixin, PolymerProxyMixin {
	FileDrop.created() : super.created();
	factory FileDrop() => new Element.tag('file-drop');

}
@initMethod
upgradeFileDrop() => registerDartType('file-drop', FileDrop);
