library syages.proxys.chart_line;

import 'dart:html';
import 'dart:js';
import 'package:web_components/interop.dart';
import 'package:polymer/polymer.dart';
import 'package:custom_element_apigen/src/common.dart';

class ChartLine extends HtmlElement with DomProxyMixin, PolymerProxyMixin {
	ChartLine.created() : super.created();
	factory ChartLine() => new Element.tag('chart-line');

	int get height => int.parse(jsElement[r'height']);
	set height(int value) { jsElement[r'height'] = value.toString(); }
	int get width => int.parse(jsElement[r'width']);
	set width(int value) { jsElement[r'width'] = value.toString(); }

	List<String> get colors => jsElement[r'colors'];
	set colors(List<String> value) { jsElement[r'colors'] = new JsArray<String>.from(value); }

	List<String> get labels => jsElement[r'labels'];
	set labels(List<String> value) { jsElement[r'labels'] = new JsArray.from(value); }

	List<List<num>> get values => jsElement[r'values'] as JsArray<JsArray<num>>;
	set values(List<List<num>> value) {
		JsArray<JsArray<num>> values = new JsArray<JsArray<num>>();
		value.forEach((List<num> list) {
			values.add(new JsArray<num>.from(list));
		});
		jsElement[r'values'] = values;
	}
	
	updateChart() => jsElement.callMethod('updateChart', []);
}
@initMethod
upgradeChartLine() => registerDartType('chart-line', ChartLine);
