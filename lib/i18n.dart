library syages.i18n;

abstract class i18n {
	static List<String> get MONTHS => ["janvier", "février", "mars", "avril", "mai", "juin",
	                                   "juillet", "août", "septembre", "octobre", "novembre", "décembre"];
	static List<String> get WEEKDAYS => ["lundi", "mardi", "mercredi", "jeudi", "vendredi", "samedi", "dimanche"];
}