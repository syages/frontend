library syages.Routes;

import 'Router.dart';

abstract class Routes {
	static String uuid = "((\\w{8}(-\\w{4}){3}-\\w{12}?)|new)";
	static String token = "([a-z0-9]{16})";

	static RouteConfiguration get notFound => new RouteConfiguration('^\r\$', "syv-error404", requireAuth: false, withNavbar: false);
	static RouteConfiguration get notFoundWhenLogged => new RouteConfiguration('^\r\$', "syv-error404", requireAuth: true, withNavbar: true);
	static RouteConfiguration get root => new RouteConfiguration('^(login)?\$', "syv-login", requireAuth: false, withNavbar: false);

	static RouteConfiguration get contactDirectory => new RouteConfiguration('^\contactDirectory\$', "syv-contact-directory", requireAuth: true, withNavbar: true);
	static RouteConfiguration get contextTester => new RouteConfiguration("^ctx\/$uuid\$", "context-tester", paramCaptures: {0: new RouteParameter("model.field")}, requireAuth: false, withNavbar: false);
	static RouteConfiguration get dashboard => new RouteConfiguration('^dashboard\$', "syv-dashboard", requireAuth: true, withNavbar: true);
	static RouteConfiguration get newsFeed => new RouteConfiguration('^news\$', "syv-newsfeed", requireAuth: true, withNavbar: true);
	static RouteConfiguration get profileDetails => new RouteConfiguration('^profile\$', "syv-profile-details", requireAuth: true, withNavbar: true);
	static RouteConfiguration get profileSettings => new RouteConfiguration('^profile\$', "syv-profile-form", requireAuth: true, withNavbar: true);
	static RouteConfiguration get signupForm => new RouteConfiguration("^signup\$", "syv-signup-form", requireAuth: false, withNavbar: false);
	static RouteConfiguration get forgotPassword => new RouteConfiguration("^iforgot\$", "syv-password-reset-request", requireAuth: false, withNavbar: false);
	static RouteConfiguration get forgotPasswordForm => new RouteConfiguration("^iforgot\/$token\$", "syv-password-reset-form", paramCaptures: {0: new RouteParameter("tokenKey")}, requireAuth: false, withNavbar: false);

	// Administrators
	static RouteConfiguration get a_bugReports => new RouteConfiguration('^bugReports\$', "syv-bugreport-list--a", requireAuth: true, withNavbar: true);
	static RouteConfiguration get a_bugReportDetail => new RouteConfiguration("^bugReports\/$uuid\$", "syv-bugreport-detail--a", paramCaptures: {0: new RouteParameter("reportId")}, requireAuth: true, withNavbar: true);
	static RouteConfiguration get aps_administrators => new RouteConfiguration("^administrators\$", "syv-administrator-list--aps", requireAuth: true, withNavbar: true);
	static RouteConfiguration get aps_administratorForm => new RouteConfiguration("^administrators\/$uuid\$", "syv-administrator-form--aps", paramCaptures: {0: new RouteParameter("administratorId")}, requireAuth: true, withNavbar: true);
	static RouteConfiguration get aps_courseForm => new RouteConfiguration("^courses\/$uuid\$", 'syv-course-form--aps', paramCaptures: {0: new RouteParameter("courseId")}, requireAuth: true, withNavbar: true);
	static RouteConfiguration get aps_courses => new RouteConfiguration("^courses\$", 'syv-course-list--aps', requireAuth: true, withNavbar: true);
	static RouteConfiguration get aps_diplomas => new RouteConfiguration("^diplomas\$", "syv-diploma-list--aps", requireAuth: true, withNavbar: true);
	static RouteConfiguration get aps_diplomaForm => new RouteConfiguration('^diplomas\/$uuid\$', "syv-diploma-form--aps", requireAuth: true, withNavbar: true);
	static RouteConfiguration get aps_domains => new RouteConfiguration("^domains\$", "syv-domain-list--aps", requireAuth: true, withNavbar: true);
	static RouteConfiguration get aps_domainForm => new RouteConfiguration("^domains\/$uuid\$", "syv-domain-form--aps", paramCaptures: {0: new RouteParameter("domainId")}, requireAuth: true, withNavbar: true);
	static RouteConfiguration get aps_interns => new RouteConfiguration("^interns\$", "syv-intern-list--aps", requireAuth: true, withNavbar: true);
	static RouteConfiguration get aps_internForm => new RouteConfiguration("^interns\/$uuid\$", "syv-intern-form--aps", paramCaptures: {0: new RouteParameter("internId")}, requireAuth: true, withNavbar: true);
	static RouteConfiguration get aps_newsForm => new RouteConfiguration("^news\/$uuid\$", "syv-news-form--aps", paramCaptures: {0: new RouteParameter("informationId")}, requireAuth: true, withNavbar: true);
	static RouteConfiguration get aps_presidents => new RouteConfiguration("^presidents\$", "syv-president-list--aps", requireAuth: true, withNavbar: true);
	static RouteConfiguration get aps_presidentForm => new RouteConfiguration("^presidents\/$uuid\$", "syv-president-form--aps", paramCaptures: {0: new RouteParameter("presidentId")}, requireAuth: true, withNavbar: true);
	static RouteConfiguration get aps_staffs => new RouteConfiguration("^staffs\$", "syv-staff-list--aps", requireAuth: true, withNavbar: true);
    static RouteConfiguration get aps_staffForm => new RouteConfiguration("^staffs\/$uuid\$", "syv-staff-form--aps", paramCaptures: {0: new RouteParameter("staffId")}, requireAuth: true, withNavbar: true);
	static RouteConfiguration get aps_teachers => new RouteConfiguration("^teachers\$", "syv-teacher-list--aps", requireAuth: true, withNavbar: true);
	static RouteConfiguration get aps_teacherForm => new RouteConfiguration("^teachers\/$uuid\$", "syv-teacher-form--aps", paramCaptures: {0: new RouteParameter("teacherId")}, requireAuth: true, withNavbar: true);
	static RouteConfiguration get apst_evaluationForm => new RouteConfiguration("evaluations\/$uuid\$", "syv-evaluation-form--apst", paramCaptures: {0: new RouteParameter("evaluationId")}, requireAuth: true, withNavbar: true);
	static RouteConfiguration get apst_evaluationDetail => new RouteConfiguration("evaluations\/$uuid\/details\$", "syv-evaluation-detail--apst", paramCaptures: {0: new RouteParameter("evaluationId")}, requireAuth: true, withNavbar: true);
	static RouteConfiguration get apst_lectureForm => new RouteConfiguration("lectures\/$uuid\$", "syv-lecture-form--apst", paramCaptures: {0: new RouteParameter("lectureId")}, requireAuth: true, withNavbar: true);
	static RouteConfiguration get apst_lectureDetail => new RouteConfiguration("lectures\/$uuid\/details\$", "syv-lecture-detail--apst", paramCaptures: {0: new RouteParameter("lectureId")}, requireAuth: true, withNavbar: true);

	// President
    static RouteConfiguration get ps_diplomas => notFoundWhenLogged;

    // Staff members

    // Teachers
    static RouteConfiguration get t_lessons => new RouteConfiguration("^lessons\$", "syv-teacher-lessons--t", requireAuth: true, withNavbar: true);
    static RouteConfiguration get t_classroom => new RouteConfiguration("^classroom\/$uuid\$", "syv-classroom--t", paramCaptures: {0: new RouteParameter("lessonId")}, requireAuth: true, withNavbar: true);
    static RouteConfiguration get t_lectures => notFoundWhenLogged;

    // Interns
    static RouteConfiguration get i_absences => new RouteConfiguration("^absences\$", "syv-absences--i", requireAuth: true, withNavbar: true);
    static RouteConfiguration get i_firstLogin => new RouteConfiguration("^firstLogin\$", "syv-first-login--i", requireAuth: true, withNavbar: false);
    static RouteConfiguration get i_marks => new RouteConfiguration('^marks\$', "syv-marks--i", requireAuth: true, withNavbar: true);


}
