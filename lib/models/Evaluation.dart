library syages.models.Evaluation;

import 'dart:async';
import 'dart:convert';
import 'dart:html';
import 'package:polymer/polymer.dart';
import '../Exceptions.dart';
import '../Helpers.dart';
import '../Instance.dart';
import 'Empty.dart';
import 'Lesson.dart';
import 'Mark.dart';
import 'Period.dart';

class Evaluation extends Observable {
	static final List<String> TYPE = ["Continue", "Final"];

	@observable double coef;
	@observable DateTime date;
	@observable int gradingScale;
	@observable String id;
	@observable Lesson lesson;
	@observable String lessonId;
	@observable String name;
	@observable Period period;
	@observable String type;
	@observable int version;
	@observable bool visible;

	Evaluation(String name, DateTime date, int gradingScale, double coef, bool visible, String type, Lesson lesson)
	{
        this.coef = coef;
        this.date = date;
        this.gradingScale = gradingScale;
		this.id = null;
        this.lesson = lesson;
        this.name = name;
        this.type = type;
        this.version = 0;
        this.visible = visible;
	}

	Evaluation.empty() : super()
	{
		this.lesson = new Lesson.empty();
		this.type = TYPE[0];
		this.visible = true;
		this.coef = 1.0;
		this.gradingScale = 20;
		this.date = new DateTime.now();
	}

	Evaluation.fromMap(Map<String, dynamic> json)
	{
        this.coef = json["coefficient"] as double;
        this.date = new DateTime.fromMillisecondsSinceEpoch(json["date"]);
        this.gradingScale = json["gradingScale"];
		this.id = json["id"];
        this.lesson = Lesson.createFromJson(json["lesson"] as Map);
        this.name = json["name"];
        this.type = json["type"];
        this.version = json["version"];
        this.visible = json["visible"];
	}

	Map<String, dynamic> toMapForCreation()
	{
    	return {
    		"coefficient" : this.coef,
        	"date" : this.date.millisecondsSinceEpoch,
        	"gradingScale" : this.gradingScale,
        	"lesson" : this.lessonId,
        	"name" : this.name,
        	"type" : this.type,
        	"visible" : this.visible
        } as Map<String,dynamic>;
	}

	Map<String, dynamic> toMapForUpdate()
    {
    	return {
    		"coefficient" : this.coef,
        	"date" : this.date.millisecondsSinceEpoch,
        	"gradingScale" : this.gradingScale,
        	"id" : this.id,
        	"lesson" : this.lesson.id,
        	"name" : this.name,
        	"type" : this.type,
        	"version" : this.version,
        	"visible" : this.visible
    	} as Map<String,dynamic>;
    }

	String toString() => "Evaluation #$id: $name ($type) en ${lesson.course.name}";

	/* Requests */
	static Future<Evaluation> evaluation(String id) {
    	return new Future<Evaluation>.microtask(() {
        	HttpRequest req = new HttpRequest();
        	req.open("GET", SyagesInstance.shared.apiUrl("evaluations/$id"), async: false);
    		addHeadersToRequest(req, requestHeaders());
    		req.send();
    		if (req.response == null) throw new RequestFailureException();

    		switch(req.status)
   			{
   				case 200:
					return new Evaluation.fromMap(JSON.decode(req.response));
            	case 400:
            		throw new BadRequestException("Erreur ${req.status}:\n\t${req.response}");
   				case 401:
   					throw new UnauthorizedException("Erreur ${req.status}:\n\t${req.response}");
   				default:
   					throw new UnknownException("Erreur ${req.status}:\n\t${req.response}");
   			}
   		});
   	}

	static Future<List<Evaluation>> evaluationList([int limit = 25, int offset = 0])
	{
		return new Future<List<Evaluation>>.microtask(() {
    		HttpRequest req = new HttpRequest();
    		req.open("GET", SyagesInstance.shared.apiUrl("evaluations?limit=$limit&offset=$offset"), async: false);
			addHeadersToRequest(req, requestHeaders());
			req.send();
			if (req.response == null) throw new RequestFailureException();

			switch(req.status)
			{
				case 200:
					List<Map<String,dynamic>> res = JSON.decode(req.response);
					List<Evaluation> list = new List<Evaluation>();
					res.forEach((Map<String, dynamic> _) {
						list.add(new Evaluation.fromMap(_));
					});
					return list;
            	case 400:
            		throw new BadRequestException("Erreur ${req.status}:\n\t${req.response}");
				case 401:
					throw new UnauthorizedException("Erreur ${req.status}:\n\t${req.response}");
				case 404:
					throw new NotFoundException("Erreur ${req.status}:\n\t${req.response}");
				default:
					throw new UnknownException("Erreur ${req.status}:\n\t${req.response}");
			}
		});
	}

	Future<List<Mark>> addMarks(List<Mark> marks)
	{
    	return new Future<List<Mark>>.microtask(() {
        	HttpRequest req = new HttpRequest();
        	req.open("POST", SyagesInstance.shared.apiUrl("evaluations/$id/marks"), async: false);
    		addHeadersToRequest(req, requestHeaders());
//    		Pourquoi JSON.encode et pas marks.toMapForCreation ou une fonction dans le meme style ?
    		var m = new List();
    		marks.forEach((mark) => m.add(mark.toMapForCreation()));
    		req.send( toJsonString(m));
    		if (req.response == null) throw new RequestFailureException();

    		switch(req.status)
   			{
   				case 201:
   					List<Map<String,dynamic>> res = JSON.decode(req.response);
                    List<Mark> list = new List<Mark>();
                    res.forEach((Map<String, dynamic> _) {
                    	list.add(new Mark.fromMap(_));
                    });
                    return list;
            	case 400:
            		throw new BadRequestException("Erreur ${req.status}:\n\t${req.response}");
   				case 401:
   					throw new UnauthorizedException("Erreur ${req.status}:\n\t${req.response}");
   				default:
   					throw new UnknownException("Erreur ${req.status}:\n\t${req.response}");
   			}
   		});
   	}

	Future<Empty> delete() {
    	return new Future<Empty>.microtask(() {
        	HttpRequest req = new HttpRequest();
        	req.open("DELETE", SyagesInstance.shared.apiUrl("evaluations/$id"), async: false);
    		addHeadersToRequest(req, requestHeaders(null));
    		req.send();
    		if (req.response == null) throw new RequestFailureException();

    		switch(req.status)
   			{
   				case 204:
					return new Empty();
            	case 400:
            		throw new BadRequestException("Erreur ${req.status}:\n\t${req.response}");
   				case 404:
   					throw new NotFoundException("Erreur ${req.status}:\n\t${req.response}");
   				default:
   					throw new UnknownException("Erreur ${req.status}:\n\t${req.response}");
   			}
   		});
   	}

	Future<List<Mark>> editMarks(List<Mark> marks)
	{
    	return new Future<List<Mark>>.microtask(() {
        	HttpRequest req = new HttpRequest();
        	req.open("PUT", SyagesInstance.shared.apiUrl("evaluations/$id/marks"), async: false);
    		addHeadersToRequest(req, requestHeaders());
    		var m = new List();
    		marks.forEach((mark) => m.add(mark.toMapForUpdate()));
			req.send( toJsonString(m));
    		if (req.response == null) throw new RequestFailureException();

    		switch(req.status)
   			{
   				case 200:
   					List<Map<String,dynamic>> res = JSON.decode(req.response);
                    List<Mark> list = new List<Mark>();
                    res.forEach((Map<String, dynamic> _) {
                    	list.add(new Mark.fromMap(_));
                    });
                    return list;
            	case 400:
            		throw new BadRequestException("Erreur ${req.status}:\n\t${req.response}");
   				case 401:
   					throw new UnauthorizedException("Erreur ${req.status}:\n\t${req.response}");
   				default:
   					throw new UnknownException("Erreur ${req.status}:\n\t${req.response}");
   			}
   		});
   	}

	Future<List<Mark>> marks() {
    	return new Future<List<Mark>>.microtask(() {
        	HttpRequest req = new HttpRequest();
        	req.open("GET", SyagesInstance.shared.apiUrl("evaluations/$id/marks"), async: false);
    		addHeadersToRequest(req, requestHeaders());
    		req.send();
    		if (req.response == null) throw new RequestFailureException();

    		switch(req.status)
   			{
   				case 200:
   					List<Map<String,dynamic>> res = JSON.decode(req.response);
                    List<Mark> list = new List<Mark>();
                    res.forEach((Map<String, dynamic> _) {
                    	list.add(new Mark.fromMap(_));
                    });
                    return list;
            	case 400:
            		throw new BadRequestException("Erreur ${req.status}:\n\t${req.response}");
   				case 401:
   					throw new UnauthorizedException("Erreur ${req.status}:\n\t${req.response}");
   				default:
   					throw new UnknownException("Erreur ${req.status}:\n\t${req.response}");
   			}
   		});
   	}


	Future<Evaluation> save()
	{
		if (id == null) return _save();
		else return _update();
	}

	Future<Evaluation> _save()
	{
		return new Future<Evaluation>.microtask(()
		{
			HttpRequest req = new HttpRequest();
			req.open("POST", SyagesInstance.shared.apiUrl("evaluations"), async: false);
			addHeadersToRequest(req, requestHeaders());
			var m = this.toMapForCreation();
			req.send(toJsonString(m));
			if (req.response == null) throw new RequestFailureException();

			switch(req.status)
			{
				case 201:
					return new Evaluation.fromMap(JSON.decode(req.response));
            	case 400:
            		throw new BadRequestException("Erreur ${req.status}:\n\t${req.response}");
				case 401:
					throw new UnauthorizedException("Erreur ${req.status}:\n\t${req.response}");
				default:
					throw new UnknownException("Erreur ${req.status}:\n\t${req.response}");
			}
		});
	}

	Future<Evaluation> _update()
	{
		return new Future<Evaluation>.microtask(()
		{
        	HttpRequest req = new HttpRequest();
        	req.open("PUT", SyagesInstance.shared.apiUrl("evaluations/$id"), async: false);
        	addHeadersToRequest(req, requestHeaders());
        	req.send(toJsonString(this.toMapForUpdate()));
        	if (req.response == null) throw new RequestFailureException();

        	switch(req.status)
        	{
        		case 200:
        			return new Evaluation.fromMap(JSON.decode(req.response));
            	case 400:
            		throw new BadRequestException("Erreur ${req.status}:\n\t${req.response}");
        		case 401:
        			throw new UnauthorizedException("Erreur ${req.status}:\n\t${req.response}");
        		default:
        			throw new UnknownException("Erreur ${req.status}:\n\t${req.response}");
        	}
        });
	}
}
