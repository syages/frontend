library syages.models.Period;

import 'dart:async';
import 'dart:convert';
import 'dart:html';
import 'package:polymer/polymer.dart';
import '../Exceptions.dart';
import '../Helpers.dart';
import '../Instance.dart';
import 'Diploma.dart';
import 'Empty.dart';
import 'Evaluation.dart';

class Period extends Observable {
	@observable Diploma diploma;
	@observable DateTime endDate;
	@observable String id;
	@observable int ordre;
	@observable DateTime startDate;
	@observable int version;

	Period(String id, Diploma diploma, int ordre, DateTime startDate, DateTime endDate) :
		this.diploma = diploma,
		this.endDate = endDate,
		this.id = id,
		this.ordre = ordre,
		this.startDate = startDate,
		this.version = 0;

	Period.empty() : super()
	{
		this.startDate = new DateTime.now();
		this.endDate = new DateTime.now();
	}

	Period.fromMap(Map<String, dynamic> json) :
		this.diploma = Diploma.createFromJson(json["diploma"] as Map),
		this.endDate = new DateTime.fromMillisecondsSinceEpoch(json["endDate"]),
		this.id = json["id"],
		this.ordre = json["ordre"],
		this.startDate = new DateTime.fromMillisecondsSinceEpoch(json["startDate"]),
		this.version = json["version"];

	Map<String, dynamic> toMapForCreation()
	{
    	return {
        	"endDate" : this.endDate.millisecondsSinceEpoch,
        	"ordre" : this.ordre,
        	"startDate" : this.startDate.millisecondsSinceEpoch
        } as Map<String,dynamic>;
	}

	Map<String, dynamic> toMapForUpdate()
    {
    	return {
    		"endDate" : this.endDate.millisecondsSinceEpoch,
        	"id" : this.id,
        	"ordre" : this.ordre,
        	"startDate" : this.startDate.millisecondsSinceEpoch,
        	"version" : this.version
    	} as Map<String,dynamic>;
    }

	String toString() => "Period #$id: $ordre trimestre du ${diploma.name}";

	/* Requests */
	static Future<Period> period(String id) {
    	return new Future<Period>.microtask(() {
        	HttpRequest req = new HttpRequest();
        	req.open("GET", SyagesInstance.shared.apiUrl("periods/$id"), async: false);
    		addHeadersToRequest(req, requestHeaders());
    		req.send();
    		if (req.response == null) throw new RequestFailureException();

    		switch(req.status)
   			{
   				case 200:
					return new Period.fromMap(JSON.decode(req.response));
            	case 400:
            		throw new BadRequestException("Erreur ${req.status}:\n\t${req.response}");
   				case 401:
   					throw new UnauthorizedException("Erreur ${req.status}:\n\t${req.response}");
   				default:
   					throw new UnknownException("Erreur ${req.status}:\n\t${req.response}");
   			}
   		});
   	}

	static Future<List<Period>> periodList([int limit = 25, int offset = 0])
	{
		return new Future<List<Period>>.microtask(() {
    		HttpRequest req = new HttpRequest();
    		req.open("GET", SyagesInstance.shared.apiUrl("periods?limit=$limit&offset=$offset"), async: false);
			addHeadersToRequest(req, requestHeaders());
			req.send();
			if (req.response == null) throw new RequestFailureException();

			switch(req.status)
			{
				case 200:
					List<Map<String,dynamic>> res = JSON.decode(req.response);
					List<Period> list = new List<Period>();
					res.forEach((Map<String, dynamic> _) {
						list.add(new Period.fromMap(_));
					});
					return list;
            	case 400:
            		throw new BadRequestException("Erreur ${req.status}:\n\t${req.response}");
				case 401:
					throw new UnauthorizedException("Erreur ${req.status}:\n\t${req.response}");
				case 404:
					throw new NotFoundException("Erreur ${req.status}:\n\t${req.response}");
				default:
					throw new UnknownException("Erreur ${req.status}:\n\t${req.response}");
			}
		});
	}

	Future<Empty> delete() {
    	return new Future<Empty>.microtask(() {
        	HttpRequest req = new HttpRequest();
        	req.open("DELETE", SyagesInstance.shared.apiUrl("periods/$id"), async: false);
    		addHeadersToRequest(req, requestHeaders(null));
    		req.send();
    		if (req.response == null) throw new RequestFailureException();

    		switch(req.status)
   			{
   				case 204:
					return new Empty();
            	case 400:
            		throw new BadRequestException("Erreur ${req.status}:\n\t${req.response}");
   				case 404:
   					throw new NotFoundException("Erreur ${req.status}:\n\t${req.response}");
   				default:
   					throw new UnknownException("Erreur ${req.status}:\n\t${req.response}");
   			}
   		});
   	}

	Future<List<Evaluation>> evaluations([int limit = 25, int offset = 0])
    {
    	return new Future<List<Evaluation>>.microtask(() {
       		HttpRequest req = new HttpRequest();
       		req.open("GET", SyagesInstance.shared.apiUrl("periods/$id/evaluations?limit=$limit&offset=$offset"), async: false);
    		addHeadersToRequest(req, requestHeaders());
    		req.send();
    		if (req.response == null) throw new RequestFailureException();

    		switch(req.status)
    		{
    			case 200:
    				List<Map<String,dynamic>> res = JSON.decode(req.response);
    				List<Evaluation> list = new List<Evaluation>();
    				res.forEach((Map<String, dynamic> _) {
    					list.add(new Evaluation.fromMap(_));
    				});
    				return list;
            	case 400:
            		throw new BadRequestException("Erreur ${req.status}:\n\t${req.response}");
    			case 401:
    				throw new UnauthorizedException("Erreur ${req.status}:\n\t${req.response}");
    			case 404:
    				throw new NotFoundException("Erreur ${req.status}:\n\t${req.response}");
    			default:
    				throw new UnknownException("Erreur ${req.status}:\n\t${req.response}");
    		}
    	});
	}

	Future<Period> save()
	{
		if (id == null) return _save();
		else return _update();
	}

	Future<Period> _save()
	{
		return new Future<Period>.microtask(()
		{
			HttpRequest req = new HttpRequest();
			req.open("POST", SyagesInstance.shared.apiUrl("periods"), async: false);
			addHeadersToRequest(req, requestHeaders());
			req.send(toJsonString(this.toMapForCreation()));
			if (req.response == null) throw new RequestFailureException();

			switch(req.status)
			{
				case 201:
					return new Period.fromMap(JSON.decode(req.response));
            	case 400:
            		throw new BadRequestException("Erreur ${req.status}:\n\t${req.response}");
				case 401:
					throw new UnauthorizedException("Erreur ${req.status}:\n\t${req.response}");
				default:
					throw new UnknownException("Erreur ${req.status}:\n\t${req.response}");
			}
		});
	}

	Future<Period> _update()
	{
		return new Future<Period>.microtask(()
		{
        	HttpRequest req = new HttpRequest();
        	req.open("PUT", SyagesInstance.shared.apiUrl("periods/$id"), async: false);
        	addHeadersToRequest(req, requestHeaders());
        	req.send(toJsonString(this.toMapForUpdate()));
        	if (req.response == null) throw new RequestFailureException();

        	switch(req.status)
        	{
        		case 201:
        			return new Period.fromMap(JSON.decode(req.response));
            	case 400:
            		throw new BadRequestException("Erreur ${req.status}:\n\t${req.response}");
        		case 401:
        			throw new UnauthorizedException("Erreur ${req.status}:\n\t${req.response}");
        		default:
        			throw new UnknownException("Erreur ${req.status}:\n\t${req.response}");
        	}
        });
	}
}
