library syages.models.Staff;

import 'dart:async';
import 'dart:convert';
import 'dart:html';
import 'package:polymer/polymer.dart';
import '../Exceptions.dart';
import '../Helpers.dart';
import '../Instance.dart';
import 'Empty.dart';
import 'User.dart';

class Staff extends Observable {
	@observable String id;
	@observable String number;
	@observable String office;
	@observable User user;
	@observable int version;

	Staff(String email, String firstName, String lastName, [String number=null, String office=null]) :
		this.id = null,
        this.number = number,
        this.office = office,
		this.user = new User(email, firstName, lastName),
		this.version = 0
	{
		this.user.kind = User.KINDS[User.KIND_STAFF];
	}

	Staff.empty() : super()
	{
		this.user = new User.empty();
		this.user.kind = User.KINDS[User.KIND_STAFF];
	}

	Staff.fromMap(Map<String, dynamic> json)
	{
		this.id = json["id"];
        this.number = json["number"];
        this.office = json["office"];
		this.user = new User.fromMap(json["user"] as Map);
        this.user.kind = User.KINDS[User.KIND_STAFF];
        this.version = json["version"];
	}

	Map<String, dynamic> toMapForCreation()
	{
		Map<String, dynamic> m = {
    		"user": this.user.toMapForCreation(),
        } as Map<String,dynamic>;

        if (this.number != null)	m.putIfAbsent("number", () => this.number);
        if (this.office != null)	m.putIfAbsent("office", () => this.office);

        return m;
	}

	Map<String, dynamic> toMapForUpdate()
    {
    	return {
    		"id": this.id,
			"number": this.number,
			"office": this.office,
			"user": this.user.toMapForUpdate(),
			"version": this.version
    	} as Map<String,dynamic>;
    }

	String toString() => "Staff #$id: ${user.name}";

	/* Requests */
	Future<Empty> delete() {
    	return new Future<Empty>.microtask(() {
        	HttpRequest req = new HttpRequest();
        	req.open("DELETE", SyagesInstance.shared.apiUrl("staff/${user.id}"), async: false);
    		addHeadersToRequest(req, requestHeaders());
    		req.send();
    		if (req.response == null) throw new RequestFailureException();

    		switch(req.status)
   			{
   				case 204:
					return new Empty();
            	case 400:
            		throw new BadRequestException("Erreur ${req.status}:\n\t${req.response}");
   				case 404:
   					throw new NotFoundException("Erreur ${req.status}:\n\t${req.response}");
   				default:
   					throw new UnknownException("Erreur ${req.status}:\n\t${req.response}");
   			}
   		});
   	}

	static Future<Staff> staff(String id) {
    	return new Future<Staff>.microtask(() {
        	HttpRequest req = new HttpRequest();
        	req.open("GET", SyagesInstance.shared.apiUrl("staff/$id"), async: false);
    		addHeadersToRequest(req, requestHeaders());
    		req.send();
    		if (req.response == null) throw new RequestFailureException();

    		switch(req.status)
   			{
   				case 200:
					return new Staff.fromMap(JSON.decode(req.response));
            	case 400:
            		throw new BadRequestException("Erreur ${req.status}:\n\t${req.response}");
   				case 401:
   					throw new UnauthorizedException("Erreur ${req.status}:\n\t${req.response}");
   				default:
   					throw new UnknownException("Erreur ${req.status}:\n\t${req.response}");
   			}
   		});
   	}

	static Future<List<Staff>> staffList([int limit = 25, int offset = 0]) {
		return new Future<List<Staff>>.microtask(() {
    		HttpRequest req = new HttpRequest();
    		req.open("GET", SyagesInstance.shared.apiUrl("staff?limit=$limit&offset=$offset"), async: false);
			addHeadersToRequest(req, requestHeaders());
			req.send();
			if (req.response == null) throw new RequestFailureException();

			switch(req.status)
			{
				case 200:
					List<Map<String,dynamic>> res = JSON.decode(req.response);
					List<Staff> list = new List<Staff>();
					res.forEach((Map<String, dynamic> _) {
						list.add(new Staff.fromMap(_));
					});
					return list;
            	case 400:
            		throw new BadRequestException("Erreur ${req.status}:\n\t${req.response}");
				case 401:
					throw new UnauthorizedException("Erreur ${req.status}:\n\t${req.response}");
				default:
					throw new UnknownException("Erreur ${req.status}:\n\t${req.response}");
			}
		});
	}

	Future<Staff> save() {
		if (id == null) return _save();
		else return _update();
	}

	Future<Staff> _save() {
		return new Future<Staff>.microtask(() {
			HttpRequest req = new HttpRequest();
			req.open("POST", SyagesInstance.shared.apiUrl("staff"), async: false);
			addHeadersToRequest(req, requestHeaders());
			req.send(toJsonString(this.toMapForCreation()));
			if (req.response == null) throw new RequestFailureException();

			switch(req.status)
			{
				case 201:
					return new Staff.fromMap(JSON.decode(req.response));
            	case 400:
            		throw new BadRequestException("Erreur ${req.status}:\n\t${req.response}");
				case 401:
					throw new UnauthorizedException("Erreur ${req.status}:\n\t${req.response}");
				default:
					throw new UnknownException("Erreur ${req.status}:\n\t${req.response}");
			}
		});
	}

	Future<Staff> _update() {
		return new Future<Staff>.microtask(() {
        	HttpRequest req = new HttpRequest();
        	req.open("PUT", SyagesInstance.shared.apiUrl("staff/${user.id}"), async: false);
        	addHeadersToRequest(req, requestHeaders());
        	req.send(toJsonString(this.toMapForUpdate()));
        	if (req.response == null) throw new RequestFailureException();

        	switch(req.status)
        	{
        		case 200:
        			return new Staff.fromMap(JSON.decode(req.response));
            	case 400:
            		throw new BadRequestException("Erreur ${req.status}:\n\t${req.response}");
        		case 401:
        			throw new UnauthorizedException("Erreur ${req.status}:\n\t${req.response}");
        		default:
        			throw new UnknownException("Erreur ${req.status}:\n\t${req.response}");
        	}
        });
	}
}
