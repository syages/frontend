library syages.models.Mark;

import 'dart:async';
import 'dart:convert';
import 'dart:html';
import 'package:polymer/polymer.dart';
import '../Exceptions.dart';
import '../Helpers.dart';
import '../Instance.dart';
import 'Empty.dart';
import 'Evaluation.dart';
import 'Intern.dart';

class Mark extends Observable {

	static final List<String> STATUS = ["Absent", "Excuse", "Present"];

	@observable Evaluation evaluation;
	@observable String id;
	@observable Intern intern;
	@observable double mark;
	@observable String status;
	@observable int version;

	Mark(String id, Intern intern, String status, Evaluation evaluation, [double mark=null]):
		this.evaluation = evaluation,
		this.id = id,
		this.intern = intern,
		this.mark = mark,
		this.status = status,
		this.version = 0;

	Mark.empty() : super()
	{
		this.evaluation = new Evaluation.empty();
		this.intern = new Intern.empty();
	}
	
	Mark.fromIntern(Intern intern): super()
	{
		this.intern = intern;
		this.status = STATUS[2];
		this.mark = 0.0;
	}

	Mark.fromMap(Map<String, dynamic> json):
		this.evaluation = (json["evaluation"] as Map) != null ? new Evaluation.fromMap(json["evaluation"] as Map) : null,
		this.id = json["id"],
		this.intern = Intern.createFromJson(json["intern"] as Map),
		this.mark = json["mark"],
		this.status = json["status"],
		this.version = json["version"];

	Map<String, dynamic> toMapForCreation()
	{
		Map<String, dynamic> m = {
        	"intern" : this.intern.id,
        	"status" : this.status
        } as Map<String,dynamic>;

        if (this.mark != null)	m.putIfAbsent("mark", () => this.mark);

        return m;
	}

	Map<String, dynamic> toMapForUpdate()
    {
    	return {
        	"id" : this.id,
        	"intern" : this.intern.id,
        	"mark" : this.mark,
        	"status" : this.status,
        	"version" : this.version
    	} as Map<String,dynamic>;
    }

	String toString() => "Mark #$id: ${intern.user.name} à eu ${mark} ($status}";

	/* Requests */
	static Future<Mark> getMark(String id) {
    	return new Future<Mark>.microtask(() {
        	HttpRequest req = new HttpRequest();
        	req.open("GET", SyagesInstance.shared.apiUrl("marks/$id"), async: false);
    		addHeadersToRequest(req, requestHeaders());
    		req.send();
    		if (req.response == null) throw new RequestFailureException();

    		switch(req.status)
   			{
   				case 200:
					return new Mark.fromMap(JSON.decode(req.response));
            	case 400:
            		throw new BadRequestException("Erreur ${req.status}:\n\t${req.response}");
   				case 401:
   					throw new UnauthorizedException("Erreur ${req.status}:\n\t${req.response}");
   				default:
   					throw new UnknownException("Erreur ${req.status}:\n\t${req.response}");
   			}
   		});
   	}

	static Future<List<Mark>> markList([int limit = 25, int offset = 0])
	{
		return new Future<List<Mark>>.microtask(() {
    		HttpRequest req = new HttpRequest();
    		req.open("GET", SyagesInstance.shared.apiUrl("marks?limit=$limit&offset=$offset"), async: false);
			addHeadersToRequest(req, requestHeaders());
			req.send();
			if (req.response == null) throw new RequestFailureException();

			switch(req.status)
			{
				case 200:
					List<Map<String,dynamic>> res = JSON.decode(req.response);
					List<Mark> list = new List<Mark>();
					res.forEach((Map<String, dynamic> _) {
						list.add(new Mark.fromMap(_));
					});
					return list;
            	case 400:
            		throw new BadRequestException("Erreur ${req.status}:\n\t${req.response}");
				case 401:
					throw new UnauthorizedException("Erreur ${req.status}:\n\t${req.response}");
				case 404:
					throw new NotFoundException("Erreur ${req.status}:\n\t${req.response}");
				default:
					throw new UnknownException("Erreur ${req.status}:\n\t${req.response}");
			}
		});
	}



	Future<Empty> delete() {
    	return new Future<Empty>.microtask(() {
        	HttpRequest req = new HttpRequest();
        	req.open("DELETE", SyagesInstance.shared.apiUrl("marks/$id"), async: false);
    		addHeadersToRequest(req, requestHeaders(null));
    		req.send();
    		if (req.response == null) throw new RequestFailureException();

    		switch(req.status)
   			{
   				case 204:
					return new Empty();
            	case 400:
            		throw new BadRequestException("Erreur ${req.status}:\n\t${req.response}");
   				case 404:
   					throw new NotFoundException("Erreur ${req.status}:\n\t${req.response}");
   				default:
   					throw new UnknownException("Erreur ${req.status}:\n\t${req.response}");
   			}
   		});
   	}

	Future<Mark> save()
	{
		if (id == null) return _save();
		else return _update();
	}

	Future<Mark> _save()
	{
		return new Future<Mark>.microtask(()
		{
			HttpRequest req = new HttpRequest();
			req.open("POST", SyagesInstance.shared.apiUrl("marks"), async: false);
			addHeadersToRequest(req, requestHeaders());
			req.send(toJsonString(this.toMapForCreation()));
			if (req.response == null) throw new RequestFailureException();

			switch(req.status)
			{
				case 201:
					return new Mark.fromMap(JSON.decode(req.response));
            	case 400:
            		throw new BadRequestException("Erreur ${req.status}:\n\t${req.response}");
				case 401:
					throw new UnauthorizedException("Erreur ${req.status}:\n\t${req.response}");
				default:
					throw new UnknownException("Erreur ${req.status}:\n\t${req.response}");
			}
		});
	}

	Future<Mark> _update()
	{
		return new Future<Mark>.microtask(()
		{
        	HttpRequest req = new HttpRequest();
        	req.open("PUT", SyagesInstance.shared.apiUrl("marks/$id"), async: false);
        	addHeadersToRequest(req, requestHeaders());
        	req.send(toJsonString(this.toMapForUpdate()));
        	if (req.response == null) throw new RequestFailureException();

        	switch(req.status)
        	{
        		case 201:
        			return new Mark.fromMap(JSON.decode(req.response));
            	case 400:
            		throw new BadRequestException("Erreur ${req.status}:\n\t${req.response}");
        		case 401:
        			throw new UnauthorizedException("Erreur ${req.status}:\n\t${req.response}");
        		default:
        			throw new UnknownException("Erreur ${req.status}:\n\t${req.response}");
        	}
        });
	}
}
