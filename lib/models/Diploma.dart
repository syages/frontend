library syages.models.Diploma;

import 'dart:async';
import 'dart:convert';
import 'dart:html';
import 'package:polymer/polymer.dart';
import '../Exceptions.dart';
import '../Helpers.dart';
import '../Instance.dart';
import 'Empty.dart';
import 'Intern.dart';
import 'Lesson.dart';
import 'Period.dart';
import 'Teacher.dart';

class Diploma extends Observable {
	
	static final List<String> STATUS = ["Active", "Inactive"];

	@observable int endYear;
	@observable String id;
	@observable int version;
	@observable String name;
	@observable int startYear;
	@observable String status;
	@observable Teacher supervisor;
	@observable double passMark;
	@observable List<Period> periodList = new List();
	@observable List<Lesson> lessonList = new List();

	Diploma(String id, String name, String status, Teacher supervisor, int startYear, int endYear):
		this.endYear = endYear,
		this.id = id,
		this.name = name,
		this.startYear = startYear,
		this.status = status,
		this.supervisor = supervisor;

	Diploma.empty() : super()
	{
		for(int i = 0; i< 3; i++){
			Period p = new Period.empty();
			p.ordre = i+1;
			this.periodList.add(p);
		}
		this.status = STATUS[0];
	}

	Diploma.fromMap(Map<String, dynamic> json):
		this.endYear =json['endYear'],
		this.id = json["id"],
		this.version = json['version'],
		this.name = json["name"],
		this.status = json['status'],
		this.startYear = json['startYear'],
		this.passMark = json['passMark'] != null ? json['passMark'] * 1.0 : 0.0,
		this.supervisor = Teacher.createFromJson(json["supervisor"] as Map){
		if(json['periods'] != null){
			(json['periods'] as List).forEach((j) => this.periodList.add(new Period.fromMap(j as Map)));
		}
	}

	static Diploma createFromJson(Map<String, dynamic> json){
		if(json==null){
			return new Diploma.empty();
		}
		return new Diploma.fromMap(json);
	}

	Map<String, dynamic> toMapForCreation()
	{
    	Map<String, dynamic> m = {
        	"name" : this.name,
        	"passMark": this.passMark,
        	"supervisor" : this.supervisor.id == null ? this.supervisor.user.id: this.supervisor.id,
        	"status": this.status,
        } as Map<String,dynamic>;
        List ps = new List();
        for(Period p in periodList){
        	ps.add(p.toMapForCreation());
        }
        if(!ps.isEmpty) m.putIfAbsent('periods', () => ps);
        List ls = new List();
        for(Lesson l in lessonList){
        	ls.add(l.toMapForCreation());
        }
        if(!ls.isEmpty) m.putIfAbsent('lessons', () => ls);
        
        return m;
	}

	Map<String, dynamic> toMapForUpdate()
    {
		Map<String, dynamic> m = {
    		"id" : this.id,
        	"name" : this.name,
        	"version" : this.version,
        	"status" : this.status,
        	"passMark" : this.passMark,
        	"supervisor" : this.supervisor.id == null ? this.supervisor.user.id: this.supervisor.id
        } as Map<String,dynamic>;
        List ps = new List();
        for(Period p in periodList){
        	ps.add(p.toMapForUpdate());
        }
        if(!ps.isEmpty) m.putIfAbsent('periods', () => ps);
        List ls = new List();
        for(Lesson l in lessonList){
        	ls.add(l.toMapForUpdate());
        }
        if(!ls.isEmpty) m.putIfAbsent('lessons', () => ls);
        return m;
    }

	String toString() => "Diploma #$id: $name - Promotion $endYear supervisée par ${supervisor.user.name}";

	/* Requests */
	static Future<Diploma> diploma(String id) {
    	return new Future<Diploma>.microtask(() {
        	HttpRequest req = new HttpRequest();
        	req.open("GET", SyagesInstance.shared.apiUrl("diplomas/$id"), async: false);
    		addHeadersToRequest(req, requestHeaders());
    		req.send();
    		if (req.response == null) throw new RequestFailureException();

    		switch(req.status)
   			{
   				case 200:
					return new Diploma.fromMap(JSON.decode(req.response));
            	case 400:
            		throw new BadRequestException("Erreur ${req.status}:\n\t${req.response}");
   				case 401:
   					throw new UnauthorizedException("Erreur ${req.status}:\n\t${req.response}");
   				default:
   					throw new UnknownException("Erreur ${req.status}:\n\t${req.response}");
   			}
   		});
   	}

	static Future<List<Diploma>> diplomaList([int limit = 25, int offset = 0])
	{
		return new Future<List<Diploma>>.microtask(() {
    		HttpRequest req = new HttpRequest();
    		req.open("GET", SyagesInstance.shared.apiUrl("diplomas?limit=$limit&offset=$offset"), async: false);
			addHeadersToRequest(req, requestHeaders());
			req.send();
			if (req.response == null) throw new RequestFailureException();

			switch(req.status)
			{
				case 200:
					List<Map<String,dynamic>> res = JSON.decode(req.response);
					List<Diploma> list = new List<Diploma>();
					res.forEach((Map<String, dynamic> _) {
						list.add(new Diploma.fromMap(_));
					});
					return list;
            	case 400:
            		throw new BadRequestException("Erreur ${req.status}:\n\t${req.response}");
				case 401:
					throw new UnauthorizedException("Erreur ${req.status}:\n\t${req.response}");
				case 404:
					throw new NotFoundException("Erreur ${req.status}:\n\t${req.response}");
				default:
					throw new UnknownException("Erreur ${req.status}:\n\t${req.response}");
			}
		});
	}

	Future<Empty> delete() {
    	return new Future<Empty>.microtask(() {
        	HttpRequest req = new HttpRequest();
        	req.open("DELETE", SyagesInstance.shared.apiUrl("diplomas/$id"), async: false);
    		addHeadersToRequest(req, requestHeaders(null));
    		req.send();
    		if (req.response == null) throw new RequestFailureException();

    		switch(req.status)
   			{
   				case 204:
					return new Empty();
            	case 400:
            		throw new BadRequestException("Erreur ${req.status}:\n\t${req.response}");
   				case 404:
   					throw new NotFoundException("Erreur ${req.status}:\n\t${req.response}");
   				default:
   					throw new UnknownException("Erreur ${req.status}:\n\t${req.response}");
   			}
   		});
   	}

	Future<Diploma> save()
	{
		if (id == null) return _save();
		else return _update();
	}

	Future<Diploma> _save()
	{
		return new Future<Diploma>.microtask(()
		{
			HttpRequest req = new HttpRequest();
			req.open("POST", SyagesInstance.shared.apiUrl("diplomas"), async: false);
			addHeadersToRequest(req, requestHeaders());
			var m = this.toMapForCreation();
			req.send(toJsonString(m));
			if (req.response == null) throw new RequestFailureException();

			switch(req.status)
			{
				case 201:
					return new Diploma.fromMap(JSON.decode(req.response));
            	case 400:
            		throw new BadRequestException("Erreur ${req.status}:\n\t${req.response}");
				case 401:
					throw new UnauthorizedException("Erreur ${req.status}:\n\t${req.response}");
				default:
					throw new UnknownException("Erreur ${req.status}:\n\t${req.response}");
			}
		});
	}

	Future<Diploma> _update()
	{
		return new Future<Diploma>.microtask(()
		{
        	HttpRequest req = new HttpRequest();
        	req.open("PUT", SyagesInstance.shared.apiUrl("diplomas/$id"), async: false);
        	addHeadersToRequest(req, requestHeaders());
        	req.send(toJsonString(this.toMapForUpdate()));
        	if (req.response == null) throw new RequestFailureException();

        	switch(req.status)
        	{
        		case 200:
        			return new Diploma.fromMap(JSON.decode(req.response));
            	case 400:
            		throw new BadRequestException("Erreur ${req.status}:\n\t${req.response}");
        		case 401:
        			throw new UnauthorizedException("Erreur ${req.status}:\n\t${req.response}");
        		default:
        			throw new UnknownException("Erreur ${req.status}:\n\t${req.response}");
        	}
        });
	}

	Future<Diploma> changeStatus(String status)
	{
		return new Future<Diploma>.microtask(()
		{
			HttpRequest req = new HttpRequest();
			req.open("PATCH", SyagesInstance.shared.apiUrl("diplomas/$id/status"), async: false);
			addHeadersToRequest(req, requestHeaders());
			req.send(toJsonString('{"status": "$status" }'));
			if (req.response == null) throw new RequestFailureException();

			switch(req.status)
			{
				case 200:
					return new Diploma.fromMap(JSON.decode(req.response));
            	case 400:
            		throw new BadRequestException("Erreur ${req.status}:\n\t${req.response}");
				case 401:
					throw new UnauthorizedException("Erreur ${req.status}:\n\t${req.response}");
				default:
					throw new UnknownException("Erreur ${req.status}:\n\t${req.response}");
			}
		});
	}

	Future<List<Intern>> internFollower([int limit = 25, int offset = 0])
	{
		return new Future<List<Intern>>.microtask(() {
    		HttpRequest req = new HttpRequest();
    		req.open("GET", SyagesInstance.shared.apiUrl("diplomas/$id/interns"), async: false);
			addHeadersToRequest(req, requestHeaders());
			req.send();
			if (req.response == null) throw new RequestFailureException();

			switch(req.status)
			{
				case 200:
					List<Map<String,dynamic>> res = JSON.decode(req.response);
					List<Intern> list = new List<Intern>();
					res.forEach((Map<String, dynamic> _) {
						list.add(new Intern.fromMap(_));
					});
					return list;
            	case 400:
            		throw new BadRequestException("Erreur ${req.status}:\n\t${req.response}");
				case 401:
					throw new UnauthorizedException("Erreur ${req.status}:\n\t${req.response}");
				case 404:
					throw new NotFoundException("Erreur ${req.status}:\n\t${req.response}");
				default:
					throw new UnknownException("Erreur ${req.status}:\n\t${req.response}");
			}
		});
	}

	Future<List<Lesson>> lessons()
	{
		return new Future<List<Lesson>>.microtask(() {
    		HttpRequest req = new HttpRequest();
    		req.open("GET", SyagesInstance.shared.apiUrl("diplomas/$id/lessons"), async: false);
			addHeadersToRequest(req, requestHeaders());
			req.send();
			if (req.response == null) throw new RequestFailureException();

			switch(req.status)
			{
				case 200:
					List<Map<String,dynamic>> res = JSON.decode(req.response);
					List<Lesson> list = new List<Lesson>();
					res.forEach((Map<String, dynamic> _) {
						list.add(new Lesson.fromMap(_));
					});
					return list;
            	case 400:
            		throw new BadRequestException("Erreur ${req.status}:\n\t${req.response}");
				case 401:
					throw new UnauthorizedException("Erreur ${req.status}:\n\t${req.response}");
				case 404:
					throw new NotFoundException("Erreur ${req.status}:\n\t${req.response}");
				default:
					throw new UnknownException("Erreur ${req.status}:\n\t${req.response}");
			}
		});
	}

	Future<List<Period>> periods()
	{
		return new Future<List<Period>>.microtask(() {
    		HttpRequest req = new HttpRequest();
    		req.open("GET", SyagesInstance.shared.apiUrl("diplomas/$id/periods"), async: false);
			addHeadersToRequest(req, requestHeaders());
			req.send();
			if (req.response == null) throw new RequestFailureException();

			switch(req.status)
			{
				case 200:
					List<Map<String,dynamic>> res = JSON.decode(req.response);
					List<Period> list = new List<Period>();
					res.forEach((Map<String, dynamic> _) {
						list.add(new Period.fromMap(_));
					});
					return list;
				case 401:
					throw new UnauthorizedException("Erreur ${req.status}:\n\t${req.response}");
				case 404:
					throw new NotFoundException("Erreur ${req.status}:\n\t${req.response}");
				default:
					throw new UnknownException("Erreur ${req.status}:\n\t${req.response}");
			}
		});
	}
}
