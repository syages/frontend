library syages.models.NotedAbsence;

import 'dart:async';
import 'dart:convert';
import 'dart:html';
import 'package:polymer/polymer.dart';
import '../Exceptions.dart';
import '../Helpers.dart';
import '../Instance.dart';
import 'Empty.dart';
import 'Intern.dart';
import 'User.dart';

class NotedAbsence extends Observable {

	static final List<String> TIMEOFDAY = ["Afternoon", "Morning"];

	@observable DateTime date;
	@observable String id;
	@observable Intern intern;
	@observable User observerUser;
	@observable String status;
	@observable String timeOfDay;
	@observable int version;

	NotedAbsence(String id, DateTime date, String timeOfDay,String status, Intern intern, User observerUser):
		this.date = date,
		this.id = id,
		this.intern = intern,
		this.observerUser = observerUser,
		this.timeOfDay = timeOfDay,
		this.status = status,
		this.version = 0;

	NotedAbsence.empty() : super()
	{
		this.intern = new Intern.empty();
		this.observerUser = new User.empty();

	}
	
	NotedAbsence.from(NotedAbsence n){
		new NotedAbsence(n.id, n.date, n.timeOfDay, n.status, n.intern, n.observerUser);
	}

	NotedAbsence.fromMap(Map<String, dynamic> json):
		this.date = new DateTime.fromMillisecondsSinceEpoch(int.parse(json['date'])),
		this.id = json["id"],
		this.intern = new Intern.fromMap(json["intern"] as Map),
		this.observerUser = json['observer'] != null ? new User.fromMap(json["observerUser"] as Map) : null,
		this.status = json ["status"],
		this.timeOfDay = json["timeOfDay"],
		this.version = json["version"];

	Map<String, dynamic> toMapForCreation()
	{
    	return {
    		"date" : this.date.millisecondsSinceEpoch,
        	"id" : this.id,
        	"intern" : this.intern.toMapForCreation(),
        	"observerUser" : this.observerUser.toMapForCreation(),
        	"status" : this.status,
        	"timeOfDay" : this.timeOfDay,
        	"version" : this.version
        } as Map<String,dynamic>;
	}

	Map<String, dynamic> toMapForUpdate()
    {
    	return {
    		"date" : this.date.millisecondsSinceEpoch,
        	"id" : this.id,
        	"intern" : this.intern.id,
        	"observerUser" : this.observerUser.toMapForUpdate(),
        	"status" : this.status,
        	"timeOfDay" : this.timeOfDay,
        	"version" : this.version
    	} as Map<String,dynamic>;
    }

	String toString() => "NotedAbsence #$id: ${intern.user.name} absent le $date ($status)";

	/* Requests */
	static Future<NotedAbsence> notedAbsence(String id) {
    	return new Future<NotedAbsence>.microtask(() {
        	HttpRequest req = new HttpRequest();
        	req.open("GET", SyagesInstance.shared.apiUrl("notedAbsences/$id"), async: false);
    		addHeadersToRequest(req, requestHeaders());
    		req.send();
    		if (req.response == null) throw new RequestFailureException();

    		switch(req.status)
   			{
   				case 200:
					return new NotedAbsence.fromMap(JSON.decode(req.response));
            	case 400:
            		throw new BadRequestException("Erreur ${req.status}:\n\t${req.response}");
   				case 401:
   					throw new UnauthorizedException("Erreur ${req.status}:\n\t${req.response}");
   				default:
   					throw new UnknownException("Erreur ${req.status}:\n\t${req.response}");
   			}
   		});
   	}

	static Future<List<NotedAbsence>> notedAbsenceList([int limit = 25, int offset = 0])
	{
		return new Future<List<NotedAbsence>>.microtask(() {
    		HttpRequest req = new HttpRequest();
    		req.open("GET", SyagesInstance.shared.apiUrl("notedAbsences?limit=$limit&offset=$offset"), async: false);
			addHeadersToRequest(req, requestHeaders());
			req.send();
			if (req.response == null) throw new RequestFailureException();

			switch(req.status)
			{
				case 200:
					List<Map<String,dynamic>> res = JSON.decode(req.response);
					List<NotedAbsence> list = new List<NotedAbsence>();
					res.forEach((Map<String, dynamic> _) {
						list.add(new NotedAbsence.fromMap(_));
					});
					return list;
            	case 400:
            		throw new BadRequestException("Erreur ${req.status}:\n\t${req.response}");
				case 401:
					throw new UnauthorizedException("Erreur ${req.status}:\n\t${req.response}");
				case 404:
					throw new NotFoundException("Erreur ${req.status}:\n\t${req.response}");
				default:
					throw new UnknownException("Erreur ${req.status}:\n\t${req.response}");
			}
		});
	}



	Future<Empty> delete() {
    	return new Future<Empty>.microtask(() {
        	HttpRequest req = new HttpRequest();
        	req.open("DELETE", SyagesInstance.shared.apiUrl("notedAbsences/$id"), async: false);
    		addHeadersToRequest(req, requestHeaders(null));
    		req.send();
    		if (req.response == null) throw new RequestFailureException();

    		switch(req.status)
   			{
   				case 204:
					return new Empty();
            	case 400:
            		throw new BadRequestException("Erreur ${req.status}:\n\t${req.response}");
   				case 404:
   					throw new NotFoundException("Erreur ${req.status}:\n\t${req.response}");
   				default:
   					throw new UnknownException("Erreur ${req.status}:\n\t${req.response}");
   			}
   		});
   	}

	Future<NotedAbsence> save()
	{
		if (id == null) return _save();
		else return _update();
	}

	Future<NotedAbsence> _save()
	{
		return new Future<NotedAbsence>.microtask(()
		{
			HttpRequest req = new HttpRequest();
			req.open("POST", SyagesInstance.shared.apiUrl("notedAbsences"), async: false);
			addHeadersToRequest(req, requestHeaders());
			req.send(toJsonString(this.toMapForCreation()));
			if (req.response == null) throw new RequestFailureException();

			switch(req.status)
			{
				case 201:
					return new NotedAbsence.fromMap(JSON.decode(req.response));
            	case 400:
            		throw new BadRequestException("Erreur ${req.status}:\n\t${req.response}");
				case 401:
					throw new UnauthorizedException("Erreur ${req.status}:\n\t${req.response}");
				default:
					throw new UnknownException("Erreur ${req.status}:\n\t${req.response}");
			}
		});
	}

	Future<NotedAbsence> _update()
	{
		return new Future<NotedAbsence>.microtask(()
		{
        	HttpRequest req = new HttpRequest();
        	req.open("PUT", SyagesInstance.shared.apiUrl("notedAbsences/$id"), async: false);
        	addHeadersToRequest(req, requestHeaders());
        	req.send(toJsonString(this.toMapForUpdate()));
        	if (req.response == null) throw new RequestFailureException();

        	switch(req.status)
        	{
        		case 201:
        			return new NotedAbsence.fromMap(JSON.decode(req.response));
            	case 400:
            		throw new BadRequestException("Erreur ${req.status}:\n\t${req.response}");
        		case 401:
        			throw new UnauthorizedException("Erreur ${req.status}:\n\t${req.response}");
        		default:
        			throw new UnknownException("Erreur ${req.status}:\n\t${req.response}");
        	}
        });
	}
}
