library syages.models.Report;

import 'dart:async';
import 'dart:convert';
import 'dart:html';
import 'package:polymer/polymer.dart';
import '../\$Cache.dart';
import '../Exceptions.dart';
import '../Helpers.dart';
import '../Instance.dart';
import 'User.dart';

class Report extends Observable {
	static final int PRIORITY_COMMON = 0;
	static final int PRIORITY_NORMAL = 1;
	static final int PRIORITY_CRITICAL = 2;
	static final List<String> PRIORITIES = ["Critical", "Minor", "Normal"];

	static final int STATUS_CLOSED = 0;
	static final int STATUS_OPENED = 1;
	static final List<String> STATUSES = ["Closed", "Opened"];

	@observable String context;
	@observable String controller;
	@observable String commentary;
	@observable DateTime createdAt;
	@observable String id;
	@observable String priority;
	@observable String route;
	@observable String status; // true si "opened", false sinon
	@observable String type;
	@observable User user;
	@observable String userInfo;
	@observable int version;

	Report.bug(int priority, String commentary) :
		this.context = toJsonString(new SyagesInstance().applicationController.controller == null ? null : new SyagesInstance().applicationController.controller.serializedState()),
		this.controller = SyagesInstance.shared.applicationController.currentContext == null ? null : SyagesInstance.shared.applicationController.currentContext.configuration.controller,
		this.commentary = commentary,
		this.createdAt = null,
		this.id = null,
		this.priority = PRIORITIES[priority],
		this.route = window.location.hash,
		this.status = STATUSES[1],
		this.type = "Bug",
		this.user = $Cache.sharedInstance.user,
		this.userInfo = toJsonString(gatherUserConfiguration()),
		this.version = null;

	Report.empty() : super()
	{
		this.user = new User.empty();
	}

	Report.message(String message) :
    	this.context = toJsonString(new SyagesInstance().applicationController.controller == null ? null : new SyagesInstance().applicationController.controller.serializedState()),
    	this.controller = SyagesInstance.shared.applicationController.currentContext == null ? null : SyagesInstance.shared.applicationController.currentContext.configuration.controller,
    	this.commentary = message,
    	this.createdAt = null,
    	this.id = null,
    	this.priority = null,
    	this.route = window.location.hash,
    	this.status = STATUSES[1],
    	this.type = "Message",
    	this.user = $Cache.sharedInstance.user,
    	this.userInfo = toJsonString(gatherUserConfiguration()),
    	this.version = null;

	Report.fromMap(Map<String, dynamic> values)
	{
		this.context = values["context"];
		this.controller = values["controller"];
		this.commentary = values["commentary"];
		this.createdAt = values["createdAt"] == null ? null : new DateTime.fromMillisecondsSinceEpoch(values["createdAt"]);
		this.id = values["id"];
		this.priority = PRIORITIES.contains(values["priority"]) ? values["priority"] : null;
		this.route = values["route"];
		this.status = values["status"];
		this.type = values["type"];
		this.userInfo = values["userInfo"];
		this.version = null;

		dynamic user = values['user'];
		if(user is User) this.user = user;
		else if(user is Map) this.user = new User.fromMap(user);
		else if(user is String) {
			Map<String, dynamic> map = JSON.decode(user);
			this.user = new User.fromMap(map);
		} else this.user = null;
	}

	Map<String, dynamic> toMapForCreation() {
		Map<String,String> m = {
			"context": this.context,
			"controller": this.controller,
			"commentary": this.commentary,
			"route": this.route,
			"type": this.type,
			"userInfo": this.userInfo
		} as Map<String,String>;
		if (this.priority != null)
			m.putIfAbsent("priority", () => this.priority);
		return m;
	}

	Map<String, dynamic> toMapForUpdate()
    {
    	return {
    		"context" : this.context,
        	"controller" : this.controller,
        	"commentary" : this.commentary,
        	"createdAt" : this.createdAt.toString(),
        	"id" : this.id,
        	"priority" : this.priority,
        	"route" : this.route,
        	"status" : this.status,
        	"type" : this.type,
        	"user" : this.user.toMapForUpdate(),
        	"userInfo" : this.userInfo,
        	"version" : this.version
    	} as Map<String,dynamic>;
    }

	String toString() => "Report #$id: $type ($status) sur $controller";

	/* Requests */
	Future<Report> changeStatus(bool close) {
		return new Future<Report>.microtask(() {
			HttpRequest req = new HttpRequest();
			req.open("PATCH", SyagesInstance.shared.apiUrl("reports/${id}"), async: false);
			addHeadersToRequest(req, requestHeaders());
			req.send('{"status": ${close ? '"Closed"' : '"Opened"'}}');
			if (req.response == null) throw new RequestFailureException();
			
			switch(req.status)
			{
				case 200:
					return new Report.fromMap(JSON.decode(req.response));
            	case 400:
            		throw new BadRequestException("Erreur ${req.status}:\n\t${req.response}");
				case 401:
					throw new UnauthorizedException("Erreur ${req.status}:\n\t${req.response}");
				default:
					throw new UnknownException("Erreur ${req.status}:\n\t${req.response}");
			}
		});
	}
	
	static Future<List<Report>> closedReportsList([int limit = 25, int offset = 0]) {
    	return new Future<List<Report>>.microtask(() {
    		HttpRequest req = new HttpRequest();
    		req.open("GET", SyagesInstance.shared.apiUrl("reports/closed?limit=$limit&offset=$offset"), async: false);
			addHeadersToRequest(req, requestHeaders());
			req.send();
			if (req.response == null) throw new RequestFailureException();

			switch(req.status)
			{
				case 200:
					List<Map<String,dynamic>> res = JSON.decode(req.response);
					List<Report> list = new List<Report>();
					res.forEach((Map<String, dynamic> _) {
						list.add(new Report.fromMap(_));
					});
					return list;
            	case 400:
            		throw new BadRequestException("Erreur ${req.status}:\n\t${req.response}");
				case 401:
					throw new UnauthorizedException("Erreur ${req.status}:\n\t${req.response}");
				default:
					throw new UnknownException("Erreur ${req.status}:\n\t${req.response}");
			}
		});
    }

	static Future<List<Report>> openedReportsList([int limit = 25, int offset = 0]) {
    	return new Future<List<Report>>.microtask(() {
    		HttpRequest req = new HttpRequest();
    		req.open("GET", SyagesInstance.shared.apiUrl("reports/opened?limit=$limit&offset=$offset"), async: false);
			addHeadersToRequest(req, requestHeaders());
			req.send();
			if (req.response == null) throw new RequestFailureException();

			switch(req.status)
			{
				case 200:
					List<Map<String,dynamic>> res = JSON.decode(req.response);
					List<Report> list = new List<Report>();
					res.forEach((Map<String, dynamic> _) {
						list.add(new Report.fromMap(_));
					});
					return list;
            	case 400:
            		throw new BadRequestException("Erreur ${req.status}:\n\t${req.response}");
				case 401:
					throw new UnauthorizedException("Erreur ${req.status}:\n\t${req.response}");
				default:
					throw new UnknownException("Erreur ${req.status}:\n\t${req.response}");
			}
		});
    }

	static Future<Report> report(String id) {
        return new Future<Report>.microtask(() {
       		HttpRequest req = new HttpRequest();
       		req.open("GET", SyagesInstance.shared.apiUrl("reports/$id"), async: false);
    		addHeadersToRequest(req, requestHeaders());
    		req.send();
    		if (req.response == null) throw new RequestFailureException();

    		switch(req.status)
    		{
    			case 200:
    				return new Report.fromMap(JSON.decode(req.response) as Map<String, dynamic>);
            	case 400:
            		throw new BadRequestException("Erreur ${req.status}:\n\t${req.response}");
    			case 401:
    				throw new UnauthorizedException("Erreur ${req.status}:\n\t${req.response}");
    			default:
    				throw new UnknownException("Erreur ${req.status}:\n\t${req.response}");
    		}
		});
	}

	static Future<List<Report>> reportsList([int limit = 25, int offset = 0]) {
		return new Future<List<Report>>.microtask(() {
            HttpRequest req = new HttpRequest();
            req.open("GET", SyagesInstance.shared.apiUrl("reports?limit=$limit&offset=$offset"), async: false);
        	addHeadersToRequest(req, requestHeaders());
        	req.send();
        	if (req.response == null) throw new RequestFailureException();

        	switch(req.status)
        	{
        		case 200:
        			List<Map<String,dynamic>> res = JSON.decode(req.response);
        			List<Report> list = new List<Report>();
        			res.forEach((Map<String, dynamic> _) {
        				list.add(new Report.fromMap(_));
        			});
        			return list;
            	case 400:
            		throw new BadRequestException("Erreur ${req.status}:\n\t${req.response}");
        		case 401:
        			throw new UnauthorizedException("Erreur ${req.status}:\n\t${req.response}");
        		default:
        			throw new UnknownException("Erreur ${req.status}:\n\t${req.response}");
        	}
		});
	}

	Future<Report> save() {
		return new Future<Report>.microtask(() {
			HttpRequest req = new HttpRequest();
            req.open("POST", SyagesInstance.shared.apiUrl("reports"), async: false);
            addHeadersToRequest(req, requestHeaders());
            String data = toJsonString(this.toMapForCreation());
            req.send(data);
            if (req.response == null) throw new RequestFailureException();

            switch(req.status)
			{
				case 201:
					return new Report.fromMap(JSON.decode(req.response) as Map<String, dynamic>);
            	case 400:
            		throw new BadRequestException("Erreur ${req.status}:\n\t${req.response}");
				case 401:
					throw new UnauthorizedException("Erreur ${req.status}:\n\t${req.response}");
				default:
					throw new UnknownException("Erreur ${req.status}:\n\t${req.response}");
			}
		});
	}
	
	Future delete() {
		return new Future.microtask(() {
			HttpRequest req = new HttpRequest();
			req.open("DELETE", SyagesInstance.shared.apiUrl("reports/$id"), async: false);
			addHeadersToRequest(req, requestHeaders(null));
			String data = toJsonString(this.toMapForCreation());
			req.send(data);
			if (req.response == null) throw new RequestFailureException();
			
			switch(req.status)
			{
				case 204:
					return false;
				default:
					throw new UnknownException("Erreur ${req.status}:\n\t${req.response}");
			}
		});
	}
}