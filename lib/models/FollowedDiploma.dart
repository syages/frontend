library syages.models.FollowedDiploma;

import 'dart:html';
import 'dart:convert';
import 'dart:async';
import 'package:polymer/polymer.dart';
import 'Lesson.dart';
import 'Diploma.dart';

enum Status{
		Undergoing,
		Failed,
		Validated
	}

class FollowedDiploma extends Observable{
	
	
	
	@observable Diploma diploma;
	@observable List<Lesson> optionalLessons = [null, null];
	@observable String id;
	@observable int version;
	@observable bool paid;
	@observable Status status;

	FollowedDiploma(Diploma d, List<Lesson> lessons){
		this.diploma = d;
		this.optionalLessons = lessons;
	}
	
	FollowedDiploma.empty(){
		this.paid = false;
	}
	
	FollowedDiploma.fromMap(Map<String, dynamic> json){
		this.diploma = Diploma.createFromJson(json['diploma'] as Map);
		this.optionalLessons = new List();
		for(Map<String, dynamic> elem in (json['optionalLessons'] as List)){
			this.optionalLessons.add(Lesson.createFromJson(elem));
		}
		this.id = json['id'];
		this.version = json['version'];
		this.paid = json['paid'];
		switch(json['status']){
			case 'Undergoing':
				this.status = Status.Undergoing;
				break;
			case 'Failed':
				this.status = Status.Failed;
				break;
			case 'Validated':
				this.status = Status.Validated;
				break;
			default:
				this.status = null;
		}
	}
	
	Map<String, dynamic> toMapForCreation(){
		Map<String, dynamic> m = {
    		"paid" : this.paid
        } as Map<String,dynamic>;
		m.putIfAbsent('diploma', () => this.diploma.id);
		List<String> idLessons = new List();
		for(Lesson l in this.optionalLessons){
			idLessons.add(l.id);
		}
		m.putIfAbsent('optionalLessons', () => idLessons);
		return m;
	}
	
	Map<String, dynamic> toMapForUpdate(){
		Map<String, dynamic> m = {
    		"paid" : this.paid,
    		'id': this.id,
    		'version': this.version
        } as Map<String,dynamic>;
		m.putIfAbsent('diploma', () => this.diploma.id);
		List<String> idLessons = new List();
		for(Lesson l in this.optionalLessons){
			idLessons.add(l.id);
		}
		m.putIfAbsent('optionalLessons', () => idLessons);
		return m;
	}
	
}