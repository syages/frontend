library syages.models.Course;

import 'dart:async';
import 'dart:convert';
import 'dart:html';
import 'package:polymer/polymer.dart';
import '../Exceptions.dart';
import '../Helpers.dart';
import '../Instance.dart';
import 'Domain.dart';
import 'Empty.dart';

class Course extends Observable {
	@observable Domain domain;
	@observable String id;
	@observable String name;
	@observable int version;

	Course(String name, [Domain domain=null]) :
        this.domain = domain,
		this.id = null,
		this.name = name,
		this.version = 0;

	Course.empty() : super()
	{
		this.domain = new Domain.empty();
	}

	Course.fromMap(Map<String, dynamic> json)
	{
		this.domain = Domain.createFromJson(json["domain"] as Map);
		this.id = json["id"];
		this.name = json["name"];
		this.version = json["version"];
	}

	static Course createFromJson(Map<String, dynamic> json){
		if(json == null){
			return new Course.empty();
		}
		return new Course.fromMap(json);
	}

	Map<String, dynamic> toMapForCreation()
	{
		Map<String, dynamic> m = {
    		"name": this.name
        } as Map<String,dynamic>;

        if (this.domain != null)	m.putIfAbsent("domain", () => this.domain.toMapForCreation());

        return m;
	}

	Map<String, dynamic> toMapForUpdate()
    {
    	return {
    		"id": this.id,
			"domain": this.domain.id,
			"name": this.name,
			"version": this.version
    	} as Map<String,dynamic>;
    }

	String toString() => "Course #$id: $name [$domain]";


	/* Requests */
	static Future<Course> course(String id)
	{
		return new Future<Course>.microtask(()
		{
			HttpRequest req = new HttpRequest();
			req.open("GET", SyagesInstance.shared.apiUrl("courses/$id"), async: false);
			addHeadersToRequest(req, requestHeaders());
			req.send();
			if (req.response == null) throw new RequestFailureException();

			switch(req.status)
			{
				case 200:
					return new Course.fromMap(JSON.decode(req.response));
            	case 400:
            		throw new BadRequestException("Erreur ${req.status}:\n\t${req.response}");
				case 401:
					throw new UnauthorizedException("Erreur ${req.status}:\n\t${req.response}");
				default:
					throw new UnknownException("Erreur ${req.status}:\n\t${req.response}");
			}
		});
	}

	static Future<List<Course>> courseList([int limit = 25, int offset = 0])
	{
		return new Future<List<Course>>.microtask(()
		{
			HttpRequest req = new HttpRequest();
    		req.open("GET", SyagesInstance.shared.apiUrl("courses?limit=$limit&offset=$offset"), async: false);
			addHeadersToRequest(req, requestHeaders());
			req.send();
			if (req.response == null) throw new RequestFailureException();

			switch(req.status)
			{
				case 200:
					List<Map<String,dynamic>> res = JSON.decode(req.response);
					List<Course> list = new List<Course>();
					res.forEach((Map<String, dynamic> _) {
						list.add(new Course.fromMap(_));
					});
					return list;
            	case 400:
            		throw new BadRequestException("Erreur ${req.status}:\n\t${req.response}");
				case 401:
					throw new UnauthorizedException("Erreur ${req.status}:\n\t${req.response}");
				case 404:
					throw new NotFoundException("Erreur ${req.status}:\n\t${req.response}");
				default:
					throw new UnknownException("Erreur ${req.status}:\n\t${req.response}");
			}
		});
	}

	Future<Empty> delete() {
    	return new Future<Empty>.microtask(() {
        	HttpRequest req = new HttpRequest();
        	req.open("DELETE", SyagesInstance.shared.apiUrl("courses/$id"), async: false);
    		addHeadersToRequest(req, requestHeaders(null));
    		req.send();
    		if (req.response == null) throw new RequestFailureException();

    		switch(req.status)
   			{
   				case 204:
					return new Empty();
            	case 400:
            		throw new BadRequestException("Erreur ${req.status}:\n\t${req.response}");
   				case 404:
   					throw new NotFoundException("Erreur ${req.status}:\n\t${req.response}");
   				default:
   					throw new UnknownException("Erreur ${req.status}:\n\t${req.response}");
   			}
   		});
   	}

	Future<Course> save()
	{
		if (id == null) return _save();
		else return _update();
	}

	Future<Course> _save()
	{
		return new Future<Course>.microtask(()
		{
			HttpRequest req = new HttpRequest();
			req.open("POST", SyagesInstance.shared.apiUrl("courses"), async: false);
			addHeadersToRequest(req, requestHeaders());
			req.send(toJsonString(this.toMapForCreation()));
			if (req.response == null) throw new RequestFailureException();

			switch(req.status)
			{
				case 201:
					return new Course.fromMap(JSON.decode(req.response));
            	case 400:
            		throw new BadRequestException("Erreur ${req.status}:\n\t${req.response}");
				case 401:
					throw new UnauthorizedException("Erreur ${req.status}:\n\t${req.response}");
				default:
					throw new UnknownException("Erreur ${req.status}:\n\t${req.response}");
			}
		});
	}

	Future<Course> _update()
	{
		return new Future<Course>.microtask(()
		{
			HttpRequest req = new HttpRequest();
			req.open("PUT", SyagesInstance.shared.apiUrl("courses/$id"), async: false);
			addHeadersToRequest(req, requestHeaders());
			req.send(toJsonString(this.toMapForUpdate()));
			if (req.response == null) throw new RequestFailureException();

			switch(req.status)
			{
				case 200:
					return new Course.fromMap(JSON.decode(req.response));
            	case 400:
            		throw new BadRequestException("Erreur ${req.status}:\n\t${req.response}");
				case 401:
					throw new UnauthorizedException("Erreur ${req.status}:\n\t${req.response}");
				default:
					throw new UnknownException("Erreur ${req.status}:\n\t${req.response}");
			}
		});
	}
}
