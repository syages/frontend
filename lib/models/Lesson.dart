library syages.models.Lesson;

import 'dart:async';
import 'dart:convert';
import 'dart:html';
import 'package:polymer/polymer.dart';
import '../Exceptions.dart';
import '../Helpers.dart';
import '../Instance.dart';
import 'Course.dart';
import 'Diploma.dart';
import 'Evaluation.dart';
import 'Intern.dart';
import 'Lecture.dart';
import 'Teacher.dart';

class Lesson extends Observable {
	@observable double coefficient;
	@observable Course course;
	@observable Diploma diploma;
	@observable String id;
	@observable bool optional;
	@observable double passMark;
	@observable Teacher teacher;
	@observable int version;

	Lesson(String id, Course course, Diploma diploma, double coefficient, bool optional,
			double passMark, [Teacher teacher=null]) :
		this.coefficient = coefficient,
		this.course = course,
		this.diploma = diploma,
		this.id = id,
		this.optional = optional,
		this.passMark = passMark,
		this.teacher = teacher;

	Lesson.empty() : super()
	{
		this.course = new Course.empty();
		this.teacher = new Teacher.empty();
		this.optional = false;
	}

	Lesson.fromMap(Map<String, dynamic> json) :
		this.coefficient = json['coefficient'] != null ? json['coefficient'] * 1.0 : 0.0,
        this.course = Course.createFromJson(json["course"] as Map),
        this.diploma = Diploma.createFromJson(json["diploma"] as Map),
        this.id = json["id"],
        this.optional = json["optional"],
        this.passMark = json['passMark'] != null ? json['passMark'] * 1.0: 0.0,
        this.teacher = Teacher.createFromJson(json["teacher"] as Map),
        this.version = json["version"];
	
	static Lesson createFromJson(Map<String, dynamic> json){
		if(json==null){
			return new Lesson.empty();
		}
		return new Lesson.fromMap(json);
	}

	Map<String, dynamic> toMapForCreation()
	{
		Map<String, dynamic> m = {
    		"coefficient" : this.coefficient,
        	"course" : this.course.id,
        	"teacher": this.teacher.id == null ? this.teacher.user.id : this.teacher.id,
        	"optional" : this.optional,
        	"passMark" : this.passMark
        } as Map<String,dynamic>;

        return m;
	}

	Map<String, dynamic> toMapForUpdate()
    {
    	return {
    		"coefficient" : this.coefficient,
        	"course" : this.course.id,
        	"id" : this.id,
        	"optional" : this.optional,
        	"passMark" : this.passMark,
        	"teacher" : this.teacher.id == null ? this.teacher.user.id : this.teacher.id,
        	"version" : this.version
    	} as Map<String,dynamic>;
    }

	String toString() => "Lesson #$id: ${course.name} par ${teacher.user.name}";

	/* Requests */
	static Future<Lesson> lesson(String id) {
    	return new Future<Lesson>.microtask(() {
        	HttpRequest req = new HttpRequest();
        	req.open("GET", SyagesInstance.shared.apiUrl("lessons/$id"), async: false);
    		addHeadersToRequest(req, requestHeaders());
    		req.send();
    		if (req.response == null) throw new RequestFailureException();

    		switch(req.status)
   			{
   				case 200:
					return new Lesson.fromMap(JSON.decode(req.response));
            	case 400:
            		throw new BadRequestException("Erreur ${req.status}:\n\t${req.response}");
   				case 401:
   					throw new UnauthorizedException("Erreur ${req.status}:\n\t${req.response}");
   				default:
   					throw new UnknownException("Erreur ${req.status}:\n\t${req.response}");
   			}
   		});
   	}

	static Future<List<Lesson>> lessonList([int limit = 25, int offset = 0])
	{
		return new Future<List<Lesson>>.microtask(() {
    		HttpRequest req = new HttpRequest();
    		req.open("GET", SyagesInstance.shared.apiUrl("lessons?limit=$limit&offset=$offset"), async: false);
			addHeadersToRequest(req, requestHeaders());
			req.send();
			if (req.response == null) throw new RequestFailureException();

			switch(req.status)
			{
				case 200:
					List<Map<String,dynamic>> res = JSON.decode(req.response);
					List<Lesson> list = new List<Lesson>();
					res.forEach((Map<String, dynamic> _) {
						list.add(new Lesson.fromMap(_));
					});
					return list;
            	case 400:
            		throw new BadRequestException("Erreur ${req.status}:\n\t${req.response}");
				case 401:
					throw new UnauthorizedException("Erreur ${req.status}:\n\t${req.response}");
				case 404:
					throw new NotFoundException("Erreur ${req.status}:\n\t${req.response}");
				default:
					throw new UnknownException("Erreur ${req.status}:\n\t${req.response}");
			}
		});
	}

	Future<List<Evaluation>> evaluations([int limit = 25, int offset = 0])
    {
    	return new Future<List<Evaluation>>.microtask(() {
       		HttpRequest req = new HttpRequest();
       		req.open("GET", SyagesInstance.shared.apiUrl("lessons/$id/evaluations?limit=$limit&offset=$offset"), async: false);
    		addHeadersToRequest(req, requestHeaders());
    		req.send();
    		if (req.response == null) throw new RequestFailureException();

    		switch(req.status)
    		{
    			case 200:
    				List<Map<String,dynamic>> res = JSON.decode(req.response);
    				List<Evaluation> list = new List<Evaluation>();
    				res.forEach((Map<String, dynamic> _) {
    					list.add(new Evaluation.fromMap(_));
    				});
    				return list;
            	case 400:
            		throw new BadRequestException("Erreur ${req.status}:\n\t${req.response}");
    			case 401:
    				throw new UnauthorizedException("Erreur ${req.status}:\n\t${req.response}");
    			case 404:
    				throw new NotFoundException("Erreur ${req.status}:\n\t${req.response}");
    			default:
    				throw new UnknownException("Erreur ${req.status}:\n\t${req.response}");
    		}
    	});
	}

	Future<List<Intern>> interns([int limit = 25, int offset = 0])
    {
    	return new Future<List<Intern>>.microtask(() {
       		HttpRequest req = new HttpRequest();
       		req.open("GET", SyagesInstance.shared.apiUrl("lessons/$id/interns?limit=$limit&offset=$offset"), async: false);
    		addHeadersToRequest(req, requestHeaders());
    		req.send();
    		if (req.response == null) throw new RequestFailureException();

    		switch(req.status)
    		{
    			case 200:
    				List<Map<String,dynamic>> res = JSON.decode(req.response);
    				List<Intern> list = new List<Intern>();
    				res.forEach((Map<String, dynamic> _) {
    					list.add(new Intern.fromMap(_));
    				});
    				return list;
            	case 400:
            		throw new BadRequestException("Erreur ${req.status}:\n\t${req.response}");
    			case 401:
    				throw new UnauthorizedException("Erreur ${req.status}:\n\t${req.response}");
    			case 404:
    				throw new NotFoundException("Erreur ${req.status}:\n\t${req.response}");
    			default:
    				throw new UnknownException("Erreur ${req.status}:\n\t${req.response}");
    		}
    	});
	}

	Future<List<Lecture>> lectures([int limit = 25, int offset = 0])
    {
    	return new Future<List<Lecture>>.microtask(() {
       		HttpRequest req = new HttpRequest();
       		req.open("GET", SyagesInstance.shared.apiUrl("lessons/$id/lectures?limit=$limit&offset=$offset"), async: false);
    		addHeadersToRequest(req, requestHeaders());
    		req.send();
    		if (req.response == null) throw new RequestFailureException();

    		switch(req.status)
    		{
    			case 200:
    				List<Map<String,dynamic>> res = JSON.decode(req.response);
    				List<Lecture> list = new List<Lecture>();
    				res.forEach((Map<String, dynamic> _) {
    					list.add(new Lecture.fromMap(_));
    				});
    				return list;
            	case 400:
            		throw new BadRequestException("Erreur ${req.status}:\n\t${req.response}");
    			case 401:
    				throw new UnauthorizedException("Erreur ${req.status}:\n\t${req.response}");
    			case 404:
    				throw new NotFoundException("Erreur ${req.status}:\n\t${req.response}");
    			default:
    				throw new UnknownException("Erreur ${req.status}:\n\t${req.response}");
    		}
    	});
	}

	Future<Lesson> save()
	{
		if (id == null) return _save();
		else return _update();
	}

	Future<Lesson> _save()
	{
		return new Future<Lesson>.microtask(()
		{
			HttpRequest req = new HttpRequest();
			req.open("POST", SyagesInstance.shared.apiUrl("lessons"), async: false);
			addHeadersToRequest(req, requestHeaders());
			req.send(toJsonString(this.toMapForCreation()));
			if (req.response == null) throw new RequestFailureException();

			switch(req.status)
			{
				case 201:
					return new Lesson.fromMap(JSON.decode(req.response));
            	case 400:
            		throw new BadRequestException("Erreur ${req.status}:\n\t${req.response}");
				case 401:
					throw new UnauthorizedException("Erreur ${req.status}:\n\t${req.response}");
				default:
					throw new UnknownException("Erreur ${req.status}:\n\t${req.response}");
			}
		});
	}

	Future<Lesson> _update()
	{
		return new Future<Lesson>.microtask(()
		{
        	HttpRequest req = new HttpRequest();
        	req.open("PUT", SyagesInstance.shared.apiUrl("lessons/$id"), async: false);
        	addHeadersToRequest(req, requestHeaders());
        	req.send(toJsonString(this.toMapForUpdate()));
        	if (req.response == null) throw new RequestFailureException();

        	switch(req.status)
        	{
        		case 201:
        			return new Lesson.fromMap(JSON.decode(req.response));
            	case 400:
            		throw new BadRequestException("Erreur ${req.status}:\n\t${req.response}");
        		case 401:
        			throw new UnauthorizedException("Erreur ${req.status}:\n\t${req.response}");
        		default:
        			throw new UnknownException("Erreur ${req.status}:\n\t${req.response}");
        	}
        });
	}
}
