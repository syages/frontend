library syages.models.President;

import 'dart:async';
import 'dart:convert';
import 'dart:html';
import 'package:polymer/polymer.dart';
import '../Exceptions.dart';
import '../Helpers.dart';
import '../Instance.dart';
import 'Empty.dart';
import 'User.dart';

class President extends Observable {
	@observable int day;
	@observable String id;
	@observable String hour;
	@observable String office;
	@observable String number;
	@observable int recurrence;
	@observable User user;
	@observable int version;

	President(User user, [int day=null, String hour=null, String office=null,
								String number=null, int recurrence=null]) :
		this.day = day,
		this.id = null,
		this.hour = hour,
		this.office = office,
		this.number = number,
		this.recurrence = recurrence,
		this.user = user,
		this.version = 0;

	President.empty() : super()
	{
		this.user = new User.empty();
		this.user.kind = User.KINDS[User.KIND_PRESIDENT];
	}

	President.fromMap(Map<String, dynamic> json)
	{
		this.day = json["day"];
		this.id = json["id"];
		this.hour = json["hour"];
		this.office = json["office"];
		this.number = json["number"];
		this.recurrence = json["recurrence"];
		this.user = new User.fromMap(json["user"]);
		this.version = json["version"];
	}

	Map<String, dynamic> toMapForCreation()
	{
		Map<String, dynamic> m = {
        	"user" : this.user.toMapForCreation()
        } as Map<String,dynamic>;

        if (this.day != null)	m.putIfAbsent("day", () => this.day);
        if (this.hour != null)	m.putIfAbsent("hour", () => this.hour);
        if (this.office != null)	m.putIfAbsent("office", () => this.office);
        if (this.number != null)	m.putIfAbsent("number", () => this.number);
        if (this.recurrence != null)	m.putIfAbsent("recurrence", () => this.recurrence);

        return m;
	}

	Map<String, dynamic> toMapForUpdate()
    {
    	return {
    		"day" : this.day,
        	"id" : this.id,
        	"hour" : this.hour,
        	"office" : this.office,
        	"number" : this.number,
        	"recurrence" : this.recurrence,
        	"user" : this.user.toMapForUpdate(),
        	"version" : this.version
    	} as Map<String,dynamic>;
    }

	String toString() => "President #${user.id}: ${user.name}, office[$office], number[$number]";

	static Future<President> president(String id) {
    	return new Future<President>.microtask(() {
        	HttpRequest req = new HttpRequest();
        	req.open("GET", SyagesInstance.shared.apiUrl("presidents/$id"), async: false);
    		addHeadersToRequest(req, requestHeaders());
    		req.send();
    		if (req.response == null) throw new RequestFailureException();

    		switch(req.status)
   			{
   				case 200:
					return new President.fromMap(JSON.decode(req.response));
            	case 400:
            		throw new BadRequestException("Erreur ${req.status}:\n\t${req.response}");
   				case 401:
   					throw new UnauthorizedException("Erreur ${req.status}:\n\t${req.response}");
   				default:
   					throw new UnknownException("Erreur ${req.status}:\n\t${req.response}");
   			}
   		});
   	}

	static Future<List<President>> presidentList([int limit = 25, int offset = 0])
	{
		return new Future<List<President>>.microtask(() {
    		HttpRequest req = new HttpRequest();
    		req.open("GET", SyagesInstance.shared.apiUrl("presidents?limit=$limit&offset=$offset"), async: false);
			addHeadersToRequest(req, requestHeaders());
			req.send();
			if (req.response == null) throw new RequestFailureException();

			switch(req.status)
			{
				case 200:
					List<Map<String,dynamic>> res = JSON.decode(req.response);
					List<President> list = new List<President>();
					res.forEach((Map<String, dynamic> _) {
						list.add(new President.fromMap(_));
					});
					return list;
            	case 400:
            		throw new BadRequestException("Erreur ${req.status}:\n\t${req.response}");
				case 401:
					throw new UnauthorizedException("Erreur ${req.status}:\n\t${req.response}");
				case 404:
					throw new NotFoundException("Erreur ${req.status}:\n\t${req.response}");
				default:
					throw new UnknownException("Erreur ${req.status}:\n\t${req.response}");
			}
		});
	}



	Future<Empty> delete() {
    	return new Future<Empty>.microtask(() {
        	HttpRequest req = new HttpRequest();
        	req.open("DELETE", SyagesInstance.shared.apiUrl("presidents/${user.id}"), async: false);
    		addHeadersToRequest(req, requestHeaders(null));
    		req.send();
    		if (req.response == null) throw new RequestFailureException();

    		switch(req.status)
   			{
   				case 204:
					return new Empty();
            	case 400:
            		throw new BadRequestException("Erreur ${req.status}:\n\t${req.response}");
   				case 404:
   					throw new NotFoundException("Erreur ${req.status}:\n\t${req.response}");
   				default:
   					throw new UnknownException("Erreur ${req.status}:\n\t${req.response}");
   			}
   		});
   	}

	Future<President> save()
	{
		if (id == null) return _save();
		else return _update();
	}

	Future<President> _save()
	{
		return new Future<President>.microtask(()
		{
			HttpRequest req = new HttpRequest();
			req.open("POST", SyagesInstance.shared.apiUrl("presidents"), async: false);
			addHeadersToRequest(req, requestHeaders());
			req.send(toJsonString(this.toMapForCreation()));
			if (req.response == null) throw new RequestFailureException();

			switch(req.status)
			{
				case 200:
					return new President.fromMap(JSON.decode(req.response));
            	case 400:
            		throw new BadRequestException("Erreur ${req.status}:\n\t${req.response}");
				case 401:
					throw new UnauthorizedException("Erreur ${req.status}:\n\t${req.response}");
				default:
					throw new UnknownException("Erreur ${req.status}:\n\t${req.response}");
			}
		});
	}

	Future<President> _update()
	{
		return new Future<President>.microtask(()
		{
        	HttpRequest req = new HttpRequest();
        	req.open("PUT", SyagesInstance.shared.apiUrl("presidents/${user.id}"), async: false);
        	addHeadersToRequest(req, requestHeaders());
        	req.send(toJsonString(this.toMapForUpdate()));
        	if (req.response == null) throw new RequestFailureException();

        	switch(req.status)
        	{
        		case 200:
        			return new President.fromMap(JSON.decode(req.response));
            	case 400:
            		throw new BadRequestException("Erreur ${req.status}:\n\t${req.response}");
        		case 401:
        			throw new UnauthorizedException("Erreur ${req.status}:\n\t${req.response}");
        		default:
        			throw new UnknownException("Erreur ${req.status}:\n\t${req.response}");
        	}
        });
	}
}
