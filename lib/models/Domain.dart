library syages.models.Domain;

import 'dart:async';
import 'dart:convert';
import 'dart:html';
import 'package:polymer/polymer.dart';
import '../Exceptions.dart';
import '../Helpers.dart';
import '../Instance.dart';
import 'Empty.dart';

class Domain extends Observable {
	@observable String id;
	@observable String name;
	@observable int version;

	Domain(String name) :
		this.id = null,
        this.version = 0,
        this.name = name;

	Domain.empty() : super(){}

	Domain.fromMap(Map<String, dynamic> json)
	{
		this.id = json["id"];
        this.version = json["version"];
        this.name = json["name"];
	}

	static Domain createFromJson(Map<String, dynamic> json){
		if(json==null){
			return new Domain.empty();
		}
		return new Domain.fromMap(json);
	}

	Map<String, dynamic> toMapForCreation()
	{
    	return {
    		"name": this.name
        } as Map<String,dynamic>;
	}

	Map<String, dynamic> toMapForUpdate()
    {
    	return {
    		"id": this.id,
			"name": this.name,
			"version": this.version
    	} as Map<String,dynamic>;
    }

	String toString() => "Domain #$id: $name";

	/* Requests */
	static Future<Domain> domain(String id)
	{
		return new Future<Domain>.microtask(()
		{
			HttpRequest req = new HttpRequest();
			req.open("GET", SyagesInstance.shared.apiUrl("domains/$id"), async: false);
			addHeadersToRequest(req, requestHeaders());
			req.send();
			if (req.response == null) throw new RequestFailureException();

			switch(req.status)
			{
				case 200:
					return new Domain.fromMap(JSON.decode(req.response));
            	case 400:
            		throw new BadRequestException("Erreur ${req.status}:\n\t${req.response}");
				case 401:
					throw new UnauthorizedException("Erreur ${req.status}:\n\t${req.response}");
				default:
					throw new UnknownException("Erreur ${req.status}:\n\t${req.response}");
			}
		});
	}

	static Future<List<Domain>> domainList([int limit = 25, int offset = 0])
	{
		return new Future<List<Domain>>.microtask(()
		{
			HttpRequest req = new HttpRequest();
    		req.open("GET", SyagesInstance.shared.apiUrl("domains?limit=$limit&offset=$offset"), async: false);
			addHeadersToRequest(req, requestHeaders());
			req.send();
			if (req.response == null) throw new RequestFailureException();

			switch(req.status)
			{
				case 200:
					List<Map<String,dynamic>> res = JSON.decode(req.response);
					List<Domain> list = new List<Domain>();
					res.forEach((Map<String, dynamic> _) {
						list.add(new Domain.fromMap(_));
					});
					return list;
            	case 400:
            		throw new BadRequestException("Erreur ${req.status}:\n\t${req.response}");
				case 401:
					throw new UnauthorizedException("Erreur ${req.status}:\n\t${req.response}");
				case 404:
					throw new NotFoundException("Erreur ${req.status}:\n\t${req.response}");
				default:
					throw new UnknownException("Erreur ${req.status}:\n\t${req.response}");
			}
		});
	}

	Future<Empty> delete() {
    	return new Future<Empty>.microtask(() {
        	HttpRequest req = new HttpRequest();
        	req.open("DELETE", SyagesInstance.shared.apiUrl("domains/$id"), async: false);
    		addHeadersToRequest(req, requestHeaders(null));
    		req.send();
    		if (req.response == null) throw new RequestFailureException();

    		switch(req.status)
   			{
   				case 204:
					return new Empty();
            	case 400:
            		throw new BadRequestException("Erreur ${req.status}:\n\t${req.response}");
   				case 404:
   					throw new NotFoundException("Erreur ${req.status}:\n\t${req.response}");
   				default:
   					throw new UnknownException("Erreur ${req.status}:\n\t${req.response}");
   			}
   		});
   	}

	Future<Domain> save()
	{
		if (id == null) return _save();
		else return _update();
	}

	Future<Domain> _save()
	{
		return new Future<Domain>.microtask(()
		{
			HttpRequest req = new HttpRequest();
			req.open("POST", SyagesInstance.shared.apiUrl("domains"), async: false);
			addHeadersToRequest(req, requestHeaders());
			req.send(toJsonString(this.toMapForCreation()));
			if (req.response == null) throw new RequestFailureException();

			switch(req.status)
			{
				case 201:
					return new Domain.fromMap(JSON.decode(req.response));
            	case 400:
            		throw new BadRequestException("Erreur ${req.status}:\n\t${req.response}");
				case 401:
					throw new UnauthorizedException("Erreur ${req.status}:\n\t${req.response}");
				default:
					throw new UnknownException("Erreur ${req.status}:\n\t${req.response}");
			}
		});
	}

	Future<Domain> _update()
	{
		return new Future<Domain>.microtask(()
		{
			HttpRequest req = new HttpRequest();
			req.open("PUT", SyagesInstance.shared.apiUrl("domains/$id"), async: false);
			addHeadersToRequest(req, requestHeaders());
			req.send(toJsonString(this.toMapForUpdate()));
			if (req.response == null) throw new RequestFailureException();

			switch(req.status)
			{
				case 200:
					return new Domain.fromMap(JSON.decode(req.response));
            	case 400:
            		throw new BadRequestException("Erreur ${req.status}:\n\t${req.response}");
				case 401:
					throw new UnauthorizedException("Erreur ${req.status}:\n\t${req.response}");
				default:
					throw new UnknownException("Erreur ${req.status}:\n\t${req.response}");
			}
		});
	}
}
