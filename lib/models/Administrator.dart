library syages.models.Administrator;

import 'dart:async';
import 'dart:convert';
import 'dart:html';
import 'package:polymer/polymer.dart';
import '../Exceptions.dart';
import '../Helpers.dart';
import '../Instance.dart';
import 'Empty.dart';
import 'User.dart';

class Administrator extends Observable
{
	@observable String id;
	@observable String number;
	@observable String office;
	@observable User user;
	@observable int version;

	Administrator(User user, [String office=null, String number=null]) :
		this.id = null,
		this.number = number,
		this.office = office,
		this.user = user,
		this.version = 0;

	Administrator.empty() : super()
	{
		this.user = new User.empty();
		this.user.kind = User.KINDS[User.KIND_ADMINISTRATOR];
	}

	Administrator.fromMap(Map<String, dynamic> json)
	{
		this.id = json["id"];
		this.number = json["number"];
		this.office = json["office"];
		this.user = new User.fromMap(json["user"]);
		this.version = json["version"];
	}

	Map<String, dynamic> toMapForCreation()
	{
		Map<String, dynamic> m = {
    		"user": this.user.toMapForCreation()
        } as Map<String,dynamic>;

        if (this.number != null)	m.putIfAbsent("number", () => this.number);
        if (this.office != null)	m.putIfAbsent("office", () => this.office);

        return m;
	}

	Map<String, dynamic> toMapForUpdate()
    {
    	return {
    		"id": this.id,
			"number": this.number,
			"office": this.office,
			"user": this.user.toMapForUpdate(),
			"version": this.version
    	} as Map<String,dynamic>;
    }

	String toString() => "Administrator #${user.id}: ${user.name}, office[$office], number[$number]";

	/* Requests */
	static Future<Administrator> administrator(String id) {
    	return new Future<Administrator>.microtask(() {
        	HttpRequest req = new HttpRequest();
        	req.open("GET", SyagesInstance.shared.apiUrl("administrators/$id"), async: false);
    		addHeadersToRequest(req, requestHeaders());
    		req.send();
    		if (req.response == null) throw new RequestFailureException();

    		switch(req.status)
   			{
   				case 200:
					return new Administrator.fromMap(JSON.decode(req.response));
            	case 400:
            		throw new BadRequestException("Erreur ${req.status}:\n\t${req.response}");
   				case 401:
   					throw new UnauthorizedException("Erreur ${req.status}:\n\t${req.response}");
   				default:
   					throw new UnknownException("Erreur ${req.status}:\n\t${req.response}");
   			}
   		});
   	}

	static Future<List<Administrator>> administratorList([int limit = 25, int offset = 0])
	{
		return new Future<List<Administrator>>.microtask(() {
    		HttpRequest req = new HttpRequest();
    		req.open("GET", SyagesInstance.shared.apiUrl("administrators?limit=$limit&offset=$offset"), async: false);
			addHeadersToRequest(req, requestHeaders());
			req.send();
			if (req.response == null) throw new RequestFailureException();

			switch(req.status)
			{
				case 200:
					List<Map<String,dynamic>> res = JSON.decode(req.response);
					List<Administrator> list = new List<Administrator>();
					res.forEach((Map<String, dynamic> _) {
						list.add(new Administrator.fromMap(_));
					});
					return list;
            	case 400:
            		throw new BadRequestException("Erreur ${req.status}:\n\t${req.response}");
				case 401:
					throw new UnauthorizedException("Erreur ${req.status}:\n\t${req.response}");
				case 404:
					throw new NotFoundException("Erreur ${req.status}:\n\t${req.response}");
				default:
					throw new UnknownException("Erreur ${req.status}:\n\t${req.response}");
			}
		});
	}



	Future<Empty> delete() {
    	return new Future<Empty>.microtask(() {
        	HttpRequest req = new HttpRequest();
        	req.open("DELETE", SyagesInstance.shared.apiUrl("administrators/${user.id}"), async: false);
    		addHeadersToRequest(req, requestHeaders(null));
    		req.send();
    		if (req.response == null) throw new RequestFailureException();

    		switch(req.status)
   			{
   				case 204:
					return new Empty();
            	case 400:
            		throw new BadRequestException("Erreur ${req.status}:\n\t${req.response}");
   				case 404:
   					throw new NotFoundException("Erreur ${req.status}:\n\t${req.response}");
   				default:
   					throw new UnknownException("Erreur ${req.status}:\n\t${req.response}");
   			}
   		});
   	}

	Future<Administrator> save()
	{
		if (id == null) return _save();
		else return _update();
	}

	Future<Administrator> _save()
	{
		return new Future<Administrator>.microtask(()
		{
			HttpRequest req = new HttpRequest();
			req.open("POST", SyagesInstance.shared.apiUrl("administrators"), async: false);
			addHeadersToRequest(req, requestHeaders());
			req.send(toJsonString(this.toMapForCreation()));
			if (req.response == null) throw new RequestFailureException();

			switch(req.status)
			{
				case 201:
					return new Administrator.fromMap(JSON.decode(req.response));
            	case 400:
            		throw new BadRequestException("Erreur ${req.status}:\n\t${req.response}");
				case 401:
					throw new UnauthorizedException("Erreur ${req.status}:\n\t${req.response}");
				default:
					throw new UnknownException("Erreur ${req.status}:\n\t${req.response}");
			}
		});
	}

	Future<Administrator> _update()
	{
		return new Future<Administrator>.microtask(()
		{
        	HttpRequest req = new HttpRequest();
        	req.open("PUT", SyagesInstance.shared.apiUrl("administrators/${user.id}"), async: false);
        	addHeadersToRequest(req, requestHeaders());
        	req.send(toJsonString(this.toMapForUpdate()));
        	if (req.response == null) throw new RequestFailureException();

        	switch(req.status)
        	{
        		case 200:
        			return new Administrator.fromMap(JSON.decode(req.response));
            	case 400:
            		throw new BadRequestException("Erreur ${req.status}:\n\t${req.response}");
        		case 401:
        			throw new UnauthorizedException("Erreur ${req.status}:\n\t${req.response}");
        		default:
        			throw new UnknownException("Erreur ${req.status}:\n\t${req.response}");
        	}
        });
	}

}
