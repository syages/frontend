library syages.models.Intern;

import 'dart:async';
import 'dart:convert';
import 'dart:html';
import 'package:polymer/polymer.dart';
import '../Exceptions.dart';
import '../Helpers.dart';
import '../Instance.dart';
import 'Diploma.dart';
import 'Empty.dart';
import 'User.dart';
import 'FollowedDiploma.dart';

class Intern extends Observable {
	@observable DateTime dateOfBirth;
	@observable List<FollowedDiploma> followedDiplomas = new List();
	@observable String id;
	@observable User user;
	@observable int version;
	
	// infos
	@observable String idInfos;
	@observable String job;
	@observable String lastSchool;
	@observable DateTime lastSchoolYear;
	@observable String professionalProject;
	@observable bool worked;
	@observable int versionInfos;

	Intern(String email, String firstName, String lastName, DateTime dateOfBirth) :
		this.dateOfBirth = dateOfBirth,
		this.followedDiplomas = new List<FollowedDiploma>(),
		this.id = null,
		this.user = new User(email, firstName, lastName),
	    this.version = 0
	{
		this.user.kind = User.KINDS[User.KIND_INTERN];
	}

	Intern.empty() : super()
	{
		this.followedDiplomas = new List<FollowedDiploma>();
		this.user = new User.empty();
		this.user.kind = User.KINDS[User.KIND_INTERN];
	}

	Intern.fromMap(Map<String, dynamic> json)
	{
  		this.dateOfBirth = new DateTime.fromMillisecondsSinceEpoch(json["dateOfBirth"]);
        (json['followedDiplomas'] as List).forEach((Map<String, dynamic> elem) => this.followedDiplomas.add(new FollowedDiploma.fromMap(elem)));
		this.id = json["id"];
		this.user = new User.fromMap(json["user"] as Map);
		this.user.kind = User.KINDS[User.KIND_INTERN];
        this.version = json["version"];
        
        if(json.containsKey("infos") && json["infos"]!=null)
        {
            Map<String, dynamic> infos = json["infos"];
            idInfos = infos["id"];
            job = infos["job"];
        	lastSchool = infos["lastSchool"];
        	lastSchoolYear = new DateTime.fromMillisecondsSinceEpoch(infos["lastSchoolYear"]);
        	professionalProject = infos["professionalProject"];
        	worked = infos["worked"];
        	versionInfos = infos["version"];	
        }
	}
	
	static Intern createFromJson(Map<String, dynamic> json){
		if(json == null){
			return new Intern.empty();
		}
		return new Intern.fromMap(json);
	}

	Map<String, dynamic> toMapForCreation()
	{
		Map<String, dynamic> m = {
    		"dateOfBirth" : this.dateOfBirth.millisecondsSinceEpoch,
        	"user" : this.user.toMapForCreation()
        } as Map<String,dynamic>;

        List<dynamic> listfDiplomas = new List<dynamic>();
        followedDiplomas.forEach((f){
        	listfDiplomas.add(f.toMapForCreation());
        });
		if (!listfDiplomas.isEmpty) m.putIfAbsent("followedDiplomas", () => listfDiplomas);

        return m;
	}

	Map<String, dynamic> toMapForUpdate()
    {
    	return {
    		"dateOfBirth" : this.dateOfBirth.millisecondsSinceEpoch,
        	"followedDiplomas" : this.followedDiplomas,
        	"id" : this.id,
        	"user" : this.user.toMapForUpdate(),
        	"version" : this.version
    	} as Map<String,dynamic>;
    }

	String toString() => "Intern #$id: ${user.name}";

	static Future<Intern> intern(String id) {
    	return new Future<Intern>.microtask(() {
        	HttpRequest req = new HttpRequest();
        	req.open("GET", SyagesInstance.shared.apiUrl("interns/$id"), async: false);
    		addHeadersToRequest(req, requestHeaders());
    		req.send();
    		if (req.response == null) throw new RequestFailureException();

    		switch(req.status)
   			{
   				case 200:
					return new Intern.fromMap(JSON.decode(req.response));
            	case 400:
            		throw new BadRequestException("Erreur ${req.status}:\n\t${req.response}");
   				case 401:
   					throw new UnauthorizedException("Erreur ${req.status}:\n\t${req.response}");
   				default:
   					throw new UnknownException("Erreur ${req.status}:\n\t${req.response}");
   			}
   		});
   	}

	static Future<List<Intern>> internList([int limit = 25, int offset = 0])
	{
		return new Future<List<Intern>>.microtask(() {
    		HttpRequest req = new HttpRequest();
    		req.open("GET", SyagesInstance.shared.apiUrl("interns?limit=$limit&offset=$offset"), async: false);
			addHeadersToRequest(req, requestHeaders());
			req.send();
			if (req.response == null) throw new RequestFailureException();

			switch(req.status)
			{
				case 200:
					List<Map<String,dynamic>> res = JSON.decode(req.response);
					List<Intern> list = new List<Intern>();
					res.forEach((Map<String, dynamic> _) {
						list.add(new Intern.fromMap(_));
					});
					return list;
            	case 400:
            		throw new BadRequestException("Erreur ${req.status}:\n\t${req.response}");
				case 401:
					throw new UnauthorizedException("Erreur ${req.status}:\n\t${req.response}");
				case 404:
					throw new NotFoundException("Erreur ${req.status}:\n\t${req.response}");
				default:
					throw new UnknownException("Erreur ${req.status}:\n\t${req.response}");
			}
		});
	}

	static Future userForContactDirectory() 
	 {
	  return new Future.microtask(() {
	    var ifuture = _internsForContactDirectory();
	    var tfuture = _teachersForContactDirectory();
	
	    ifuture.then((ilist)
	    { return ilist; });
	    tfuture.then((tlist)
	    { return tlist; });
	
	    return Future.wait([ifuture, tfuture]);
	  });
	}

	Future<Empty> delete() {
    	return new Future<Empty>.microtask(() {
        	HttpRequest req = new HttpRequest();
        	req.open("DELETE", SyagesInstance.shared.apiUrl("interns/${user.id}"), async: false);
    		addHeadersToRequest(req, requestHeaders(null));
    		req.send();
    		if (req.response == null) throw new RequestFailureException();

    		switch(req.status)
   			{
   				case 204:
					return new Empty();
            	case 400:
            		throw new BadRequestException("Erreur ${req.status}:\n\t${req.response}");
   				case 404:
   					throw new NotFoundException("Erreur ${req.status}:\n\t${req.response}");
   				default:
   					throw new UnknownException("Erreur ${req.status}:\n\t${req.response}");
   			}
   		});
   	}

	static Future<bool> isFirst(String id)
	{
		return new Future<bool>.microtask(() {
			HttpRequest req = new HttpRequest();
			req.open("GET", SyagesInstance.shared.apiUrl("interns/$id/isFirst"), async: false);
			addHeadersToRequest(req, requestHeaders());
            req.send();
            if (req.response == null) throw new RequestFailureException();

            switch(req.status)
            {
            	case 200:
            		Map<String, bool> res = JSON.decode(req.response);
            		return res["first"];
            	case 400:
                	throw new BadRequestException("Erreur ${req.status}:\n\t${req.response}");
                case 404:
                	throw new NotFoundException("Erreur ${req.status}:\n\t${req.response}");
                default:
                    throw new UnknownException("Erreur ${req.status}:\n\t${req.response}");
            }
		});
	}

	Future<Intern> save()
	{
		if (id == null) return _save();
		else return _update();
	}

	static Future<Map<String, dynamic>> getInfos(String id)
	{
		return new Future<Map<String, dynamic>>.microtask(()
        {
    		HttpRequest req = new HttpRequest();
       		req.open("GET", SyagesInstance.shared.apiUrl("interns/$id/infos"), async: false);
       		addHeadersToRequest(req, requestHeaders());

        	req.send();
        	if (req.response == null) throw new RequestFailureException();

			switch(req.status)
        	{
				case 200:
        		case 204:
        			Map<String, dynamic> json = JSON.decode(req.responseText);
        			return json;
                case 400:
                    throw new BadRequestException("Erreur ${req.status}:\n\t${req.response}");
        		case 401:
        			throw new UnauthorizedException("Erreur ${req.status}:\n\t${req.response}");
        		default:
        			throw new UnknownException("Erreur ${req.status}:\n\t${req.response}");
        		}
        	});
	}
	
	static Future<bool> saveInfos(String id, String lastSchool, String professionalProjet, DateTime lastSchoolYear, bool worked, String job)
	{
		return new Future<bool>.microtask(()
        {
    		HttpRequest req = new HttpRequest();
       		req.open("POST", SyagesInstance.shared.apiUrl("interns/$id/infos"), async: false);
       		addHeadersToRequest(req, requestHeaders());

       		Map<String, dynamic> m = {
				"job": job,
  				"lastSchool": lastSchool,
  				"lastSchoolYear":lastSchoolYear.millisecondsSinceEpoch,
  				"professionalProject": professionalProjet,
  				"worked": worked
            } as Map<String,dynamic>;

        	req.send(JSON.encode(m));
        	if (req.response == null) throw new RequestFailureException();

			switch(req.status)
        	{
        		case 204:
        			return true;
                case 400:
                    throw new BadRequestException("Erreur ${req.status}:\n\t${req.response}");
        		case 401:
        			throw new UnauthorizedException("Erreur ${req.status}:\n\t${req.response}");
        		default:
        			throw new UnknownException("Erreur ${req.status}:\n\t${req.response}");
        		}
        	});
	}

	Future<Intern> _save()
	{
		return new Future<Intern>.microtask(()
		{
			HttpRequest req = new HttpRequest();
			req.open("POST", SyagesInstance.shared.apiUrl("interns"), async: false);
			addHeadersToRequest(req, requestHeaders());
			req.send(toJsonString(this.toMapForCreation()));
			if (req.response == null) throw new RequestFailureException();

			switch(req.status)
			{
				case 201:
					return new Intern.fromMap(JSON.decode(req.response));
            	case 400:
            		throw new BadRequestException("Erreur ${req.status}:\n\t${req.response}");
				case 401:
					throw new UnauthorizedException("Erreur ${req.status}:\n\t${req.response}");
				default:
					throw new UnknownException("Erreur ${req.status}:\n\t${req.response}");
			}
		});
	}

	Future<Intern> _update()
	{
		return new Future<Intern>.microtask(()
		{
        	HttpRequest req = new HttpRequest();
        	req.open("PUT", SyagesInstance.shared.apiUrl("interns/${user.id}"), async: false);
        	addHeadersToRequest(req, requestHeaders());
        	req.send(toJsonString(this.toMapForUpdate()));
        	if (req.response == null) throw new RequestFailureException();

        	switch(req.status)
        	{
        		case 201:
        			return new Intern.fromMap(JSON.decode(req.response));
            	case 400:
            		throw new BadRequestException("Erreur ${req.status}:\n\t${req.response}");
        		case 401:
        			throw new UnauthorizedException("Erreur ${req.status}:\n\t${req.response}");
        		default:
        			throw new UnknownException("Erreur ${req.status}:\n\t${req.response}");
        	}
        });
	}
	
	static Future<List<User>> _internsForContactDirectory() 
	{
		return new Future<List<User>>.microtask(() {
    		HttpRequest req = new HttpRequest();
    		req.open("GET", SyagesInstance.shared.apiUrl("users/interns"), async: false);
			addHeadersToRequest(req, requestHeaders());
			req.send();
			if (req.response == null) throw new RequestFailureException();

			switch(req.status)
			{
				case 200:
					List<Map<String,dynamic>> res = JSON.decode(req.response);
					List<User> list = new List<User>();
					res.forEach((Map<String, dynamic> intern) {
						list.add(new User.fromMap(intern["user"]));
					});
					return list;
            	case 400:
            		throw new BadRequestException("Erreur ${req.status}:\n\t${req.response}");
				case 401:
					throw new UnauthorizedException("Erreur ${req.status}:\n\t${req.response}");
				case 404:
					throw new NotFoundException("Erreur ${req.status}:\n\t${req.response}");
				default:
					throw new UnknownException("Erreur ${req.status}:\n\t${req.response}");
			}
		});
   	}
	
	static Future<List<User>> _teachersForContactDirectory() 
	{
		return new Future<List<User>>.microtask(() {
    		HttpRequest req = new HttpRequest();
    		req.open("GET", SyagesInstance.shared.apiUrl("users/teachers"), async: false);
			addHeadersToRequest(req, requestHeaders());
			req.send();
			if (req.response == null) throw new RequestFailureException();

			switch(req.status)
			{
				case 200:
					List<Map<String,dynamic>> res = JSON.decode(req.response);
					List<User> list = new List<User>();
					res.forEach((Map<String, dynamic> teacher) {
						list.add(new User.fromMap(teacher["user"]));
					});
					return list;
            	case 400:
            		throw new BadRequestException("Erreur ${req.status}:\n\t${req.response}");
				case 401:
					throw new UnauthorizedException("Erreur ${req.status}:\n\t${req.response}");
				case 404:
					throw new NotFoundException("Erreur ${req.status}:\n\t${req.response}");
				default:
					throw new UnknownException("Erreur ${req.status}:\n\t${req.response}");
			}
		});
   	}
}
