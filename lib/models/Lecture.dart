library syages.models.Lecture;

import 'dart:async';
import 'dart:convert';
import 'dart:html';
import 'package:polymer/polymer.dart';
import '../Exceptions.dart';
import '../Helpers.dart';
import '../Instance.dart';
import 'Empty.dart';
import 'Lesson.dart';
import 'NotedAbsence.dart';

class Lecture extends Observable {
	@observable String classroom;
	@observable DateTime date = new DateTime.now();
	@observable String id;
	@observable String lessonId;
	@observable Lesson lesson;
	@observable int duration;
	@observable int version;
	@observable List<NotedAbsence> notedAbsences = new List<NotedAbsence>();
	@observable List<String> newNotedAbsences = new List<String>();

	Lecture(String id, Lesson lesson, DateTime date, [String classroom=null]) :
		this.classroom = classroom,
		this.date = date,
		this.id = id,
		this.lesson = lesson,
		this.version = 0;

	Lecture.empty() : super()
	{
		this.lesson = new Lesson.empty();
		this.date = new DateTime.now();
	}

	Lecture.fromMap(Map<String, dynamic> json) :
		this.classroom = json["classroom"],
		this.duration = json['duration'],
        this.date = new DateTime.fromMillisecondsSinceEpoch(json["date"]),
        this.id = json["id"],
        this.lesson = new Lesson.fromMap(json["lesson"] as Map),
        this.version = json["version"];

	Map<String, dynamic> toMapForCreation()
	{
		Map<String, dynamic> m = {
        	"date" : this.date.millisecondsSinceEpoch,
        	"lesson" : this.lessonId,
        	"duration" : this.duration,
        } as Map<String,dynamic>;

        if (this.classroom != null)	m.putIfAbsent("classroom", () => this.classroom);

        return m;
	}

	Map<String, dynamic> toMapForUpdate()
    {
    	return {
    		"classroom" : this.classroom,
    		"duration" : this.duration,
        	"date" : this.date.millisecondsSinceEpoch,
        	"id" : this.id,
        	"lesson" : this.lessonId,
        	"version" : this.version
    	} as Map<String,dynamic>;
    }

	String toString() => "Lecture #$id: ${lesson.course.name} en $classroom à $date";

	/* Requests */
	static Future<Lecture> lecture(String id) {
    	return new Future<Lecture>.microtask(() {
        	HttpRequest req = new HttpRequest();
        	req.open("GET", SyagesInstance.shared.apiUrl("lectures/$id"), async: false);
    		addHeadersToRequest(req, requestHeaders());
    		req.send();
    		if (req.response == null) throw new RequestFailureException();

    		switch(req.status)
   			{
   				case 200:
					return new Lecture.fromMap(JSON.decode(req.response));
            	case 400:
            		throw new BadRequestException("Erreur ${req.status}:\n\t${req.response}");
   				case 401:
   					throw new UnauthorizedException("Erreur ${req.status}:\n\t${req.response}");
   				default:
   					throw new UnknownException("Erreur ${req.status}:\n\t${req.response}");
   			}
   		});
   	}

	static Future<List<Lecture>> lectureList([int limit = 25, int offset = 0])
	{
		return new Future<List<Lecture>>.microtask(() {
    		HttpRequest req = new HttpRequest();
    		req.open("GET", SyagesInstance.shared.apiUrl("lectures?limit=$limit&offset=$offset"), async: false);
			addHeadersToRequest(req, requestHeaders());
			req.send();
			if (req.response == null) throw new RequestFailureException();

			switch(req.status)
			{
				case 200:
					List<Map<String,dynamic>> res = JSON.decode(req.response);
					List<Lecture> list = new List<Lecture>();
					res.forEach((Map<String, dynamic> _) {
						list.add(new Lecture.fromMap(_));
					});
					return list;
            	case 400:
            		throw new BadRequestException("Erreur ${req.status}:\n\t${req.response}");
				case 401:
					throw new UnauthorizedException("Erreur ${req.status}:\n\t${req.response}");
				case 404:
					throw new NotFoundException("Erreur ${req.status}:\n\t${req.response}");
				default:
					throw new UnknownException("Erreur ${req.status}:\n\t${req.response}");
			}
		});
	}

	Future<List<NotedAbsence>> addAbsences()
	{
		List<dynamic> absences = new List();
		absences.addAll(this.notedAbsences);
		absences.addAll(this.newNotedAbsences);
    	return new Future<List<NotedAbsence>>.microtask(() {
        	HttpRequest req = new HttpRequest();
        	req.open("POST", SyagesInstance.shared.apiUrl("lectures/$id/absences"), async: false);
    		addHeadersToRequest(req, requestHeaders());
        	req.setRequestHeader('Content-type', 'application/json');
    		req.send(JSON.encode(absences));
    		if (req.response == null) throw new RequestFailureException();

    		switch(req.status)
   			{
   				case 201:
   					List<Map<String,dynamic>> res = JSON.decode(req.response);
                    List<NotedAbsence> list = new List<NotedAbsence>();
                    res.forEach((Map<String, dynamic> _) {
                    	list.add(new NotedAbsence.fromMap(_));
                    });
                    return list;
            	case 400:
            		throw new BadRequestException("Erreur ${req.status}:\n\t${req.response}");
   				case 401:
   					throw new UnauthorizedException("Erreur ${req.status}:\n\t${req.response}");
   				default:
   					throw new UnknownException("Erreur ${req.status}:\n\t${req.response}");
   			}
   		});
   	}

	Future<List<NotedAbsence>> absencesList([int limit = 25, int offset = 0])
    {
    	return new Future<List<NotedAbsence>>.microtask(() {
       		HttpRequest req = new HttpRequest();
       		req.open("GET", SyagesInstance.shared.apiUrl("lectures/$id/absences?limit=$limit&offset=$offset"), async: false);
    		addHeadersToRequest(req, requestHeaders());
    		req.send();
   			if (req.response == null) throw new RequestFailureException();

   			switch(req.status)
    		{
    			case 200:
    				List<Map<String,dynamic>> res = JSON.decode(req.response);
    				List<NotedAbsence> list = new List<NotedAbsence>();
    				res.forEach((Map<String, dynamic> _) {
    					list.add(new NotedAbsence.fromMap(_));
    				});
    				return list;
            	case 400:
            		throw new BadRequestException("Erreur ${req.status}:\n\t${req.response}");
    			case 401:
    				throw new UnauthorizedException("Erreur ${req.status}:\n\t${req.response}");
    			case 404:
    				throw new NotFoundException("Erreur ${req.status}:\n\t${req.response}");
    			default:
    				throw new UnknownException("Erreur ${req.status}:\n\t${req.response}");
    		}
    	});
    }

	Future<Empty> delete() {
    	return new Future<Empty>.microtask(() {
        	HttpRequest req = new HttpRequest();
        	req.open("DELETE", SyagesInstance.shared.apiUrl("lectures/$id"), async: false);
    		addHeadersToRequest(req, requestHeaders(null));
    		req.send();
    		if (req.response == null) throw new RequestFailureException();

    		switch(req.status)
   			{
   				case 204:
					return new Empty();
            	case 400:
            		throw new BadRequestException("Erreur ${req.status}:\n\t${req.response}");
   				case 404:
   					throw new NotFoundException("Erreur ${req.status}:\n\t${req.response}");
   				default:
   					throw new UnknownException("Erreur ${req.status}:\n\t${req.response}");
   			}
   		});
   	}

	Future<Lecture> save()
	{
		if (id == null) return _save();
		else return _update();
	}

	Future<Lecture> _save()
	{
		return new Future<Lecture>.microtask(()
		{
			HttpRequest req = new HttpRequest();
			req.open("POST", SyagesInstance.shared.apiUrl("lectures"), async: false);
			addHeadersToRequest(req, requestHeaders());
			req.send(toJsonString(this.toMapForCreation()));
			if (req.response == null) throw new RequestFailureException();

			switch(req.status)
			{
				case 201:
					return new Lecture.fromMap(JSON.decode(req.response));
            	case 400:
            		throw new BadRequestException("Erreur ${req.status}:\n\t${req.response}");
				case 401:
					throw new UnauthorizedException("Erreur ${req.status}:\n\t${req.response}");
				default:
					throw new UnknownException("Erreur ${req.status}:\n\t${req.response}");
			}
		});
	}

	Future<Lecture> _update()
	{
		return new Future<Lecture>.microtask(()
		{
        	HttpRequest req = new HttpRequest();
        	req.open("PUT", SyagesInstance.shared.apiUrl("lectures/$id"), async: false);
        	addHeadersToRequest(req, requestHeaders());
        	req.send(toJsonString(this.toMapForUpdate()));
        	if (req.response == null) throw new RequestFailureException();

        	switch(req.status)
        	{
        		case 201:
        			return new Lecture.fromMap(JSON.decode(req.response));
            	case 400:
            		throw new BadRequestException("Erreur ${req.status}:\n\t${req.response}");
        		case 401:
        			throw new UnauthorizedException("Erreur ${req.status}:\n\t${req.response}");
        		default:
        			throw new UnknownException("Erreur ${req.status}:\n\t${req.response}");
        	}
        });
	}
}
