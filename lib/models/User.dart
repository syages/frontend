library syages.models.User;

import 'dart:async';
import 'dart:convert';
import 'dart:html';
import 'package:polymer/polymer.dart';
import '../\$Cache.dart';
import '../Exceptions.dart';
import '../Helpers.dart';
import '../Instance.dart';
import "../Session.dart";
import 'Lesson.dart';
import 'NotedAbsence.dart';
import 'DeclaredAbsence.dart';

class User extends Observable {

	static final List<String> KINDS = ["Administrator", "Intern", "President", "Staff", "Teacher", "Unknown"];
	static final int KIND_ADMINISTRATOR = 0;
	static final int KIND_INTERN = 1;
	static final int KIND_PRESIDENT = 2;
	static final int KIND_STAFF = 3;
	static final int KIND_TEACHER = 4;
	static final int KIND_UNKNOWN = 5;

	@observable String email;
	@observable String firstName;
	@observable String id;
	@observable String kind;
	@observable String lastName;
	@observable String pictureUrl;
	@observable int version;

	User(String email, String firstName, String lastName) :
		this.email = email,
		this.firstName = firstName,
        this.id = null,
        this.version = 0,
        this.kind = "None",
        this.lastName = lastName,
        this.pictureUrl = "";

	User.empty();

	User.fromMap(Map<String, dynamic> json)
	{
        this.email = json["email"];
        this.firstName = json["firstName"];
        this.id = json["id"];
        this.kind = json["flagType"];
        this.lastName = json["lastName"];
        this.pictureUrl = SyagesInstance.shared.apiUrl("users/$id/picture");
        this.version = json["version"];
	}

	String get name => "$firstName $lastName";

	Map<String, dynamic> toMapForCreation()
	{
		return {
        	"email" : this.email,
        	"firstName" : this.firstName,
        	"lastName" : this.lastName,
		} as Map<String,dynamic>;
	}

	Map<String, dynamic> toMapForUpdate()
	{
		return {
        	"id" : this.id,
        	"email" : this.email,
        	"firstName" : this.firstName,
        	"flagType" : this.kind,
        	"lastName" : this.lastName,
        	"version" : this.version
		} as Map<String,dynamic>;
	}

	String toString() => "User #$id: $name ($kind)";

	/* Requests */
	static Future<User> me() {
		if ($Cache.sharedInstance.user != null) return new Future.value($Cache.sharedInstance.user);
		return new Future<User>.microtask(() {
			HttpRequest req = new HttpRequest();
			req.open("GET", SyagesInstance.shared.apiUrl("me"), async: false);
			addHeadersToRequest(req, requestHeaders());
			req.send();
            if (req.response == null) throw new RequestFailureException();

			switch(req.status)
			{
				case 200:
					$Cache.sharedInstance.user = new User.fromMap(JSON.decode(req.response));
					return me();
            	case 400:
            		throw new BadRequestException("Erreur ${req.status}:\n\t${req.response}");
				case 401:
					throw new UnauthorizedException("Erreur ${req.status}:\n\t${req.response}");
				default:
					throw new UnknownException("Erreur ${req.status}:\n\t${req.response}");
			}
		});
	}

	static Future<List<Lesson>> meLessons(){
    		return new Future<List<Lesson>>.microtask((){
    			HttpRequest req = new HttpRequest();
    			req.open("GET", SyagesInstance.shared.apiUrl("me/lessons"), async: false);
    			addHeadersToRequest(req, requestHeaders());
    			req.send();
    			if(req.response == null) throw new RequestFailureException();

    			switch(req.status)
    			{
    				case 200:
    					List<Map<String,dynamic>> res = JSON.decode(req.response);
    					List<Lesson> list = new List<Lesson>();
    					res.forEach((Map<String, dynamic> _) {
    						list.add(new Lesson.fromMap(_));
    					});
    					return list;
    				case 400:
                		throw new BadRequestException("Erreur ${req.status}:\n\t${req.response}");
    				case 401:
    					throw new UnauthorizedException("Erreur ${req.status}:\n\t${req.response}");
    				default:
    					throw new UnknownException("Erreur ${req.status}:\n\t${req.response}");
    			}
    		});
    	}
	
	static Future<List<NotedAbsence>> meNotedAbsences(){
        		return new Future<List<NotedAbsence>>.microtask((){
        			HttpRequest req = new HttpRequest();
        			req.open("GET", SyagesInstance.shared.apiUrl("me/notedAbsences"), async: false);
        			addHeadersToRequest(req, requestHeaders());
        			req.send();
        			if(req.response == null) throw new RequestFailureException();

        			switch(req.status)
        			{
        				case 200:
        					List<Map<String,dynamic>> res = JSON.decode(req.response);
        					List<NotedAbsence> list = new List<NotedAbsence>();
        					res.forEach((Map<String, dynamic> _) {
        						list.add(new NotedAbsence.fromMap(_));
        					});
        					return list;
        				case 400:
                    		throw new BadRequestException("Erreur ${req.status}:\n\t${req.response}");
        				case 401:
        					throw new UnauthorizedException("Erreur ${req.status}:\n\t${req.response}");
        				default:
        					throw new UnknownException("Erreur ${req.status}:\n\t${req.response}");
        			}
        		});
        	}
	
	static Future<List<DeclaredAbsence>> meDeclaredAbsences(){
            		return new Future<List<DeclaredAbsence>>.microtask((){
            			HttpRequest req = new HttpRequest();
            			req.open("GET", SyagesInstance.shared.apiUrl("me/declaredAbsences"), async: false);
            			addHeadersToRequest(req, requestHeaders());
            			req.send();
            			if(req.response == null) throw new RequestFailureException();

            			switch(req.status)
            			{
            				case 200:
            					List<Map<String,dynamic>> res = JSON.decode(req.response);
            					List<DeclaredAbsence> list = new List<DeclaredAbsence>();
            					res.forEach((Map<String, dynamic> _) {
            						list.add(new DeclaredAbsence.fromMap(_));
            					});
            					return list;
            				case 400:
                        		throw new BadRequestException("Erreur ${req.status}:\n\t${req.response}");
            				case 401:
            					throw new UnauthorizedException("Erreur ${req.status}:\n\t${req.response}");
            				default:
            					throw new UnknownException("Erreur ${req.status}:\n\t${req.response}");
            			}
            		});
            	}

	static Future<User> user(String id) {
    	return new Future<User>.microtask(() {
        	HttpRequest req = new HttpRequest();
        	req.open("GET", SyagesInstance.shared.apiUrl("users/$id"), async: false);
    		addHeadersToRequest(req, requestHeaders());
    		req.send();
    		if (req.response == null) throw new RequestFailureException();

    		switch(req.status)
   			{
   				case 200:
					return new User.fromMap(JSON.decode(req.response));
            	case 400:
            		throw new BadRequestException("Erreur ${req.status}:\n\t${req.response}");
   				case 401:
   					throw new UnauthorizedException("Erreur ${req.status}:\n\t${req.response}");
   				default:
   					throw new UnknownException("Erreur ${req.status}:\n\t${req.response}");
   			}
   		});
   	}

	static Future<List<User>> usersList([int limit = 25, int offset = 0])
	{
		return new Future<List<User>>.microtask(() {
    		HttpRequest req = new HttpRequest();
    		req.open("GET", SyagesInstance.shared.apiUrl("users?limit=$limit&offset=$offset"), async: false);
			addHeadersToRequest(req, requestHeaders());
			req.send();
			if (req.response == null) throw new RequestFailureException();

			switch(req.status)
			{
				case 200:
					List<Map<String,dynamic>> res = JSON.decode(req.response);
					List<User> list = new List<User>();
					res.forEach((Map<String, dynamic> _) {
						list.add(new User.fromMap(_));
					});
					return list;
            	case 400:
            		throw new BadRequestException("Erreur ${req.status}:\n\t${req.response}");
				case 401:
					throw new UnauthorizedException("Erreur ${req.status}:\n\t${req.response}");
				case 404:
					throw new NotFoundException("Erreur ${req.status}:\n\t${req.response}");
				default:
					throw new UnknownException("Erreur ${req.status}:\n\t${req.response}");
			}
		});
	}
	
	Future<bool> setPicture(var picture){
		return new Future<bool>.microtask((){
			HttpRequest req = new HttpRequest();
			FormData data = new FormData();
			data.appendBlob('picture', picture);
			req.open('POST', SyagesInstance.shared.apiUrl("users/$id/picture"), async: false);
			req.setRequestHeader('Authorization', SyagesSession.sharedInstance.token);
			req.send(data);
			if (req.response == null) throw new RequestFailureException();
			
			switch(req.status)
			{
				case 204:
					return true;
            	case 400:
            		throw new BadRequestException("Erreur ${req.status}:\n\t${req.response}");
				case 401:
					throw new UnauthorizedException("Erreur ${req.status}:\n\t${req.response}");
				case 404:
					throw new NotFoundException("Erreur ${req.status}:\n\t${req.response}");
				default:
					throw new UnknownException("Erreur ${req.status}:\n\t${req.response}");
			}
		});
	}



}