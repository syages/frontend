library syages.models.Teacher;

import 'dart:async';
import 'dart:convert';
import 'dart:html';
import 'package:polymer/polymer.dart';
import '../Exceptions.dart';
import '../Helpers.dart';
import '../Instance.dart';
import 'Domain.dart';
import 'Empty.dart';
import 'User.dart';

class Teacher extends Observable {
	@observable List<Domain> domains;
	@observable bool shareMail;
	@observable String id;
	@observable String number;
	@observable String office;
	@observable User user;
	@observable int version;


	Teacher(String email, String firstName, String lastName, List<Domain> domains,
			[String number=null, String office=null]) :
		this.user = new User(email, firstName, lastName),
		this.id = null,
        this.version = 0,
        this.number = number,
        this.office = office,
        this.domains = domains
	{
		this.user.kind = User.KINDS[User.KIND_TEACHER];
	}

	Teacher.empty():super()
	{
		this.domains = new List<Domain>();
		this.user = new User.empty();
		this.user.kind = User.KINDS[User.KIND_TEACHER];
	}

	Teacher.fromMap(Map<String, dynamic> json)
	{
		this.user = new User.fromMap(json["user"] as Map);
		this.id = json["id"];
        this.version = json["version"];
        this.number = json["number"];
        this.office = json["office"];
        this.shareMail = json['shareMail'];
        this.domains = new List<Domain>();
        if (json["domains"] != null){
		    (json["domains"] as List).forEach((d){
		    	this.domains.add(new Domain.fromMap(d));
		    });
        }
        this.user.kind = User.KINDS[User.KIND_TEACHER];
	}

	static Teacher createFromJson(Map<String, dynamic> json){
		if(json == null){
			return new Teacher.empty();
		}
		return new Teacher.fromMap(json);
	}

	Map<String, dynamic> toMapForCreation() {
		Map<String, dynamic> m = {
        	"user": this.user.toMapForCreation(),

        } as Map<String,String>;

        if (this.number != null)	m.putIfAbsent("number", () => this.number);
        if (this.office != null)	m.putIfAbsent("office", () => this.office);
        if (this.shareMail != null) m.putIfAbsent("shareMail", () => this.shareMail);
       	if (this.domains == null) return m;
        List<String> domainsId = new List<String>();
        for (int i = 0; i < this.domains.length; i++) {
        	domainsId.add(this.domains[i].id);
       	}
        m.putIfAbsent("domains", () => domainsId);
        return m;
    }

	Map<String, dynamic> toMapForUpdate() {
		Map<String, dynamic> m = {
        	"user": this.user.toMapForUpdate(),
        	"number": this.number,
        	"office": this.office,
       		"id" : this.id,
            "version" : this.version,
            "shareMail": this.shareMail
       } as Map<String,String>;
       if (domains.length == 0) return m;

       List<String> domainsId = new List<String>();
       for (int i = 0; i < domains.length; i++) {
           domainsId.add(domains[i].id);
       }
       m.putIfAbsent("domains", () => domainsId);
       return m;
	}

	String toString() => "Teacher #$id: ${user.name}";

	/* Requests */
	Future<Empty> delete() {
		return new Future<Empty>.microtask(() {
        	HttpRequest req = new HttpRequest();
            req.open("DELETE", SyagesInstance.shared.apiUrl("teachers/${user.id}"), async: false);
            addHeadersToRequest(req, requestHeaders());
            req.send();

            if (req.response == null) throw new RequestFailureException();

            switch(req.status)
            {
                case 204:
                	return new Empty();
            	case 400:
            		throw new BadRequestException("Erreur ${req.status}:\n\t${req.response}");
                case 404:
                	throw new NotFoundException("Erreur ${req.status}:\n\t${req.response}");
                default:
                	throw new UnknownException("Erreur ${req.status}:\n\t${req.response}");
        	}
    	});
    }

	static Future<List<Teacher>> teacherInDomain(String id) {
    		return new Future<List<Teacher>>.microtask(() {
            	HttpRequest req = new HttpRequest();
                req.open("GET", SyagesInstance.shared.apiUrl("teachers/domains/$id"), async: false);
                addHeadersToRequest(req, requestHeaders());
                req.send();

                if (req.response == null) throw new RequestFailureException();

                switch(req.status)
                {
                    case 200:
                    	List<Map<String,dynamic>> res = JSON.decode(req.response);
                        List<Teacher> list = new List<Teacher>();
                        res.forEach( (Map<String, dynamic> _)
                        {
                        	list.add(new Teacher.fromMap(_));
                        });
                      	return list;
                    case 404:
                    	throw new NotFoundException("Erreur ${req.status}:\n\t${req.response}");
                    default:
                    	throw new UnknownException("Erreur ${req.status}:\n\t${req.response}");
            	}
        	});
        }

	Future<Teacher> save() {
    	if (id == null) return _save();
    	else return _update();
    }

	Future<Teacher> _save() {
        return new Future<Teacher>.microtask(() {
       		HttpRequest req = new HttpRequest();
            req.open("POST", SyagesInstance.shared.apiUrl("teachers"), async: false);
            addHeadersToRequest(req, requestHeaders());
            Map<String, dynamic> m = this.toMapForCreation();
            String data = toJsonString(m);
            warn(data);
            req.send(data);
            if (req.response == null) throw new RequestFailureException();

            switch(req.status)
        	{
        		case 201:
        			return new Teacher.fromMap(JSON.decode(req.response) as Map<String, dynamic>);
        		case 401:
        			throw new UnauthorizedException("Erreur ${req.status}:\n\t${req.response}");
        		default:
        			throw new UnknownException("Erreur ${req.status}:\n\t${req.response}");
        	}
        });
	}

	static Future<Teacher> teacher(String id) {
        return new Future<Teacher>.microtask(() {
        	HttpRequest req = new HttpRequest();
            req.open("GET", SyagesInstance.shared.apiUrl("teachers/$id"), async: false);
            addHeadersToRequest(req, requestHeaders());
            req.send();
            if (req.response == null) throw new RequestFailureException();

            switch(req.status)
            {
            	case 200:
            		return new Teacher.fromMap(JSON.decode(req.response) as Map<String, dynamic>);
            	case 400:
            		throw new BadRequestException("Erreur ${req.status}:\n\t${req.response}");
            	case 401:
            		throw new UnauthorizedException("Erreur ${req.status}:\n\t${req.response}");
            	default:
            		throw new UnknownException("Erreur ${req.status}:\n\t${req.response}");
            }
        });
	}

	static Future<List<Teacher>>teachersList([int limit = 25, int offset = 0]) {
		return new Future<List<Teacher>>.microtask(() {
        	HttpRequest req = new HttpRequest();
         	req.open("GET", SyagesInstance.shared.apiUrl("teachers?limit=$limit&offset=$offset"), async: false);
            addHeadersToRequest(req, requestHeaders());
            req.send();
            if (req.response == null) throw new RequestFailureException();
           	switch(req.status) {
                case 200:
                	List<Map<String, dynamic>> res = JSON.decode(req.response);
                	List<Teacher> list = new List<Teacher>();
                	res.forEach((Map<String, dynamic> _) {
                		list.add(new Teacher.fromMap(_));
                	});
                	return list;
            	case 400:
            		throw new BadRequestException("Erreur ${req.status}:\n\t${req.response}");
               	case 401:
                	throw new UnauthorizedException("Erreur ${req.status}:\n\t${req.response}");
                default:
                	throw new UnknownException("Erreur ${req.status}:\n\t${req.response}");
               }
        });
	}

	Future<Teacher> _update() {
    	return new Future<Teacher>.microtask(() {
            HttpRequest req = new HttpRequest();
            req.open("PUT", SyagesInstance.shared.apiUrl("teachers/${this.user.id}"), async: false);
            addHeadersToRequest(req, requestHeaders());
            String data = toJsonString(this.toMapForUpdate());
            req.send(data);
            if (req.response == null) throw new RequestFailureException();

	     	switch(req.status)
            {
                case 200:
               		return new Teacher.fromMap(JSON.decode(req.response) as Map<String, dynamic>);
            	case 400:
            		throw new BadRequestException("Erreur ${req.status}:\n\t${req.response}");
               	case 401:
               		throw new UnauthorizedException("Erreur ${req.status}:\n\t${req.response}");
               	default:
               		throw new UnknownException("Erreur ${req.status}:\n\t${req.response}");
            }
        });
     }
}
