library syages.models.DeclaredAbsence;

import 'dart:async';
import 'dart:convert';
import 'dart:html';
import 'package:polymer/polymer.dart';
import '../Exceptions.dart';
import '../Helpers.dart';
import '../Instance.dart';
import 'Empty.dart';
import 'Intern.dart';
import 'NotedAbsence.dart';

class DeclaredAbsence extends Observable {

	static final List<String> REASONS = ["Death", "DrivingTestNotification", "MedicalCertificate",
	                                        "Other", "PublicTransportProblem", "ReligiousHoliday",
	                                        "Summons ", "TheoryDrivingTestNotification" ];
	static final List<String> TIMESOFDAY = ["Afternoon", "Morning"];

	@observable String commentary;
	@observable DateTime endDate;
	@observable String id;
	@observable Intern intern;
	@observable String reason;
	@observable DateTime startDate;
	@observable String status;
	@observable String timeOfDayEnd;
	@observable String timeOfDayStart;
	@observable int version;

	DeclaredAbsence(DateTime startDate, String timeOfDayStart, DateTime endDate, String timeOfDayEnd, String status, String reason, Intern intern, [String commentary=null]) :
		this.commentary = commentary,
		this.endDate = endDate,
		this.id = null,
		this.intern = intern,
		this.reason = reason,
		this.startDate = startDate,
		this.status = status,
		this.timeOfDayEnd = timeOfDayEnd,
		this.timeOfDayStart = timeOfDayStart,
		this.version = 0;

	DeclaredAbsence.empty() : super()
	{
		this.intern = new Intern.empty();
	}

	DeclaredAbsence.fromMap(Map<String, dynamic> json) :
		this.commentary = json["commentary"],
		this.endDate = new DateTime.fromMillisecondsSinceEpoch(json["endDate"]),
		this.id = json["id"],
		this.intern = new Intern.fromMap(json["intern"] as Map),
		this.reason = json["reason"],
		this.status = json ["status"],
		this.startDate = new DateTime.fromMillisecondsSinceEpoch(json["startDate"]),
		this.timeOfDayEnd = json["timeOfDayEnd"],
		this.timeOfDayStart = json["timeOfDayStart"],
		this.version = json["version"];
	
	DeclaredAbsence.fromNotedAbsence(NotedAbsence na){
		this.startDate = na.date;
		this.endDate = na.date;
		this.timeOfDayStart = na.timeOfDay;
		this.timeOfDayEnd = na.timeOfDay;
	}

	Map<String, dynamic> toMapForCreation()
	{
		Map<String, dynamic> m = {
        	"endDate" : this.endDate.millisecondsSinceEpoch,
        	"reason" : this.reason,
        	"startDate" : this.startDate.millisecondsSinceEpoch,
        	"status" : this.status,
        	"timeOfDayEnd" : this.timeOfDayEnd,
        	"timeOfDayStart" : this.timeOfDayStart
        } as Map<String,dynamic>;

        if (this.commentary != null)	m.putIfAbsent("commentary", () => this.commentary);

        return m;
	}

	Map<String, dynamic> toMapForUpdate()
    {
    	return {
    		"commentary" : this.commentary,
        	"endDate" : this.endDate.millisecondsSinceEpoch,
        	"id" : this.id,
        	"intern" : this.intern.user.id,
        	"reason" : this.reason,
        	"startDate" : this.startDate.millisecondsSinceEpoch,
        	"status" : this.status,
        	"timeOfDayEnd" : this.timeOfDayEnd,
        	"timeOfDayStart" : this.timeOfDayStart,
        	"version" : this.version
    	} as Map<String,dynamic>;
    }

	String toString() => "DeclaredAbsence #$id ($status): ${intern.user.name} du $startDate ($timeOfDayStart) au $endDate ($timeOfDayEnd)\n\tMotif: $reason";

	/* Requests */
	static Future<DeclaredAbsence> declaredAbsence(String id) {
    	return new Future<DeclaredAbsence>.microtask(() {
        	HttpRequest req = new HttpRequest();
        	req.open("GET", SyagesInstance.shared.apiUrl("declaredAbsences/$id"), async: false);
    		addHeadersToRequest(req, requestHeaders());
    		req.send();
    		if (req.response == null) throw new RequestFailureException();

    		switch(req.status)
   			{
   				case 200:
					return new DeclaredAbsence.fromMap(JSON.decode(req.response));
            	case 400:
            		throw new BadRequestException("Erreur ${req.status}:\n\t${req.response}");
   				case 401:
   					throw new UnauthorizedException("Erreur ${req.status}:\n\t${req.response}");
   				default:
   					throw new UnknownException("Erreur ${req.status}:\n\t${req.response}");
   			}
   		});
   	}

	static Future<List<DeclaredAbsence>> declaredAbsenceList([int limit = 25, int offset = 0])
	{
		return new Future<List<DeclaredAbsence>>.microtask(() {
    		HttpRequest req = new HttpRequest();
    		req.open("GET", SyagesInstance.shared.apiUrl("declaredAbsences?limit=$limit&offset=$offset"), async: false);
			addHeadersToRequest(req, requestHeaders());
			req.send();
			if (req.response == null) throw new RequestFailureException();

			switch(req.status)
			{
				case 200:
					List<Map<String,dynamic>> res = JSON.decode(req.response);
					List<DeclaredAbsence> list = new List<DeclaredAbsence>();
					res.forEach((Map<String, dynamic> _) {
						list.add(new DeclaredAbsence.fromMap(_));
					});
					return list;
            	case 400:
            		throw new BadRequestException("Erreur ${req.status}:\n\t${req.response}");
				case 401:
					throw new UnauthorizedException("Erreur ${req.status}:\n\t${req.response}");
				case 404:
					throw new NotFoundException("Erreur ${req.status}:\n\t${req.response}");
				default:
					throw new UnknownException("Erreur ${req.status}:\n\t${req.response}");
			}
		});
	}

	Future<DeclaredAbsence> changestatus(String newStatus) {
    	return new Future<DeclaredAbsence>.microtask(() {
        	HttpRequest req = new HttpRequest();
        	req.open("PATCH", SyagesInstance.shared.apiUrl("declaredAbsences/$id/status"), async: false);
    		addHeadersToRequest(req, requestHeaders());

    		Map<String, dynamic> m = new Map<String, dynamic>();
    		m.putIfAbsent("status", () => newStatus);

    		req.send(JSON.encode(m));
    		if (req.response == null) throw new RequestFailureException();

    		switch(req.status)
   			{
   				case 200:
					return new DeclaredAbsence.fromMap(JSON.decode(req.response));
            	case 400:
            		throw new BadRequestException("Erreur ${req.status}:\n\t${req.response}");
   				case 401:
   					throw new UnauthorizedException("Erreur ${req.status}:\n\t${req.response}");
   				default:
   					throw new UnknownException("Erreur ${req.status}:\n\t${req.response}");
   			}
   		});
   	}

	Future<Empty> delete() {
    	return new Future<Empty>.microtask(() {
        	HttpRequest req = new HttpRequest();
        	req.open("DELETE", SyagesInstance.shared.apiUrl("declaredAbsences/$id"), async: false);
    		addHeadersToRequest(req, requestHeaders(null));
    		req.send();
    		if (req.response == null) throw new RequestFailureException();

    		switch(req.status)
   			{
   				case 204:
					return new Empty();
            	case 400:
            		throw new BadRequestException("Erreur ${req.status}:\n\t${req.response}");
   				case 404:
   					throw new NotFoundException("Erreur ${req.status}:\n\t${req.response}");
   				default:
   					throw new UnknownException("Erreur ${req.status}:\n\t${req.response}");
   			}
   		});
   	}

	Future<DeclaredAbsence> save()
	{
		if (id == null) return _save();
		else return _update();
	}

	Future<DeclaredAbsence> _save()
	{
		return new Future<DeclaredAbsence>.microtask(()
		{
			HttpRequest req = new HttpRequest();
			req.open("POST", SyagesInstance.shared.apiUrl("declaredAbsences"), async: false);
			addHeadersToRequest(req, requestHeaders());
			req.send(toJsonString(this.toMapForCreation()));
			if (req.response == null) throw new RequestFailureException();

			switch(req.status)
			{
				case 201:
					return new DeclaredAbsence.fromMap(JSON.decode(req.response));
            	case 400:
            		throw new BadRequestException("Erreur ${req.status}:\n\t${req.response}");
				case 401:
					throw new UnauthorizedException("Erreur ${req.status}:\n\t${req.response}");
				default:
					throw new UnknownException("Erreur ${req.status}:\n\t${req.response}");
			}
		});
	}

	Future<DeclaredAbsence> _update()
	{
		return new Future<DeclaredAbsence>.microtask(()
		{
        	HttpRequest req = new HttpRequest();
        	req.open("PUT", SyagesInstance.shared.apiUrl("declaredAbsences/$id"), async: false);
        	addHeadersToRequest(req, requestHeaders());
        	req.send(toJsonString(this.toMapForUpdate()));
        	if (req.response == null) throw new RequestFailureException();

        	switch(req.status)
        	{
        		case 201:
        			return new DeclaredAbsence.fromMap(JSON.decode(req.response));
            	case 400:
            		throw new BadRequestException("Erreur ${req.status}:\n\t${req.response}");
        		case 401:
        			throw new UnauthorizedException("Erreur ${req.status}:\n\t${req.response}");
        		default:
        			throw new UnknownException("Erreur ${req.status}:\n\t${req.response}");
        	}
        });
	}
}
