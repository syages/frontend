library syages.Annotations;

class config {
	const config();
}

class documented {
	const documented(String since);
}

class doNotCall {
	const doNotCall();
}

class debugOnly {
	const debugOnly();
}