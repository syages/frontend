@documented("f0e21a2e")
library syages.Exceptions;

import "Annotations.dart";

/// Exception levée lorsqu'une requete HTTP à retournée un code 400
class BadRequestException implements Exception {
	BadRequestException([dynamic message]) : _msg = message;
	dynamic _msg;
	toString() => _msg.toString();
}

/// Exception levée lorsqu'une requete HTTP à retournée un code 404
class NotFoundException implements Exception {
	NotFoundException([dynamic message]) : _msg = message;
	dynamic _msg;
	toString() => _msg.toString();
}

/// Exception levée lorsqu'une requete HTTP n'a pu aboutir
class RequestFailureException implements Exception {
	RequestFailureException([dynamic message]) : _msg = message;
	dynamic _msg;
	toString() => _msg.toString();
}

/// Exception levée lorsqu'une erreur est survenu dans au Runtime
class RuntimeException implements Exception {
	final String _message;

	RuntimeException(String message) :
		_message = message;

	String toString() => "Exception: $_message";
}

/// Exception levée lorsqu'une requete HTTP à retournée un code 401
class UnauthorizedException implements Exception {
	UnauthorizedException([dynamic message]) : _msg = message;
	dynamic _msg;
	toString() => _msg.toString();
}

/// Exception levée lorsqu'une requete HTTP à retournée un code innatendu
class UnknownException implements Exception {
	UnknownException([dynamic message]) : _msg = message;
	dynamic _msg;
	toString() => _msg.toString();
}