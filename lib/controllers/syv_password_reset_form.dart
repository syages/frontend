import 'dart:html';
import 'dart:async';
import 'dart:convert';
import 'package:polymer/polymer.dart';
import '../ContextFactory.dart';
import '../Exceptions.dart';
import '../Instance.dart';
import '../models/Empty.dart';
import '../Session.dart';
import '../Helpers.dart';
import '../ViewController.dart';

@CustomTag("syv-password-reset-form")
class RequestPasswordResetController extends ViewController {
    RequestPasswordResetController.created() : super.created();

    attached()
    {
    	super.attached();
    	if (SyagesSession.sharedInstance.isConnected) {
            new SyagesInstance().replaceRoute("dashboard");
       	}
    }

    @observable SendModel model = new SendModel();
    @observable bool isPhone = false;
    @published String token;
    bool valid;
    
    void tokenChanged(){
    	if(this.token != null){
    		new Future.microtask((){
    			HttpRequest req = new HttpRequest();
    			req.open('GET', SyagesInstance.shared.apiUrl("/validResetToken/$token"), async:false);
    			req.send();
    			if(req.response == null) throw new RequestFailureException();
    			switch(req.status)
    			{
    				case 200:
    					Map<String, String> res = JSON.decode(req.response);
    					this.valid = res['valid'] == 'true' ? true: false;
    					break;
    				case 400:
    					throw new BadRequestException("Erreur ${req.status}:\n\t${req.response}");
    				case 401:
    					throw new UnauthorizedException("Erreur ${req.status}:\n\t${req.response}");
    				default:
    					throw new UnknownException("Erreur ${req.status}:\n\t${req.response}");
    			}
    		}).catchError(errorHandler).whenComplete((){
    			if(!this.valid){
    				// add token invalid to ToastStack
    				cancel();
    			}
    		});
    	}
    }

    void send() 
    {
		if (model.password == null || model.confirm == null) {
			// TODO : Lancer un Toast => "Empty"
			return;
		}
		if (model.password.isEmpty || model.confirm.isEmpty){
			// TODO : Lancer un Toast => "Empty"
			return;
		}
		if(model.password != model.confirm){
			// TODO : Lancer un Toast => "Les deux saisies de Mot de passe ne correspondent pas"
			return;
		}
      
		new Future<Empty>.microtask(() 
		{
			HttpRequest req = new HttpRequest();
			req.open("POST", SyagesInstance.shared.apiUrl("password?token=" + token + "&password=" + model.password), async: false);
			addHeadersToRequest(req, requestHeaders(null));
			req.send();
            	
			if (req.response == null) throw new RequestFailureException();
			switch(req.status)
			{
				case 204:
					// TODO : lancer Toast ici => "Mot de passe correctement changé"
					cancel();
					break;
				case 400:
					throw new BadRequestException("Erreur ${req.status}:\n\t${req.response}");
				case 401:
					throw new UnauthorizedException("Erreur ${req.status}:\n\t${req.response}");
				default:
					throw new UnknownException("Erreur ${req.status}:\n\t${req.response}");
			}
		
		});
	}
        
    void cancel(){
    	SyagesInstance.shared.replaceContext(ContextFactory.login());
    }


    void _sendMessage([String reason = null]) {
    	this.$["send-popup"].show();
    }
    
    @override void deserializeState(Map<String, dynamic> state) {
    	this.token = state['tokenKey'];
    }
    @override Map<String, dynamic> serializedState() => new Map<String, dynamic>();
    @override bool shouldSerializeState() => false;
}

class SendModel extends Observable {
    @observable String password;
    @observable String confirm;
    @observable String message;
}
