import 'dart:html';
import 'dart:async';
import 'dart:convert';
import 'dart:math';
import 'package:polymer/polymer.dart';
import '../ContextFactory.dart';
import '../Helpers.dart';
import '../Exceptions.dart';
import '../Instance.dart';
import '../ViewController.dart';
import '../models/Intern.dart';
import '../models/Staff.dart';
import '../models/Teacher.dart';
import '../models/User.dart';

@CustomTag("syv-signup-form")
class SignupFormController extends ViewController{
	List available_kinds = [User.KIND_INTERN, User.KIND_STAFF, User.KIND_TEACHER];
	@observable DateTime birthDate = new DateTime(1990);
	@observable User user;
	@published String userId;

	SignupFormController.created(): super.created(){
		this.user = toObservable(new User.empty());
		var rand = new Random().nextInt(this.available_kinds.length);
		this.user.kind = User.KINDS[this.available_kinds[rand]];
	}
	
	void attached(){
		
	}
	
	void create()
	{
		switch(user.kind){
			case 'Intern':
				Intern intern = new Intern.empty();
    			intern.user = this.user;
    			intern.dateOfBirth = this.birthDate;
    			_saveIntern(intern).then((Intern i){
    				success();
    			}).catchError((e){
    				_errorMessage();
    				errorHandler(e);
    			});
				break;
			case 'Teacher':
				Teacher teacher = new Teacher.empty();
    			teacher.user = user;
    			_saveTeacher(teacher).then((Teacher t){
    				success();
    			}).catchError((l){
    				_errorMessage();
    			});
				break;
			case 'Staff':
				Staff staff = new Staff.empty();
    			staff.user = user;
    			_saveStaff(staff).then((Staff s){
    				success();
    			}).catchError((l){
    				_errorMessage();
    			});
				break;
				
		}
    }
	
	Future _saveIntern(Intern intern){
		return new Future<Intern>.microtask((){
			HttpRequest req = new HttpRequest();
			req.open("POST", SyagesInstance.shared.apiUrl("demo/interns"), async: false);
			addHeadersToRequest(req, requestHeaders());
			Map<String, dynamic> m  = intern.toMapForCreation();
			req.send(toJsonString(m));
			if (req.response == null) throw new RequestFailureException();

			switch(req.status)
			{
				case 201:
					return new Intern.fromMap(JSON.decode(req.response));
            	case 400:
            		throw new BadRequestException("Erreur ${req.status}:\n\t${req.response}");
				case 401:
					throw new UnauthorizedException("Erreur ${req.status}:\n\t${req.response}");
				default:
					throw new UnknownException("Erreur ${req.status}:\n\t${req.response}");
			}
		});
	}
	
	Future _saveStaff(Staff staff){
    		return new Future<Staff>.microtask((){
    			HttpRequest req = new HttpRequest();
    			req.open("POST", SyagesInstance.shared.apiUrl("demo/staff"), async: false);
    			addHeadersToRequest(req, requestHeaders());
    			req.send(toJsonString(staff.toMapForCreation()));
    			if (req.response == null) throw new RequestFailureException();

    			switch(req.status)
    			{
    				case 201:
    					return new Staff.fromMap(JSON.decode(req.response));
                	case 400:
                		throw new BadRequestException("Erreur ${req.status}:\n\t${req.response}");
    				case 401:
    					throw new UnauthorizedException("Erreur ${req.status}:\n\t${req.response}");
    				default:
    					throw new UnknownException("Erreur ${req.status}:\n\t${req.response}");
    			}
    		});
    	}
	
	Future _saveTeacher(Teacher teacher){
    		return new Future<Teacher>.microtask((){
    			HttpRequest req = new HttpRequest();
    			req.open("POST", SyagesInstance.shared.apiUrl("demo/teachers"), async: false);
    			addHeadersToRequest(req, requestHeaders());
    			req.send(toJsonString(teacher.toMapForCreation()));
    			if (req.response == null) throw new RequestFailureException();

    			switch(req.status)
    			{
    				case 201:
    					return new Teacher.fromMap(JSON.decode(req.response));
                	case 400:
                		throw new BadRequestException("Erreur ${req.status}:\n\t${req.response}");
    				case 401:
    					throw new UnauthorizedException("Erreur ${req.status}:\n\t${req.response}");
    				default:
    					throw new UnknownException("Erreur ${req.status}:\n\t${req.response}");
    			}
    		});
    	}
	
	void success(){
		_successMessage();
		new Timer(new Duration(seconds: 5), cancel);
	}

	void cancel(){
		SyagesInstance.shared.replaceContext(ContextFactory.login());
	}
	
	get filterKind => () => (String string){
		switch(string){
			case "Administrator":
				return "Administrateur";
			case "Intern":
				return "Stagiaire";
			case "President":
				break;
			case "Staff":
				return "Assistant";
			case "Teacher":
				return "Enseignant";
			case "Unknown":
				return "Inconnu";
		}
	};
	
	
	void _successMessage(){
		this.$['success-popup'].show();
	}
	
	void _errorMessage(){
		this.$["error-popup"].show();
	}

	@override void deserializeState(Map<String, dynamic> state){}
	@override Map<String, dynamic> serializedState() => new Map<String, dynamic>();
	@override bool shouldSerializeState() => false;
}