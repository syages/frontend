import 'package:polymer/polymer.dart';
import '../Instance.dart';
import '../syages_app.dart';
import '../ViewController.dart';

@CustomTag("syv-dashboard")
class DashboardController extends ViewController {
	
	@observable DateTime t = new DateTime.now();

	DashboardController.created() : super.created();

	ready() {
		super.ready();
	}
	
	int toastCount = 0;
	porteUnToast() {
		++toastCount;
		if (toastCount %2 == 0) SyagesInstance.shared.applicationController.porterUnToast(new Toast("Toast #$toastCount"));
		else SyagesInstance.shared.applicationController.porterUnToast(new ActionToast("Toast #$toastCount", "Action", () {
			print("coucou");
		}));
	}

	void deserializeState(Map<String, dynamic> state) {}
    Map<String, dynamic> serializedState() => {};
    bool shouldSerializeState() { return true; }
}