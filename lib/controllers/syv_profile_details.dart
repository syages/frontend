import 'dart:async';
import 'dart:html';
import 'package:polymer/polymer.dart';
import '../ContextFactory.dart';
import '../Instance.dart';
import '../ViewController.dart';
import '../models/Administrator.dart';
import '../models/Intern.dart';
import '../models/President.dart';
import '../models/Staff.dart';
import '../models/Teacher.dart';
import '../models/User.dart';

@CustomTag("syv-profile-details")
class ProfileDetailsController extends ViewController{
	@observable Administrator administrator;
	@observable String office;
	@observable String number;
	@observable Intern intern;
	@observable President president;
	@observable Staff staff;
	@observable Teacher teacher;
	@observable User user;
	@published String userId;

	ProfileDetailsController.created(): super.created(){
		User.me().then((user)
		{
			this.user = toObservable(user);
			// Administrator
			if(user.kind==User.KINDS[User.KIND_ADMINISTRATOR]){
				Administrator.administrator(user.id).then((admin)
				{
					this.administrator = admin;
					this.office = admin.office;
					this.number = admin.number;
				}).catchError(errorHandler);
			}
			// Intern
			else if(user.kind==User.KINDS[User.KIND_INTERN]){
				Intern.intern(user.id).then((intern)
				{
					this.intern = toObservable(intern);
				}).catchError(errorHandler);
			}
			// President
			else if(user.kind==User.KINDS[User.KIND_PRESIDENT]){
            	President.president(user.id).then((president)
				{
            		this.president = president;
            		this.office = president.office;
					this.number = president.number;
            	}).catchError(errorHandler);
            }
			else if(user.kind==User.KINDS[User.KIND_STAFF]){
				Staff.staff(user.id).then((staff)
				{
					this.staff = staff;
					this.office = staff.office;
					this.number = staff.number;
				}).catchError(errorHandler);
			}
			else if(user.kind==User.KINDS[User.KIND_TEACHER]){
            	Teacher.teacher(user.id).then((teacher)
				{
            		this.teacher = teacher;
            		this.office = teacher.office;
					this.number = teacher.number;
            	}).catchError(errorHandler);
            }
			else{	
				//User.kind == UNKNOWN
			}
			
		}).catchError(errorHandler);
	}

	profileIdChanged(){
		if(this.userId != "new"){
			Future<User> future;
			future = User.user(this.userId);
			future.then((User user){
				this.user = toObservable(user);
			}).catchError(errorHandler);
		}
	}

	void edit(Event event)
	{
		SyagesInstance.shared.pushContext(ContextFactory.profileSettings());
    }

	@override void deserializeState(Map<String, dynamic> state) {
		this.userId = state["userId"];
	}
	@override Map<String, dynamic> serializedState() => new Map<String, dynamic>();
	@override bool shouldSerializeState() => false;
}