import 'dart:html';
import 'package:core_elements/core_collapse.dart';
import 'package:polymer/polymer.dart';
import '../Context.dart';
import '../ContextFactory.dart';
import '../Instance.dart';
import '../NavBarFactory.dart';
import '../syages_app.dart';
import '../SerializableElement.dart';
import '../Session.dart';
import '../models/User.dart';


@CustomTag("syv-navbar")
class NavbarController extends SerializableElement {
	@observable bool canGoBack = false, canGoForward = false, canGoHome = false;
	@published bool displayed = false;
	@observable List<IAction> navbar;
	@observable User user;

	NavbarController.created() : super.created()
	{
		this.navbar = new List<IAction>();

		// Context-changes Listeners
		new SyagesInstance().onContextChange.listen(_onContextChangeHandler);

		// Session Listeners
        SyagesSession.sharedInstance.onLogin.listen(_onLoginHandler);
        if (SyagesSession.sharedInstance.isConnected) _onLoginHandler(SyagesSession.sharedInstance);
        SyagesSession.sharedInstance.onLogout.listen(_onLogoutHandler);

		// Window resizing listeners
		window.onResize.listen(_onResizeHandler);
		window.onAnimationEnd.listen(_onResizeHandler);
		window.onTransitionEnd.listen(_onResizeHandler);
	}

	attached() {
		super.attached();
		_onContextChangeHandler(null);
		_onResizeHandler(null);
	}

	// Navigation
	void goBack() {
		if (this.canGoBack) SyagesInstance.shared.popContext();
	}

	void goForward() {
		if (this.canGoForward) SyagesInstance.shared.repushContext();
	}

	void goHome() {
		if (this.canGoHome) SyagesInstance.shared.pushContext(ContextFactory.dashboard());
	}

	void goToProfileSettings() {
		SyagesInstance.shared.pushContext(ContextFactory.profileDetails());
	}

	void logout() {
		SyagesSession.sharedInstance.logout();
	}

	void toggle() {
		SyagesInstance.shared.applicationController.userToggledNavigation();
	}

	// Handlers
	void _onContextChangeHandler(SyagesAppEvent ev) {
		this.canGoBack = new SyagesInstance().historyStack.length > 1;
		this.canGoForward = new SyagesInstance().repushStack.length > 0;
		this.canGoHome = SyagesInstance.shared.currentContext != null ? SyagesInstance.shared.currentContext.configuration.controller != "syv-dashboard" : true;
		// TODO: Mettre à jour le selector
	}

	void _onLoginHandler(SyagesSession session) {
		User.me().then((User u) {
			this.user = u;
			this.navbar = toObservable(NavBarFactory.generateForUser(this.user));
		}).catchError(errorHandler);
    }

	void _onLogoutHandler(SyagesSession session) {
    	this.navbar.clear();
    	this.user = null;
    }

	void _onResizeHandler(Event e) {
		var bar = this.clientHeight;
		var footer = ($["footer"] as Element).clientHeight;
		var header = ($["header"] as Element).clientHeight;

		($["links"] as Element).style.height = (bar-(footer+header)).toString()+ "px";
	}
	
	void deserializeState(Map<String, dynamic> state) {}
	Map<String, dynamic> serializedState() => {};
}

@CustomTag("sye-navbar-action")
class ActionController extends PolymerElement {

	@published Action model;

	ActionController.created() : super.created();

	void onTap() {
		(SyagesInstance.shared.applicationController.$["navbar"] as NavbarController).toggle();
		if (this.model != null)
			this.model.onTap();
	}

}

@CustomTag("sye-navbar-group")
class ActionGroupController extends PolymerElement {

	CoreCollapse get collapseEl => $['collapseEl'] as CoreCollapse;
	@published ActionGroup model;

	ActionGroupController.created() : super.created();

	void onTap() {
    	if (this.model != null)
    		this.model.onTap();
    	if (this.collapseEl != null)
    		this.collapseEl.opened = this.model.collapsed;
    }

}

/* Models */
abstract class IAction extends Observable {
	@observable String label;
	@observable final bool isGroup;

	IAction(String label, bool isGroup) :
		this.isGroup = isGroup,
		this.label = label;

	void onTap();

	String toString() => (isGroup ? "ActionGroup" : "Action") +": $label";
}

class Action extends IAction {
	@observable Context context;

	Action(String label, Context context) :
		super(label, false),
		context = context;

	void onTap() {
		if (context == null) return;
		new SyagesInstance().pushContext(context);
	}
}

class ActionGroup extends IAction {
	@observable List<Action> actions;
	@observable bool collapsed;

	ActionGroup(String label) :
		super(label, true),
		actions = toObservable(new List<Action>()),
		collapsed = toObservable(false);

	void onTap() {
		collapsed = !collapsed;
	}
}
