import 'dart:async';
import 'dart:html';
import 'package:polymer/polymer.dart';
import '../ContextFactory.dart';
import '../Instance.dart';
import '../models/Information.dart';
import '../models/User.dart';
import '../ViewController.dart';
import '../syages_app.dart';

@CustomTag("syv-newsfeed")
class NewsFeedController extends ViewController{
	@observable List<Information> informations;

	NewsFeedController.created(): super.created(){
		this.informations = null;
		this.refresh();
	}

	void refresh() {
		Future<List<Information>> future;
		future = Information.informationsList();
		future.then((List<Information> l) {
			this.informations = toObservable(l);
		}).catchError((error) {
			this.informations = null;
			errorHandler(error);
		});
	}

	void create(Event event){
		SyagesInstance.shared.pushContext(ContextFactory.aps_newsForm());
	}

	void edit(Event event){
		String informationId = (event.target as Element).attributes["data-information-id"];
		if(loggedUser.kind==User.KINDS[User.KIND_TEACHER])
		{	informations.forEach((information)
			{	if(information.id==informationId && information.author.id==loggedUser.id)
					SyagesInstance.shared.pushContext(ContextFactory.aps_newsForm(informationId));
			});
			SyagesInstance.shared.applicationController.porterUnToast(
					new Toast("Vous n'avez pas créé cette actualité, vous en pouvez pas la modifier."));
			return;
		}
		SyagesInstance.shared.pushContext(ContextFactory.aps_newsForm(informationId));
	}


	@override void deserializeState(Map<String, dynamic> state) {}
    @override Map<String, dynamic> serializedState() => {};
    @override bool shouldSerializeState() { return true; }
}