import 'package:polymer/polymer.dart';
import '../../ContextFactory.dart';
import '../../Instance.dart';
import '../../ViewController.dart';
import '../../models/Report.dart';

@CustomTag("syv-bugreport-detail--a")
class BugReportDetailController extends ViewController {

	@observable Report report;
	@published String reportId;

	BugReportDetailController.created() : super.created();
	
	changeStatus() {
		report.changeStatus(report.status == "Opened").then((Report r) { this.report=r; }).catchError(errorHandler);
	}
	
	delete() {
		report.delete().catchError(errorHandler).then((dummy) { list(); });
	}
	
	list() {
		SyagesInstance.shared.pushContext(ContextFactory.a_bugReports());
	}
	
	reportIdChanged() {
		Report.report(this.reportId).then((r) {
        	this.report = toObservable(r);
		}).catchError(errorHandler);
	}

	@override void deserializeState(Map<String, dynamic> state) {
		this.reportId = state["reportId"];
		reportIdChanged();
	}
	@override Map<String, dynamic> serializedState() => {
		"report": this.report.toMapForUpdate()
	};
	@override bool shouldSerializeState() => false;
}
