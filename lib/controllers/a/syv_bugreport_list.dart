import 'dart:async';
import 'dart:html';
import 'package:polymer/polymer.dart';
import '../../ContextFactory.dart';
import '../../Instance.dart';
import '../../ViewController.dart';
import '../../models/Report.dart';

@CustomTag("syv-bugreport-list--a")
class BugReportListController extends ViewController {
	@observable String filter; // 0 = all, 1 = open, 2 = closed
	@observable List<Report> reports;

	BugReportListController.created() : super.created()
	{
		this.filter = "0";
		this.reports = null;
		this.refresh();
	}

	void more(Event event)
	{
		String reportId = (event.target as Element).attributes["data-report-id"];
		SyagesInstance.shared.pushContext(ContextFactory.a_bugReportDetail(reportId));
	}

	void refresh() {
		Future<List<Report>> future;
		switch (int.parse(this.filter))
		{
			case 1:
				future = Report.openedReportsList();
				break;
			case 2:
				future = Report.closedReportsList();
				break;
			default:
				future = Report.reportsList();
		}

		future.then((List<Report> l) {
			this.reports = toObservable(l);
		}).catchError((error) {
			this.reports = null;
			errorHandler(error);
		});
	}

	@override void deserializeState(Map<String, dynamic> state) {}
	@override Map<String, dynamic> serializedState() => new Map<String, dynamic>();
	@override bool shouldSerializeState() => false;
}
