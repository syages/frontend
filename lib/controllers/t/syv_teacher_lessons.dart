import 'dart:html';
import 'package:polymer/polymer.dart';
import '../../ContextFactory.dart';
import '../../Instance.dart';
import '../../ViewController.dart';
import '../../models/Lesson.dart';
import '../../models/User.dart';

@CustomTag("syv-teacher-lessons--t")
class TeacherLessonsController extends ViewController {
	@observable List<Lesson> lessons;

	TeacherLessonsController.created(): super.created(){
		this.lessons = null;
		load();
	}

	void load(){
		User.meLessons()
			.then((List<Lesson> l) {
				this.lessons = toObservable(l);
    		}).catchError((error) {
    			this.lessons = null;
    			errorHandler(error);
    		});

	}

	void open(Event event){
		String lessonId = (event.currentTarget as Element).attributes["data-lesson-id"];
		SyagesInstance.shared.pushContext(ContextFactory.t_classroom(lessonId));
	}


	@override void deserializeState(Map<String, dynamic> state) {}
    @override Map<String, dynamic> serializedState() => {};
    @override bool shouldSerializeState() { return false; }
}