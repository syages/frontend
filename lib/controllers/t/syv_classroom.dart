import 'dart:html';
import 'package:polymer/polymer.dart';
import '../../ContextFactory.dart';
import '../../Instance.dart';
import '../../ViewController.dart';
import '../../models/Evaluation.dart';
import '../../models/Lecture.dart';
import '../../models/Lesson.dart';

@CustomTag('syv-classroom--t')
class TeacherClassroomController extends ViewController{
	@observable List<Lecture> lectures;
	@observable List<Evaluation> evaluations;
	@observable Lesson lesson;
	@published String lessonId;

	TeacherClassroomController.created() : super.created(){
		this.lesson = toObservable(new Lesson.empty());
		this.lectures = null;
		this.evaluations = null;
	}

	lessonIdChanged(){
		Lesson.lesson(lessonId).then((Lesson l){
			this.lesson = toObservable(l);
			lesson.lectures().then((List<Lecture> lectures){
    			this.lectures = toObservable(lectures);
    		}).catchError(errorHandler);
    		lesson.evaluations().then((List<Evaluation> evaluations){
    			this.evaluations = toObservable(evaluations);
    		}).catchError(errorHandler);
		}).catchError(errorHandler);

	}
	
	openEvaluation(Event event){
		var evaluationId = (event.currentTarget as Element).attributes["data-evaluation-id"];
		SyagesInstance.shared.pushContext(ContextFactory.apst_evaluationDetail(this.lessonId, this.lesson.course.name, evaluationId));
	}
	
	openLecture(Event event){
		var lectureId = (event.currentTarget as Element).attributes["data-lecture-id"];
		SyagesInstance.shared.pushContext(ContextFactory.apst_lectureDetail(this.lessonId, this.lesson.course.name, lectureId));
	}

	addEval(Event event){
		SyagesInstance.shared.pushContext(ContextFactory.apst_evaluationForm(this.lessonId, this.lesson.course.name));
	}

	addLecture(Event event){
		SyagesInstance.shared.pushContext(ContextFactory.apst_lectureForm(this.lessonId, this.lesson.course.name));
	}
	

	@override void deserializeState(Map<String, dynamic> state) {
		this.lessonId = state["lessonId"];
	}
	@override Map<String, dynamic> serializedState() =>{};
	@override bool shouldSerializeState() => false;
}