import 'dart:async';
import 'package:polymer/polymer.dart';
import '../ContextFactory.dart';
import '../Instance.dart';
import '../Session.dart';
import '../ViewController.dart';
import '../models/Intern.dart';
import '../models/User.dart';

@CustomTag("syv-login")
class LoginController extends ViewController {
    LoginController.created() : super.created();

    attached()
    {
    	if (SyagesSession.sharedInstance.isConnected) {
            new SyagesInstance().replaceRoute("dashboard");
       	}
    }

    @observable LoginModel model = new LoginModel();
    @observable bool isPhone = false;
    String _onLogin = null;

    void login() {
        if (model.email == null || model.password == null) {
        	_loginError("empty"); return;
        }
        if (model.email.isEmpty || model.password.isEmpty){
        	_loginError("empty"); return;
        }
        if (SyagesSession.sharedInstance.authenticate(model.email, model.password) == false) {
        	_loginError("wrongCredentials"); return;
        }
		if (_onLogin == "popContext") {
			new SyagesInstance().popContext();
			return;
		}
		Future<User> me = SyagesSession.sharedInstance.loggedUser;
		me.then((User me){
			if (me.kind == "Intern"){
				Future<bool> futureFirst = Intern.isFirst(me.id);
                futureFirst.then((bool first){
            		if (first)
                	{
            			SyagesInstance.shared.pushContext(ContextFactory.i_firstLogin());
                	} else {
                		new SyagesInstance().replaceRoute("dashboard");
                	}
             	}).catchError(errorHandler);
			} else {
				new SyagesInstance().replaceRoute("dashboard");
			}
		}).catchError(errorHandler);
    }
    
    void reset(){
    	SyagesInstance.shared.pushContext(ContextFactory.forgotPassword());
    }
    
    void create(){
    	SyagesInstance.shared.pushContext(ContextFactory.signupForm());
    }

    void _loginError([String reason = null]) {
    	this.$["login-error-popup"].show();
    }

    void deserializeState(Map<String, dynamic> state) {
    	_onLogin = state["onLogin"];
    }
    Map<String, dynamic> serializedState() => new Map<String, dynamic>();
}

class LoginModel extends Observable {
    @observable String email;
    @observable String error;
    @observable String password;
}
