import 'dart:html';
import 'package:polymer/polymer.dart';
import '../models/Report.dart';

@CustomTag("syd-error-reporter")
class ErrorReporterController extends PolymerElement {
	@observable bool deployed = false;
	@observable String message = "";
	@observable String priority = "1";
	@observable String type = "Bug";

	ErrorReporterController.created() : super.created()
	{
		window.onResize.listen(onResizeHandler);
	}

	@override
	void attached() {
		super.attached();
		onResizeHandler(null);
	}

	void onResizeHandler(Event e) {
		var toolbarHeight = (this.$["toolbar"] as Element).clientHeight;
		var dialogHeight = (this.$["dialog"] as Element).clientHeight;
		var finalHeight = dialogHeight - toolbarHeight;
		(this.$["dialog-panel"] as Element).style.height = finalHeight.toString() + "px";
	}

	void toggle() {
		deployed = !deployed;
		if (deployed == false)
		{
        	this.message = "";
			this.priority = "1";
			this.type = "Bug";
		} else {
			onResizeHandler(null);
		}
	}

	void send() {
		if (type == "Bug") {
			new Report.bug(int.parse(this.priority), this.message).save().whenComplete(() {
				this.toggle();
			});
		} else if (type == "Message") {
			new Report.message(this.message).save().whenComplete(() {
				this.toggle();
			});
		} else {
			this.toggle();
		}
	}
}