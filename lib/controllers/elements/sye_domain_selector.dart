import 'dart:async';
import 'package:polymer/polymer.dart';
import '../../Helpers.dart';
import '../../SerializableElement.dart';
import '../../models/Domain.dart';

@CustomTag("sye-domain-selector")
class DomainSelector extends SerializableElement {
	@observable List<Domain> domains;
	@observable var selected = -1;

	@published
	Domain get value => readValue(#value);
    set value(Domain val) => writeValue(#value, val);

	DomainSelector.created(): super.created(){
		this.domains= null;
		this.refresh();
	}

	void refresh(){
		Future<List<Domain>> future;
		future = Domain.domainList();
		future.then((List<Domain> domains){
			this.domains = toObservable(domains);
		}).catchError((error){
			this.domains = null;
			errorHandler(error);
		});
	}

	valueChanged(){
		if(this.domains!= null && this.value != null && this.value.id != null){
			this.selected = this.domains.indexOf(this.domains.singleWhere((i) => i.id == this.value.id));
			warn(this.selected);
		}
	}

	selectedChanged(){
		this.value = this.domains.elementAt(this.selected);
	}
	
	void deserializeState(Map<String, dynamic> state) {}
	Map<String, dynamic> serializedState() => {};
}