import 'package:polymer/polymer.dart';
import '../../ExprFilters.dart';
import '../../Helpers.dart';
import 'dart:html';

@CustomTag('sye-md-timepicker-button')
class TimePickerButton extends PolymerElement{
	@published
	DateTime get value => readValue(#value);
	set value(DateTime val) => writeValue(#value, val);
	
	TimePickerButton.created():super.created(){
		this.value = new DateTime.now();
	}
	
	Function formatTime = filters.time;
	
	void onTap(){
    	this.$['dialog'].open();
    }
	
	timeSelected(Event e, var details){
		warn(details);
	}
	
	valueChanged(){
		warn(this.value);
	}
	
	
}