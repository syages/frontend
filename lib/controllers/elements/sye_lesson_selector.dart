import 'dart:async';
import 'package:polymer/polymer.dart';
import '../../Helpers.dart';
import '../../SerializableElement.dart';
import '../../models/Lesson.dart';
import '../../models/Diploma.dart';

@CustomTag("sye-lesson-selector")
class LessonSelector extends SerializableElement {
	@observable List<Lesson> lessons;
	@observable var selected = -1;
	@published String diplomaId;
	@observable Diploma diploma;

	@published
	Lesson get value => readValue(#value);
    set value(Lesson val) => writeValue(#value, val);

    LessonSelector.created(): super.created(){
		this.lessons= null;
	}

	diplomaIdChanged(){
		Diploma.diploma(diplomaId).then((d){
				d.lessons().then((List<Lesson> lessons){
					this.lessons = toObservable(lessons.where((l) => l.optional));
					valueChanged();
				}).catchError((error){
					this.lessons = null;
					errorHandler(error);
				}).whenComplete(()=>valueChanged());
		});
	}

	valueChanged(){
		if(this.lessons!= null && this.value != null){
			this.selected = this.lessons.indexOf(this.lessons.singleWhere((i) => i.id == this.value.id));
		}
	}

	selectedChanged(){
		this.value = this.lessons.elementAt(this.selected);
	}
	
	void deserializeState(Map<String, dynamic> state) {}
	Map<String, dynamic> serializedState() => {};
}