import 'package:polymer/polymer.dart';
import '../../ExprFilters.dart';

@CustomTag('sye-date-picker-button')
class DatePickerButton extends PolymerElement{
	
	@published
	DateTime get date => readValue(#date);
    set date(DateTime value) => writeValue(#date, value);
	
	DatePickerButton.created(): super.created(){
		this.date = new DateTime.now();
	}
	
	Function formatDate = filters.date;
	
	void onTap(){
		this.$['dateDialog'].open();
	}
}