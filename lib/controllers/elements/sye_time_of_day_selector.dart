import 'package:polymer/polymer.dart';
import '../../SerializableElement.dart';

@CustomTag("sye-time-of-day-selector")
class TimeOfDaySelector extends SerializableElement {
	@observable List<String> timeOfDays = ['Morning', 'Afternoon'];
	@observable var selected;

	@published
	String get value => readValue(#value);
    set value(String val) => writeValue(#value, val);

	TimeOfDaySelector.created(): super.created(){
		this.selected = 0;
		selectedChanged();
	}

	valueChanged(){
		if(!this.timeOfDays.contains(this.value)){
			this.value = this.timeOfDays[0];
		}
		this.selected = this.timeOfDays.indexOf(this.value);
	}

	selectedChanged(){
		this.value = this.timeOfDays.elementAt(this.selected);
	}
	
	void deserializeState(Map<String, dynamic> state) {}
	Map<String, dynamic> serializedState() => {};
}