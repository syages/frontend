import 'dart:async';
import 'dart:html';
import 'package:polymer/polymer.dart';
import '../../SerializableElement.dart';
import '../../models/Domain.dart';

@CustomTag("sye-domains-selector")
class DomainsSelector extends SerializableElement {
	@observable List<Domain> domains;
	@observable Map<String,bool> checkedDomains;

	@published
	List<Domain> get values => readValue(#values);
    set values(List<Domain> value) => writeValue(#values, value);

	DomainsSelector.created(): super.created(){
		this.domains= null;
		this.refresh();
	}

	void refresh(){
		Future<List<Domain>> future;
		future = Domain.domainList();
		future.then((List<Domain> domains){
			this.domains = toObservable(domains);
			this.checkedDomains = toObservable(new Map.fromIterable(this.domains,
			                                                    key: (item) => item.id.toString(),
			                                                   	value: (item) => false));
		}).catchError((error){
			this.domains = null;
			errorHandler(error);
		});
	}

	valuesChanged(oldValue, newValue){
		if(oldValue == null){
			this.values.forEach((input){
				this.checkedDomains[input.id] = true;
			});
		}
	}

	void updateValues(){
		this.values = new List();
		this.checkedDomains.forEach((k,v){
			if(v){
				this.values.add(this.domains.singleWhere((item) => item.id == k));
			}
		});
	}

	changeListener(Event e, var detail, Node target){
		updateValues();
	}
	
	void deserializeState(Map<String, dynamic> state) {}
	Map<String, dynamic> serializedState() => {};
}