import 'dart:async';
import 'package:polymer/polymer.dart';
import '../../Helpers.dart';
import '../../SerializableElement.dart';
import '../../models/Teacher.dart';

@CustomTag("sye-teacher-selector")
class TeacherSelector extends SerializableElement {
	@observable List<Teacher> teachers;
	@observable var selected = -1;

	@published
	Teacher get value => readValue(#value);
    set value(Teacher val) => writeValue(#value, val);

	TeacherSelector.created(): super.created(){
		this.teachers= null;
		this.refresh();
	}

	void refresh(){
		Future<List<Teacher>> future;
		future = Teacher.teachersList();
		future.then((List<Teacher> teachers){
			this.teachers = toObservable(teachers);
			valueChanged();
		}).catchError(errorHandler);
	}

	valueChanged(){
		if(this.teachers!= null && this.value != null && (this.value.id != null || this.value.user.id != null)){
			this.selected = this.teachers.indexOf(this.teachers.singleWhere((i) => i.id == this.value.id || i.id == this.value.user.id));
		}
	}

	selectedChanged(){
		this.value = this.teachers.elementAt(this.selected);
	}
	
	void deserializeState(Map<String, dynamic> state) {}
	Map<String, dynamic> serializedState() => {};
}