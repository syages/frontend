import 'dart:async';
import 'package:polymer/polymer.dart';
import '../../Helpers.dart';
import '../../SerializableElement.dart';
import '../../models/Diploma.dart';

@CustomTag("sye-diploma-selector")
class DiplomaSelector extends SerializableElement {
	@observable List<Diploma> diplomas;
	@observable var selected = -1;

	@published
	Diploma get value => readValue(#value);
    set value(Diploma val) => writeValue(#value, val);

    DiplomaSelector.created(): super.created(){
		this.diplomas= null;
		this.refresh();
	}

	void refresh(){
		Future<List<Diploma>> future;
		future = Diploma.diplomaList();
		future.then((List<Diploma> diplomas){
			this.diplomas = toObservable(diplomas);
			valueChanged();
		}).catchError((error){
			this.diplomas = null;
			errorHandler(error);
		});
	}

	valueChanged(){
		if(this.diplomas!= null && this.value != null){
			this.selected = this.diplomas.indexOf(this.diplomas.singleWhere((i) => i.id == this.value.id));
		}
	}

	selectedChanged(){
		this.value = this.diplomas.elementAt(this.selected);
	}
	
	void deserializeState(Map<String, dynamic> state) {}
	Map<String, dynamic> serializedState() => {};
}