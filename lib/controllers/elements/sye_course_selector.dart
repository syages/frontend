import 'dart:async';
import 'package:polymer/polymer.dart';
import '../../Helpers.dart';
import '../../SerializableElement.dart';
import '../../models/Course.dart';

@CustomTag("sye-course-selector")
class CourseSelector extends SerializableElement {
	@observable List<Course> courses;
	@observable var selected = -1;

	@published
	Course get value => readValue(#value);
    set value(Course val) => writeValue(#value, val);

	CourseSelector.created(): super.created(){
		this.courses = null;
		this.refresh();
	}

	void refresh(){
		Future<List<Course>> future;
		future = Course.courseList();
		future.then((List<Course> courses){
			this.courses = toObservable(courses);
			valueChanged();
		}).catchError(errorHandler);
	}

	valueChanged(){
		if(this.courses!= null && this.value != null && this.value.id != null){
			this.selected = this.courses.indexOf(this.courses.singleWhere((i) => i.id == this.value.id));
		}
	}

	selectedChanged(){
		this.value = this.courses.elementAt(this.selected);
	}
	
	void deserializeState(Map<String, dynamic> state) {}
	Map<String, dynamic> serializedState() => {};
}