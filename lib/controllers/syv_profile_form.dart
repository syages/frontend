import 'dart:async';
import 'dart:html';
import 'package:polymer/polymer.dart';
import '../ContextFactory.dart';
import '../Helpers.dart';
import '../Instance.dart';
import '../ViewController.dart';
import '../models/Administrator.dart';
import '../models/Empty.dart';
import '../models/Intern.dart';
import '../models/President.dart';
import '../models/Staff.dart';
import '../models/Teacher.dart';
import '../models/User.dart';
import '../syages_app.dart';

@CustomTag("syv-profile-form")
class ProfileFormController extends ViewController{
	@observable Administrator administrator;
	@observable String office;
	@observable String number;
	@observable Intern intern;
	@observable President president;
	@observable Staff staff;
	@observable Teacher teacher;
	@observable User user;
	@published String userId;

	ProfileFormController.created(): super.created(){
		User.me().then((user)
		{
			this.user = toObservable(user);
			// Administrator
			if(user.kind==User.KINDS[User.KIND_ADMINISTRATOR]){
				Administrator.administrator(user.id).then((admin)
				{
					this.administrator = admin;
					this.office = admin.office;
					this.number = admin.number;
				}).catchError(errorHandler);
			}
			// Intern
			else if(user.kind==User.KINDS[User.KIND_INTERN]){
				Intern.intern(user.id).then((intern)
				{
					this.intern = toObservable(intern);
				}).catchError(errorHandler);
			}
			// President
			else if(user.kind==User.KINDS[User.KIND_PRESIDENT]){
            	President.president(user.id).then((president)
				{
            		this.president = president;
            		this.office = president.office;
					this.number = president.number;
            	}).catchError(errorHandler);
            }
			else if(user.kind==User.KINDS[User.KIND_STAFF]){
				Staff.staff(user.id).then((staff)
				{
					this.staff = staff;
					this.office = staff.office;
					this.number = staff.number;
				}).catchError(errorHandler);
			}
			else if(user.kind==User.KINDS[User.KIND_TEACHER]){
            	Teacher.teacher(user.id).then((teacher)
				{
            		this.teacher = teacher;
            		this.office = teacher.office;
					this.number = teacher.number;
            	}).catchError(errorHandler);
            }
			else{	
				//User.kind == UNKNOWN
			}
			
		}).catchError(errorHandler);
	}
	
	dropImage(Event event, var details, Node target)
	{
		SyagesInstance.shared.applicationController.porterUnToast(
        					new Toast("Image en téléchargement... Vueuillez patienter."));
		
		new Future<Empty>.microtask(()
        {
        	// l'image se trouve dans details.files
            // montrer le chargement du fichier vers le serveur
            final files = details['files'];
            this.loggedUser.setPicture(files[0]).then((res)
            {
	      		// if(res){
	    	 	// // picture uploaded
	      		// }
	            SyagesInstance.shared.popContext();
	            SyagesInstance.shared.pushContext(ContextFactory.profileSettings());
            }).catchError(errorHandler);
        }).then((e)
		{
			SyagesInstance.shared.applicationController.porterUnToast(
                    					new Toast("Image téléchargé"));
		});
	}


	profileIdChanged(){
		if(this.userId != "new"){
			Future<User> future;
			future = User.user(this.userId);
			future.then((User user){
				this.user = toObservable(user);
			}).catchError(errorHandler);
		}
	}

	void save(Event event)
	{
		String kind = this.loggedUser.kind;
		if(kind==User.KINDS[User.KIND_ADMINISTRATOR])
		{	administrator.number = number;
			administrator.office = office;
			administrator.save().then((u){});
		}else if(kind==User.KINDS[User.KIND_INTERN])
		{	intern.save().then((u){});
		}else if(kind==User.KINDS[User.KIND_PRESIDENT])
		{	president.number = number;
			administrator.office = office;
			president.save().then((u){});
		}else if(kind==User.KINDS[User.KIND_STAFF])
		{	staff.number = number;
			staff.office = office;
			staff.save().then((u){});
		}else if(kind==User.KINDS[User.KIND_TEACHER])
		{	teacher.number = number;
			teacher.office = office;
			teacher.save().then((u){});
		}
		SyagesInstance.shared.popContext();
    }

	void cancel(Event event){
		SyagesInstance.shared.popContext();
	}

	@override void deserializeState(Map<String, dynamic> state) {
		this.userId = state["userId"];
	}
	@override Map<String, dynamic> serializedState() => new Map<String, dynamic>();
	@override bool shouldSerializeState() => false;
}