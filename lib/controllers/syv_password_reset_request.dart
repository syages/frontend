import 'dart:html';
import 'dart:async';
import 'package:polymer/polymer.dart';
import '../Exceptions.dart';
import '../Helpers.dart';
import '../Instance.dart';
import '../models/Empty.dart';
import '../Session.dart';
import '../ViewController.dart';

@CustomTag("syv-password-reset-request")
class RequestPasswordResetController extends ViewController {
    RequestPasswordResetController.created() : super.created();

    attached()
    {
    	if (SyagesSession.sharedInstance.isConnected) {
            new SyagesInstance().replaceRoute("dashboard");
       	}
    }

    @observable SendModel model = new SendModel();
    @observable bool isPhone = false;

    void send() {
    	if (model.email == null || model.email.isEmpty) 
        	return;
        
        
    	new Future<Empty>.microtask(() 
        {
        	HttpRequest req = new HttpRequest();
        	req.open("GET", SyagesInstance.shared.apiUrl("resetPassword?email=" + model.email), async: false);
     		addHeadersToRequest(req, requestHeaders());
        	req.send();
        	
    		if (req.response == null) throw new RequestFailureException();
    		switch(req.status)
   			{
   				case 204:
   					// TODO : lancer Toast ici
   					cancel();
   					break;
            	case 400:
            		throw new BadRequestException("Erreur ${req.status}:\n\t${req.response}");
   				case 401:
   					throw new UnauthorizedException("Erreur ${req.status}:\n\t${req.response}");
   				default:
   					throw new UnknownException("Erreur ${req.status}:\n\t${req.response}");
   			}
        	
        });
       
    }


    
    void cancel(){
    	SyagesInstance.shared.popContext();
    }

    void _sendMessage() {
    	this.$["send-popup"].show();
    }
    
    void deserializeState(Map<String, dynamic> state) {
    }
    Map<String, dynamic> serializedState() => new Map<String, dynamic>();
}

class SendModel extends Observable {
    @observable String email;
    @observable String message;
}
