import 'dart:async';
import 'dart:html';
import 'package:polymer/polymer.dart';
import '../../ContextFactory.dart';
import '../../Instance.dart';
import '../../ViewController.dart';
import '../../models/Staff.dart';

@CustomTag("syv-staff-list--aps")
class StaffListController extends ViewController {
	@observable List<Staff> staffs;

	StaffListController.created(): super.created(){
		this.staffs = null;
		this.refresh();
	}

	void refresh(){
		Future<List<Staff>> future;
		future = Staff.staffList();
		future.then((List<Staff> l) {
			this.staffs = toObservable(l);
		}).catchError((error) {
			this.staffs = null;
			errorHandler(error);
		});
	}

	void create(){
		SyagesInstance.shared.pushContext(ContextFactory.aps_staffForm());
	}

	void edit(Event event){
		String staffId = (event.currentTarget as Element).attributes["data-staff-id"];
		SyagesInstance.shared.pushContext(ContextFactory.aps_staffForm(staffId));
	}


	@override void deserializeState(Map<String, dynamic> state) {}
    @override Map<String, dynamic> serializedState() => {};
    @override bool shouldSerializeState() { return false; }
}