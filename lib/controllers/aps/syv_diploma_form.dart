import 'dart:async';
import 'dart:html';
import 'package:polymer/polymer.dart';
import '../../ContextFactory.dart';
import '../../Instance.dart';
import '../../Helpers.dart';
import '../../ViewController.dart';
import '../../models/Diploma.dart';
import '../../models/Lesson.dart';

@CustomTag("syv-diploma-form--aps")
class DiplomaFormController extends ViewController {
	@observable Diploma diploma;
	@published String diplomaId;
	@observable List periods = toObservable(new List());
	@observable List<Lesson> lessons = toObservable(new List());
	@observable bool edit;
	@observable bool dStatus = true;

	DiplomaFormController.created(): super.created(){
		this.diploma = toObservable(new Diploma.empty());
		this.periods = toObservable(this.diploma.periodList);
	}

	diplomaIdChanged(){
		if(this.diplomaId != "new"){
			this.edit = true;
			Future<Diploma> future;
			future = Diploma.diploma(diplomaId);
			future.then((diploma){
				this.diploma = toObservable(diploma);
				this.periods = toObservable(diploma.periodList);
				this.dStatus = toObservable(diploma.status == 'Active');
				diploma.lessons().then((l) => this.lessons = toObservable(l));
			}).catchError(errorHandler)
			.whenComplete(() {
					});
		}
	}
	
	dStatusChanged(){
		if(dStatus){
			this.diploma.status = 'Active';
		}
		else {
			this.diploma.status = 'Inactive';
		}
	}
	
	addRow(){
		Lesson l = new Lesson.empty();
		this.lessons.add(toObservable(l));
	}
	
	removeRow(Event event, var details, Node target){
		var hash = int.parse((event.currentTarget as Element).attributes['data-lesson']);
		this.lessons.removeWhere((l) => l.hashCode == hash);
	}

	void save(Event event){
		this.diploma.lessonList = this.lessons;
		this.diploma.periodList = this.periods;
		this.diploma.save().then((i){
			SyagesInstance.shared.replaceContext(ContextFactory.aps_diplomas());
		}).catchError(errorHandler);
	}

	void cancel(Event event){
		SyagesInstance.shared.popContext();
	}

	@override void deserializeState(Map<String, dynamic> state) {
		this.diplomaId = state["diplomaId"];
	}
	@override Map<String, dynamic> serializedState() => new Map<String, dynamic>();
	@override bool shouldSerializeState() => false;
}