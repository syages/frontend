import 'dart:async';
import 'dart:html';
import 'package:polymer/polymer.dart';
import '../../ContextFactory.dart';
import '../../Instance.dart';
import '../../ViewController.dart';
import '../../models/Teacher.dart';

@CustomTag("syv-teacher-list--aps")
class TeacherListController extends ViewController {
	@observable List<Teacher> teachers;

	TeacherListController.created(): super.created(){
		this.teachers = null;
		this.refresh();
	}

	void refresh(){
		Future<List<Teacher>> future;
		future = Teacher.teachersList();
		future.then((List<Teacher> l) {
			this.teachers = toObservable(l);
		}).catchError((error) {
			this.teachers = null;
			errorHandler(error);
		});
	}

	void create(){
		SyagesInstance.shared.pushContext(ContextFactory.aps_teacherForm());
	}

	void edit(Event event){
		String teacherId = (event.currentTarget as Element).attributes["data-teacher-id"];
		SyagesInstance.shared.pushContext(ContextFactory.aps_teacherForm(teacherId));
	}


	@override void deserializeState(Map<String, dynamic> state) {}
    @override Map<String, dynamic> serializedState() => {};
    @override bool shouldSerializeState() { return false; }
}