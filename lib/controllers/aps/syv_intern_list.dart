import 'dart:async';
import 'dart:html';
import 'package:polymer/polymer.dart';
import '../../ContextFactory.dart';
import '../../Instance.dart';
import '../../ViewController.dart';
import '../../models/Intern.dart';

@CustomTag("syv-intern-list--aps")
class InternListController extends ViewController {
	@observable List<Intern> interns;

	InternListController.created(): super.created(){
		this.interns = null;
		this.refresh();
	}

	void refresh(){
		Future<List<Intern>> future;
		future = Intern.internList();
		future.then((List<Intern> l) {
			this.interns = toObservable(l);
		}).catchError((error) {
			this.interns = null;
			errorHandler(error);
		});
	}

	void create(){
		SyagesInstance.shared.pushContext(ContextFactory.aps_internForm());
	}

	void edit(Event event){
		String internId = (event.currentTarget as Element).attributes["data-intern-id"];
		SyagesInstance.shared.pushContext(ContextFactory.aps_internForm(internId));
	}


	@override void deserializeState(Map<String, dynamic> state) {}
    @override Map<String, dynamic> serializedState() => {};
    @override bool shouldSerializeState() { return false; }
}