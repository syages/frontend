import 'dart:async';
import 'dart:html';
import 'package:polymer/polymer.dart';
import '../../ContextFactory.dart';
import '../../Instance.dart';
import '../../ViewController.dart';
import '../../models/Staff.dart';

@CustomTag("syv-staff-form--aps")
class StaffFormController extends ViewController{
	@observable Staff staff;
	@published String staffId;
	@observable bool edit;

	StaffFormController.created(): super.created(){
		this.staff = toObservable(new Staff.empty());
	}

	staffIdChanged(){
		if(this.staffId != "new"){
			edit = true;
			Future<Staff> future;
			future = Staff.staff(this.staffId);
			future.then((Staff staff){
				this.staff = toObservable(staff);
			}).catchError(errorHandler);
		}
	}

	void save(Event event){
    		this.staff.save().then((i){
    			SyagesInstance.shared.replaceContext(ContextFactory.aps_staffs());
    		}).catchError(errorHandler);
    	}

	void cancel(Event event){
		SyagesInstance.shared.popContext();
	}

	@override void deserializeState(Map<String, dynamic> state) {
		this.staffId = state["staffId"];
	}
	@override Map<String, dynamic> serializedState() => new Map<String, dynamic>();
	@override bool shouldSerializeState() => false;
}