import 'dart:async';
import 'dart:html';
import 'package:polymer/polymer.dart';
import '../../ContextFactory.dart';
import '../../Instance.dart';
import '../../ViewController.dart';
import '../../models/President.dart';

@CustomTag("syv-president-list--aps")
class PresidentListController extends ViewController {
	@observable List<President> presidents;

	PresidentListController.created(): super.created(){
		this.presidents = null;
		this.refresh();
	}

	void refresh(){
		Future<List<President>> future;
		future = President.presidentList();
		future.then((List<President> l) {
			this.presidents = toObservable(l);
		}).catchError((error) {
			this.presidents = null;
			errorHandler(error);
		});
	}

	void create(){
		SyagesInstance.shared.pushContext(ContextFactory.aps_presidentForm());
	}

	void edit(Event event){
		String presidentId = (event.currentTarget as Element).attributes["data-president-id"];
		SyagesInstance.shared.pushContext(ContextFactory.aps_presidentForm(presidentId));
	}


	@override void deserializeState(Map<String, dynamic> state) {}
    @override Map<String, dynamic> serializedState() => {};
    @override bool shouldSerializeState() { return false; }
}