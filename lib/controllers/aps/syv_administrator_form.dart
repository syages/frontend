import 'dart:async';
import 'dart:html';
import 'package:polymer/polymer.dart';
import '../../ContextFactory.dart';
import '../../Instance.dart';
import '../../ViewController.dart';
import '../../models/Administrator.dart';

@CustomTag("syv-administrator-form--aps")
class AdministratorFormController extends ViewController {
	@observable Administrator administrator;
	@published String administratorId;
	@observable bool edit;

	AdministratorFormController.created(): super.created(){
		this.administrator = toObservable(new Administrator.empty());
	}

	administratorIdChanged(){
		if(this.administratorId != "new"){
			edit = true;
			Future<Administrator> future;
			future = Administrator.administrator(this.administratorId);
			future.then((Administrator administrator){
				this.administrator = toObservable(administrator);
			}).catchError(errorHandler);
		}
	}

	void save(Event event){
    		this.administrator.save().then((i){
    			SyagesInstance.shared.replaceContext(ContextFactory.aps_administrators());
    		}).catchError(errorHandler);
    	}

	void cancel(Event event){
		SyagesInstance.shared.popContext();
	}

	@override void deserializeState(Map<String, dynamic> state) {
		this.administratorId = state["administratorId"];
	}
	@override Map<String, dynamic> serializedState() => new Map<String, dynamic>();
	@override bool shouldSerializeState() => false;
}