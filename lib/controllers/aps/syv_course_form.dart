import 'dart:async';
import 'dart:html';
import 'package:polymer/polymer.dart';
import '../../ContextFactory.dart';
import '../../Instance.dart';
import '../../ViewController.dart';
import '../../models/Course.dart';

@CustomTag('syv-course-form--aps')
class CourseFormController extends ViewController{
	@observable Course course;
	@published String courseId;
	@observable bool edit;

	CourseFormController.created(): super.created(){
		this.course = toObservable(new Course.empty());
	}

	courseIdChanged(){
		if(this.courseId != "new"){
			edit=true;
			Future<Course> future;
			future = Course.course(this.courseId);
			future.then((Course course){
				this.course = toObservable(course);
			}).catchError(errorHandler);
		}
	}

	void save(Event event){
		this.course.save().then((i){
			SyagesInstance.shared.replaceContext(ContextFactory.aps_courses());
		}).catchError(errorHandler);
	}

	void cancel(Event event){
		SyagesInstance.shared.popContext();
	}

	@override void deserializeState(Map<String, dynamic> state) {
		this.courseId = state['courseId'];
	}
	@override Map<String, dynamic> serializedState() => new Map<String, dynamic>();
	@override bool shouldSerializeState() => false;
}