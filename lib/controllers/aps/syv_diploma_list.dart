import 'dart:async';
import 'dart:html';
import 'package:polymer/polymer.dart';
import '../../ViewController.dart';
import '../../models/Diploma.dart';
import '../../Instance.dart';
import '../../ContextFactory.dart';

@CustomTag("syv-diploma-list--aps")
class DiplomaListController extends ViewController {
	@observable List<Diploma> diplomas;

	DiplomaListController.created(): super.created(){
		this.diplomas = null;
		this.refresh();
	}

	void refresh(){
		Future<List<Diploma>> future;
		future = Diploma.diplomaList();
		future.then((List<Diploma> d) {
			this.diplomas = toObservable(d);
		}).catchError(errorHandler);
	}

	void create(){
		SyagesInstance.shared.pushContext(ContextFactory.aps_diplomaForm());
	}

	void edit(Event event){
		String diplomaId = (event.currentTarget as Element).attributes["data-diploma-id"];
		SyagesInstance.shared.pushContext(ContextFactory.aps_diplomaForm(diplomaId));
	}

	@override void deserializeState(Map<String, dynamic> state) {}
    @override Map<String, dynamic> serializedState() => {};
    @override bool shouldSerializeState() { return false; }
}