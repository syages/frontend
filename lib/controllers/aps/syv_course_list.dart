import 'dart:async';
import 'dart:html';
import 'package:polymer/polymer.dart';
import '../../ContextFactory.dart';
import '../../Instance.dart';
import '../../ViewController.dart';
import '../../models/Course.dart';

@CustomTag('syv-course-list--aps')
class CourseListController extends ViewController{
	@observable List<Course> courses;

	CourseListController.created():super.created(){
		this.courses = null;
		this.refresh();
	}

	void refresh(){
		Future<List<Course>> future;
		future = Course.courseList();
		future.then((List<Course> c){
			this.courses = toObservable(c);
		}).catchError(errorHandler);
	}

	void create(){
		SyagesInstance.shared.pushContext(ContextFactory.aps_courseForm());
	}

	void edit(Event event){
		String courseId = (event.currentTarget as Element).attributes["data-course-id"];
		SyagesInstance.shared.pushContext(ContextFactory.aps_courseForm(courseId));
	}

	@override void deserializeState(Map<String, dynamic> state) {
	}
	@override Map<String, dynamic> serializedState() => {
	};
	@override bool shouldSerializeState() => false;
}