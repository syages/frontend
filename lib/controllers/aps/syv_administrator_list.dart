import 'dart:async';
import 'dart:html';
import 'package:polymer/polymer.dart';
import '../../models/Administrator.dart';
import '../../ContextFactory.dart';
import '../../Instance.dart';
import '../../ViewController.dart';

@CustomTag("syv-administrator-list--aps")
class AdministratorListController extends ViewController {
	@observable List<Administrator> administrators;

	AdministratorListController.created(): super.created(){
		this.administrators = null;
		this.refresh();
	}

	void refresh(){
		Future<List<Administrator>> future;
		future = Administrator.administratorList();
		future.then((List<Administrator> l) {
			this.administrators = toObservable(l);
		}).catchError(errorHandler);
	}

	void create(){
		SyagesInstance.shared.pushContext(ContextFactory.aps_administratorForm());
	}

	void edit(Event event){
		String administratorId = (event.currentTarget as Element).attributes["data-administrator-id"];
		SyagesInstance.shared.pushContext(ContextFactory.aps_administratorForm(administratorId));
	}


	@override void deserializeState(Map<String, dynamic> state) {}
    @override Map<String, dynamic> serializedState() => {};
    @override bool shouldSerializeState() { return false; }
}