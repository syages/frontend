import 'dart:async';
import 'dart:html';
import 'package:polymer/polymer.dart';
import '../../ViewController.dart';
import '../../models/Intern.dart';
import '../../Instance.dart';
import '../../Helpers.dart';
import '../../ContextFactory.dart';
import '../../models/Lesson.dart';
import '../../models/Diploma.dart';
import '../../models/FollowedDiploma.dart';

@CustomTag("syv-intern-form--aps")
class InternFormController extends ViewController{
	@observable Intern intern;
	@observable DateTime birthDate;
	@published String internId;
	@observable bool edit;
	@observable List<FollowedDiploma> internFDs = toObservable(new List());
	
	InternFormController.created(): super.created(){
    		this.intern = toObservable(new Intern.empty());
    }

	internIdChanged(){
		if(this.internId != "new"){
			edit = true;
			Future<Intern> future;
			future = Intern.intern(this.internId);
			future.then((Intern intern){
				this.intern = toObservable(intern);
				this.internFDs = toObservable(intern.followedDiplomas == null ? new List() : intern.followedDiplomas);
				this.birthDate = toObservable(this.intern.dateOfBirth);
			}).catchError(errorHandler);
		}
	}
	
	save(){
		this.intern.followedDiplomas = this.internFDs;
		this.intern.dateOfBirth = this.birthDate;
		this.intern.save().then((Intern i){
			SyagesInstance.shared.replaceContext(ContextFactory.aps_interns());
		}).catchError(errorHandler);
	}
	
	addRow(){
		this.internFDs.add(toObservable(new FollowedDiploma.empty()));
	}
	
	cancel(){
		SyagesInstance.shared.popContext();
	}
	
	delete(){
		this.intern.delete().then((t){
			SyagesInstance.shared.replaceContext(ContextFactory.aps_interns());
		}).catchError(errorHandler);
	}
	
	
	
	@override void deserializeState(Map<String, dynamic> state) {
    		this.internId = state["internId"];
    }
   	@override Map<String, dynamic> serializedState() => new Map<String, dynamic>();
   	@override bool shouldSerializeState() => false;
}
