import 'dart:async';
import 'dart:html';
import 'package:polymer/polymer.dart';
import '../../ContextFactory.dart';
import '../../Instance.dart';
import '../../ViewController.dart';
import '../../models/Domain.dart';

@CustomTag("syv-domain-form--aps")
class DomainFormController extends ViewController{
	@observable Domain domain;
	@published String domainId;
	@observable bool edit;

	DomainFormController.created(): super.created(){
		this.domain = toObservable(new Domain.empty());
	}

	domainIdChanged(){
		if(this.domainId != "new"){
			edit = true;
			Future<Domain> future;
			future = Domain.domain(this.domainId);
			future.then((Domain domain){
				this.domain = toObservable(domain);
			}).catchError(errorHandler);
		}
	}

	void save(Event event){
    		this.domain.save().then((i){
    			SyagesInstance.shared.replaceContext(ContextFactory.aps_domains());
    		}).catchError(errorHandler);
    	}

	void cancel(Event event){
		SyagesInstance.shared.popContext();
	}

	@override void deserializeState(Map<String, dynamic> state) {
		this.domainId = state["domainId"];
	}
	@override Map<String, dynamic> serializedState() => new Map<String, dynamic>();
	@override bool shouldSerializeState() => false;
}