import 'dart:async';
import 'dart:html';
import 'package:polymer/polymer.dart';
import '../../ContextFactory.dart';
import '../../Instance.dart';
import '../../ViewController.dart';
import '../../models/Domain.dart';

@CustomTag("syv-domain-list--aps")
class DomainListController extends ViewController {
	@observable List<Domain> domains;

	DomainListController.created(): super.created(){
		this.domains = null;
		this.refresh();
	}

	void refresh(){
		Future<List<Domain>> future;
		future = Domain.domainList();
		future.then((List<Domain> l) {
			this.domains = toObservable(l);
		}).catchError(errorHandler);
	}

	void create(){
		SyagesInstance.shared.pushContext(ContextFactory.aps_domainForm());
	}

	void edit(Event event){
		String domainId = (event.currentTarget as Element).attributes["data-domain-id"];
		SyagesInstance.shared.pushContext(ContextFactory.aps_domainForm(domainId));
	}


	@override void deserializeState(Map<String, dynamic> state) {}
    @override Map<String, dynamic> serializedState() => {};
    @override bool shouldSerializeState() { return false; }
}