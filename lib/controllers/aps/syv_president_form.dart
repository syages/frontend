import 'dart:async';
import 'dart:html';
import 'package:polymer/polymer.dart';
import '../../ContextFactory.dart';
import '../../Instance.dart';
import '../../ViewController.dart';
import '../../models/President.dart';

@CustomTag("syv-president-form--aps")
class PresidentFormController extends ViewController{
	@observable President president;
	@published String presidentId;
	@observable bool edit;

	PresidentFormController.created(): super.created(){
		this.president = toObservable(new President.empty());
	}

	presidentIdChanged(){
		if(this.presidentId != "new"){
			edit = true;
			Future<President> future;
			future = President.president(this.presidentId);
			future.then((President president){
				this.president = toObservable(president);
			}).catchError(errorHandler);
		}
	}

	void save(Event event){
    		this.president.save().then((i){
    			SyagesInstance.shared.replaceContext(ContextFactory.aps_presidents());
    		}).catchError(errorHandler);
    	}

	void cancel(Event event){
		SyagesInstance.shared.popContext();
	}

	@override void deserializeState(Map<String, dynamic> state) {
		this.presidentId = state["presidentId"];
	}
	@override Map<String, dynamic> serializedState() => new Map<String, dynamic>();
	@override bool shouldSerializeState() => false;
}