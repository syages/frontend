import 'dart:async';
import 'dart:html';
import 'package:polymer/polymer.dart';
import '../../ContextFactory.dart';
import '../../Instance.dart';
import '../../ViewController.dart';
import '../../models/Teacher.dart';

@CustomTag("syv-teacher-form--aps")
class TeacherFormController extends ViewController{
	@observable Teacher teacher;
	@published String teacherId;
	@observable bool edit;

	TeacherFormController.created(): super.created(){
		this.teacher = toObservable(new Teacher.empty());
	}

	teacherIdChanged(){
		if(this.teacherId != "new"){
			edit = true;
			Future<Teacher> future;
			future = Teacher.teacher(this.teacherId);
			future.then((Teacher teacher){
				this.teacher = toObservable(teacher);
			}).catchError(errorHandler);
		}
	}

	void save(Event event){
    		this.teacher.save().then((i){
    			SyagesInstance.shared.replaceContext(ContextFactory.aps_teachers());
    		}).catchError(errorHandler);
    	}

	void cancel(Event event){
		SyagesInstance.shared.popContext();
	}

	@override void deserializeState(Map<String, dynamic> state) {
		this.teacherId = state["teacherId"];
	}
	@override Map<String, dynamic> serializedState() => new Map<String, dynamic>();
	@override bool shouldSerializeState() => false;
}