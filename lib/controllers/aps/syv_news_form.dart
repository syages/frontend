import 'dart:html';
import 'package:polymer/polymer.dart';
import '../../ContextFactory.dart';
import '../../Instance.dart';
import '../../ViewController.dart';
import '../../models/Information.dart';

@CustomTag('syv-news-form--aps')
class NewsFormController extends ViewController{
	@observable Information information;
	@published String informationId;
	@observable bool edit;
	NewsFormController.created(): super.created(){
		this.information = toObservable(new Information.empty());
	}

	informationIdChanged() {
		if(this.informationId != "new"){
			this.edit = true;
			Information.information(this.informationId).then((i) {
            			this.information = toObservable(i);
			}).catchError(errorHandler);
		}
		else{
			this.information = toObservable(new Information.empty());
		}
	}

	void save(Event event){
		this.information.save().then((i){
			SyagesInstance.shared.replaceContext(ContextFactory.newsFeed());
		}).catchError(errorHandler);

	}

	void cancel(Event event){
		SyagesInstance.shared.popContext();
	}


	void deserializeState(Map<String, dynamic> state) {
		this.informationId = state["informationId"];
	}
    Map<String, dynamic> serializedState() => {};
    bool shouldSerializeState() { return true; }
}
