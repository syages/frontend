import 'dart:html';
import 'dart:async';
import 'package:polymer/polymer.dart';
import '../../Helpers.dart';
import '../../Instance.dart';
import '../../ViewController.dart';
import '../../models/Lecture.dart';
import '../../models/Intern.dart';
import '../../models/Lesson.dart';
import '../../models/NotedAbsence.dart';
import '../../ContextFactory.dart';

@CustomTag("syv-lecture-detail--apst")
class LectureDetailController extends ViewController{

	@published String lessonId;
	@published String lessonName;
	@published String lectureId;
	@observable Lecture lecture;
	@observable List<Intern> interns;
	@observable List<Map<String, dynamic>> absences = toObservable(new List());

	LectureDetailController.created(): super.created(){
		this.lecture = toObservable(new Lecture.empty());
	}
	
	init(){
		Future.wait([Lesson.lesson(this.lessonId), Lecture.lecture(this.lectureId)]).then((responses)
				{
					this.lecture = toObservable(responses[1]);
					Future.wait([responses[0].interns(), responses[1].absencesList()]).then((responses2)
						{
							this.interns = toObservable(responses2[0]);
							this.lecture.notedAbsences = toObservable(responses2[1]);
							for (Intern intern in this.interns) {
								var na = null;
								try{
									na = this.lecture.notedAbsences.singleWhere((n) => n.intern.user.id == intern.id);
								}catch (exception){
								}
								this.absences.add(toObservable({'intern': intern, 'notedAbsence': na != null ? new NotedAbsence.from(na): null}));
							}
						}).catchError(errorHandler);
				}).catchError(errorHandler);
	}
	
	absence(Event event){
		var internId = (event.currentTarget as Element).attributes["data-intern-id"];
		if(this.absences.any((t) => t['intern'].id == internId && t['notedAbsence'] is NotedAbsence)){
			this.absences.singleWhere((t) => t['intern'].id == internId)['notedAbsence'] = null;	
		}
		else if(this.absences.any((t) => t['intern'] == internId && t['notedAbsence'] == "new")){
			this.absences.singleWhere((t) => t == internId)['notedAbsence'] = null;
		}
		else{
			if(this.lecture.notedAbsences.any((t) => t.intern.user.id == internId)){
				this.absences.singleWhere((t) => t['intern'].id == internId)['notedAbsence'] = new NotedAbsence.from(this.lecture.notedAbsences.singleWhere((t) => t.intern.user.id == internId));
			}
			else{
				this.absences.singleWhere((t) => t['intern'].id == internId)['notedAbsence'] = "new";
			}
		}
	}
		
	save(){
    	// Transforme this.absence en deux liste:
    	this.lecture.notedAbsences = [];
    	for (var a in this.absences){
    		if (a['notedAbsence'] == "new"){
    			this.lecture.newNotedAbsences.add(a['intern'].id);
    		}
    		else if (a['notedAbsence'] != null){
    			this.lecture.notedAbsences.add(a['notedAbsence']);
    		}
    	}
    	this.lecture.addAbsences().then((t) => SyagesInstance.shared.popContext()).catchError(errorHandler);
    }
	
	cancel(){
		SyagesInstance.shared.popContext();
	}
	
	edit(){
		SyagesInstance.shared.pushContext(ContextFactory.apst_lectureForm(this.lessonId, this.lessonName, this.lecture.id));
	}

	@override void deserializeState(Map<String, dynamic> state) {
		this.lessonId = state["lessonId"];
		this.lectureId = state["lectureId"];
		this.lessonName = state["lessonName"];
		init();
	}
	@override Map<String, dynamic> serializedState() => new Map<String, dynamic>();
	@override bool shouldSerializeState() => false;
}