import 'dart:async';
import 'package:polymer/polymer.dart';
import '../../Helpers.dart';
import '../../Instance.dart';
import '../../ContextFactory.dart';
import '../../ViewController.dart';
import '../../models/Evaluation.dart';
import '../../models/Mark.dart';
import '../../models/Intern.dart';
import '../../models/Lesson.dart';

@CustomTag("syv-evaluation-detail--apst")
class EvaluationDetailController extends ViewController{
	@published String lessonId;
	@published String lessonName;
	@observable List<Intern> interns;
	@published String evaluationId;
	@observable Evaluation evaluation;
	@observable List<Mark> marks = toObservable(new List<Mark>());

	EvaluationDetailController.created(): super.created(){
		this.evaluation = toObservable(new Evaluation.empty());
	}

//	evaluationIdChanged(){
//		Evaluation.evaluation(evaluationId).then((Evaluation e){
//			this.evaluation= toObservable(e);
//			this.evaluation.marks().then((List<Mark> marks){
//				if (marks.isEmpty){
//					for(Intern i in this.interns){
//						this.marks.add(toObservable(new Mark.fromIntern(i)));
//						this.marks = toObservable(this.marks);
//					}			
//				}else{
//					this.marks = toObservable(marks);
//				}
//				
//			});
//		}).catchError(errorHandler);
//	}
//	
//	lessonIdChanged(){
//		Lesson.lesson(this.lessonId).then((Lesson l){
//			l.interns().then((List<Intern> interns){
//				this.interns = toObservable(interns);
//			}).catchError(errorHandler);
//		}).catchError(errorHandler);
//	}
	
	void init(){
		Future.wait([Lesson.lesson(this.lessonId), Evaluation.evaluation(this.evaluationId)]).then(
				(responses) { 
			this.evaluation = toObservable(responses[1]); 
			Future.wait([responses[0].interns(), responses[1].marks()]).then(
						(responses2){
			if(responses2[1].isEmpty){
				for (Intern i in responses2[0]){
					this.marks.add(toObservable(new Mark.fromIntern(i)));
				}
			}
			else{
				this.marks = toObservable(responses2[1]);
			}
		});});
	}

	cancel(){
		SyagesInstance.shared.popContext();
	}

	save(){
		if (this.marks.isNotEmpty && this.marks.elementAt(0).id != null){
			this.evaluation.editMarks(this.marks).then((List<Mark> m){
				cancel();
    		}).catchError(errorHandler);
		}
		else {
			this.evaluation.addMarks(this.marks).then((List<Mark> m){
				cancel();
    		}).catchError(errorHandler);
		}
		
	}

	edit(){
		SyagesInstance.shared.pushContext(ContextFactory.apst_evaluationForm(this.lessonId, this.lessonName, this.evaluationId));
	}

	@override void deserializeState(Map<String, dynamic> state) {
		this.lessonId = state["lessonId"];
		this.evaluationId = state["evaluationId"];
		this.lessonName = state["lessonName"];
		init();
	}
	@override Map<String, dynamic> serializedState() => new Map<String, dynamic>();
	@override bool shouldSerializeState() => false;
}
