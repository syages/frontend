import 'package:polymer/polymer.dart';
import '../../Instance.dart';
import '../../ViewController.dart';
import '../../models/Lecture.dart';
import '../../models/Intern.dart';
import '../../models/Lesson.dart';
import '../../models/NotedAbsence.dart';

@CustomTag("syv-lecture-form--apst")
class LectureFormController extends ViewController{

	@published String lessonId;
	@published String lessonName;
	@published String lectureId;
	@observable Lecture lecture;
	@observable DateTime date;
	@observable DateTime time;
	@observable Duration durationTime;
	
	@observable List<Intern> interns;

	LectureFormController.created(): super.created(){
		this.lecture = toObservable(new Lecture.empty());
	}

	lectureIdChanged(){
		if(lectureId != "new"){
			Lecture.lecture(lectureId).then((Lecture l){
				this.lecture = toObservable(l);
				this.date = toObservable(this.lecture.date);
				this.time = toObservable(this.lecture.date);
				this.lecture.absencesList().then((List<NotedAbsence> nas){
					this.lecture.notedAbsences = toObservable(nas);
				});
			}).catchError(errorHandler);
		}
		this.lecture.lessonId = this.lessonId;
	}

	save(){
		// remet en place date et time
		this.lecture.date = new DateTime(this.date.year, this.date.month, this.date.day, this.time.hour, this.time.minute);
		this.lecture.save().then((Lecture l){
			SyagesInstance.shared.popContext();
		}).catchError((err){
			errorHandler(err);
		});
	}
	
// A UTILISER POUR FAIRE LES ABSENCES !
	lessonIdChanged(){
		Lesson.lesson(this.lessonId).then((Lesson l){
			l.interns().then((List<Intern> interns){
				this.interns = toObservable(interns);
			});
		});
	}
	
	cancel(){
		SyagesInstance.shared.popContext();
	}
	
	delete(){
		this.lecture.delete().then((res) => SyagesInstance.shared.popContext());
	}

	@override void deserializeState(Map<String, dynamic> state) {
		this.lessonId = state["lessonId"];
		this.lectureId = state["lectureId"];
		this.lessonName = state["lessonName"];
	}
	@override Map<String, dynamic> serializedState() => new Map<String, dynamic>();
	@override bool shouldSerializeState() => false;
}