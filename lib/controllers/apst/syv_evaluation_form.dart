import 'package:polymer/polymer.dart';
import '../../Instance.dart';
import '../../ContextFactory.dart';
import '../../ViewController.dart';
import '../../models/Evaluation.dart';

@CustomTag("syv-evaluation-form--apst")
class EvaluationFormController extends ViewController{
	@published String lessonId;
	@published String lessonName;
	@published String evaluationId;
	@observable Evaluation evaluation;
	@observable String type = 'Continue';
	@observable String visibility = 'true';

	EvaluationFormController.created(): super.created(){
		this.evaluation = toObservable(new Evaluation.empty());
	}

	evaluationIdChanged(){
		if(this.evaluationId != "new"){
			Evaluation.evaluation(evaluationId).then((Evaluation e){
				this.evaluation= toObservable(e);
				this.type = this.evaluation.type;
				this.visibility = this.evaluation.visible ? "true" : "false";
			}).catchError(errorHandler);
		}
		else{
			this.evaluation.lessonId = this.lessonId;
		}
	}
	
	void typeChanged(){
		if (this.type == 'Final'){
			this.visibility = 'true';
		}
	}
	
	cancel(){
		SyagesInstance.shared.popContext();
	}

	save(){
		this.evaluation.visible = this.visibility == 'true';
		this.evaluation.type = Evaluation.TYPE.singleWhere((t) => t == this.type);
		this.evaluation.save().then((Evaluation e){
			SyagesInstance.shared.replaceContext(ContextFactory.t_classroom(this.lessonId));
		}).catchError(errorHandler);
	}


	@override void deserializeState(Map<String, dynamic> state) {
		this.lessonId = state["lessonId"];
		this.evaluationId = state["evaluationId"];
		this.lessonName = state["lessonName"];
	}
	@override Map<String, dynamic> serializedState() => new Map<String, dynamic>();
	@override bool shouldSerializeState() => false;
}
