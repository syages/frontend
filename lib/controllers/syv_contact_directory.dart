import 'package:polymer/polymer.dart';
import 'dart:async';
import '../ViewController.dart';
import '../models/Intern.dart';
import '../models/User.dart';

@CustomTag("syv-contact-directory")
class ContactDirectoryController extends ViewController {
	@observable String filter; // 0=all, 1=Administrator, 2=Intern, 3=President, 4=Staff, 5=Teacher
	
	@observable List<User> filtredUsers; // Liste des users concerné par le filtre
	List<User> _users; //Liste de tout les Users
	
	ContactDirectoryController.created() : super.created()
	{
		this.filter = "0";
		this._users = null;
		this.filtredUsers = null;

	}

	void attached()
	{
		this.refresh();
	}
	
	void refresh() {
		String filterStr = "";
			 if(filter == "1") filterStr = User.KINDS[User.KIND_ADMINISTRATOR];
		else if(filter == "2") filterStr = User.KINDS[User.KIND_INTERN];
		else if(filter == "3") filterStr = User.KINDS[User.KIND_PRESIDENT];
		else if(filter == "4") filterStr = User.KINDS[User.KIND_STAFF];
		else if(filter == "5") filterStr = User.KINDS[User.KIND_TEACHER];
		
			 
		if(loggedUser.kind==User.KINDS[User.KIND_INTERN])
		{	Future.
				wait([Intern.userForContactDirectory()]).
				then((resList)
				{
					List<List<User>> list = resList[0];
					if(filterStr==User.KINDS[User.KIND_INTERN])
						_users = list[0];
					else if(filterStr==User.KINDS[User.KIND_TEACHER])
						_users = list[1];
					else
					{	_users = new List<User>();
						list[0].forEach((user){ _users.add(user); });
						list[1].forEach((user){ _users.add(user); });
					}
					this.filtredUsers = toObservable(_users);
				});
		}
		else
		{
			User.usersList().then((List<User> l) {
				this._users = l;
			
				
				List<User> filtred = new List<User>();
	    		if(filterStr=="") 
	    			filtredUsers = toObservable(this._users);
	    		else
	    		{	_users.forEach((user)
		    		{	//test du User.Kind correspond a celui du filtre
		    			if(user.kind==filterStr)
		    				filtred.add(user);
		    		});
		    		this.filtredUsers = toObservable(filtred);
	    		}
			}).catchError((error) {
				this._users = null;
				errorHandler(error);
			});
		}
	}

	@override void deserializeState(Map<String, dynamic> state) {}
	@override Map<String, dynamic> serializedState() => new Map<String, dynamic>();
	@override bool shouldSerializeState() => false;
}
