import 'dart:html';
import 'dart:convert';
import 'dart:async';

import 'package:polymer/polymer.dart';

import '../../Exceptions.dart';
import '../../Instance.dart';
import '../../Helpers.dart';
import '../../ViewController.dart';
import '../../models/Diploma.dart';
import '../../models/Mark.dart';
import '../../models/Period.dart';
import '../../models/Lesson.dart';

@CustomTag("syv-marks--i")
class InternMarks extends ViewController{
	@observable MarksModel internMarks;
	@observable int currentPeriod = 0;
	
	InternMarks.created(): super.created(){
		init();
	}
		
	/*
	 * Requete pour obtenir les notes de l'intern connecté 
	 * 
	 */
	init(){
		Future<MarksModel> future = new Future.microtask((){
			HttpRequest req = new HttpRequest();
			String id = this.loggedUser.id;
			req.open('GET', SyagesInstance.shared.apiUrl("/interns/$id/marks"), async: false);
			addHeadersToRequest(req, requestHeaders(null));
			req.send();
			if (req.response == null) throw new RequestFailureException();

    		switch(req.status)
   			{
   				case 200:
   					return new MarksModel.fromMap(JSON.decode(req.response));
            	case 400:
            		throw new BadRequestException("Erreur ${req.status}:\n\t${req.response}");
   				case 401:
   					throw new UnauthorizedException("Erreur ${req.status}:\n\t${req.response}");
   				default:
   					throw new UnknownException("Erreur ${req.status}:\n\t${req.response}");
   			}
		});
		future.then((m) {
			this.internMarks = toObservable(m);
			for(CustomPeriod p in this.internMarks.periods){
				if(p.period.startDate.isBefore(new DateTime.now()) && p.period.endDate.isAfter(new DateTime.now())){
					this.currentPeriod = p.period.ordre - 1;
				}
			}
		}).catchError(errorHandler);
		
	}
	
	
	@override void deserializeState(Map<String, dynamic> state){}
	@override Map<String, dynamic> serializedState() => new Map<String, dynamic>();
	@override bool shouldSerializeState() => false;
}

/*
 * Le format du Json est:
 * - diploma : info sur la formation
 * - average : moyenne générale
 * - periods: liste contenant les trimestres de la formation 
 * 		- period: info sur le trimestre
 * 		- average: moyenne de la période
 * 		- marks: liste contenant les leçons et leurs evaluations
 * 			- lesson: info sur la leçon
 * 			- evaluations: liste des evaluations
 * 				- mark : objet Mark
 */

class MarksModel extends Observable{
	@observable Diploma diploma;
	@observable double average;
	@observable List<CustomPeriod> periods;
	
	MarksModel.fromMap(Map<String, dynamic> json){
		this.diploma = Diploma.createFromJson(json['diploma'] as Map);
		this.average = json['average'];
		this.periods = new List();
		(json['periods'] as List).forEach((p){
			this.periods.add(new CustomPeriod.fromMap(p as Map));
		});
	}
}

class CustomPeriod extends Observable{
	@observable Period period;
	@observable double average;
	@observable List<CustomMark> marks;
	
	CustomPeriod.fromMap(Map<String, dynamic> json){
		this.period = new Period.fromMap(json['period'] as Map);
		this.average = json['average'];
		this.marks = new List();
		(json['marks'] as List).forEach((m){
			this.marks.add(new CustomMark.fromMap(m as Map));
		});
	}
}

class CustomMark extends Observable{
	@observable Lesson lesson;
	@observable double average;
	@observable List<Mark> marks;
	
	CustomMark.fromMap(Map<String, dynamic> json){
		this.lesson = new Lesson.fromMap(json['lesson'] as Map);
		this.average = json['average'];
		this.marks = new List();
		(json['evaluations'] as List).forEach((m){
			this.marks.add(new Mark.fromMap(m as Map));
		});
	}
}