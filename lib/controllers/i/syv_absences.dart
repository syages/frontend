import 'dart:async';
import 'dart:html';
import 'package:polymer/polymer.dart';
import 'package:core_elements/core_menu.dart';
import '../../ViewController.dart';
import '../../models/DeclaredAbsence.dart';
import '../../models/NotedAbsence.dart';
import '../../models/User.dart';
import '../../Instance.dart';
import '../../ContextFactory.dart';
import '../../Helpers.dart';


@CustomTag('syv-absences--i')
class InternAbsencesController extends ViewController{
	@observable DeclaredAbsence newAbsence;
	@observable List<DeclaredAbsence> declaredAbsences;
	@observable List<NotedAbsence> notedAbsences;
	InternAbsencesController.created(): super.created(){
		this.newAbsence = toObservable(new DeclaredAbsence.empty());
		this.newAbsence.reason = "Death";
		this.notedAbsences = toObservable(new List());
		this.init();
	}
	
	init(){
		Future.wait([User.meNotedAbsences(), User.meDeclaredAbsences()]).then((res){
			this.declaredAbsences = toObservable(res[1]);
			this.notedAbsences = toObservable(res[0]);
		}).catchError(errorHandler);
	}
	
	justify(Event event){
		var absId = (event.currentTarget as Element).attributes['data-na-id'];
		NotedAbsence na = this.notedAbsences.singleWhere((n) => n.id == absId);
		this.newAbsence = toObservable(new DeclaredAbsence.fromNotedAbsence(na));
	}
	
	print(Event event){
		var absId = (event.target as Element).attributes['data-declaredabsences-id'];
		// request pdf for absId ( /pdf/declaredAbsence/id ?)
	}
	
	reasonSelected(Event event){
		warn((event.target as CoreMenu).selected);
	}
	
	send(){
		this.newAbsence.save().then((res) => SyagesInstance.shared.replaceContext(ContextFactory.i_absences()));
	}
	
	
	@override Map<String, dynamic> serializedState() => new Map<String, dynamic>();
	@override bool shouldSerializeState() => false;
	@override void deserializeState(Map<String, dynamic> state) {}
}