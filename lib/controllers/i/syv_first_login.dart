import 'package:polymer/polymer.dart';
import '../../ContextFactory.dart';
import '../../Instance.dart';
import '../../ViewController.dart';
import '../../models/Intern.dart';
import '../../Helpers.dart';

@CustomTag("syv-first-login--i")
class FirstLoginController extends ViewController{
	 @observable FirstLoginModel model = toObservable(new FirstLoginModel());
	 @observable String activity = "true";
	 @observable DateTime today = new DateTime.now();

	FirstLoginController.created(): super.created(){
	}

	@override void deserializeState(Map<String, dynamic> state) {

	}
	
	activityChanged(){
		this.model.worked = this.activity == "true";
	}

	void validate() {
		Intern.saveInfos(this.loggedUser.id, this.model.lastSchool, this.model.professionalProjet, new DateTime(this.model.lastSchoolYear), this.model.worked, this.model.job).then((res){			
			SyagesInstance.shared.replaceContext(ContextFactory.dashboard());
		}).catchError(errorHandler);
	}

	@override Map<String, dynamic> serializedState() => new Map<String, dynamic>();
	@override bool shouldSerializeState() => false;
}

class FirstLoginModel extends Observable {
	@observable String lastSchool;
    @observable String professionalProjet;
    @observable int lastSchoolYear = new DateTime.now().year;
    @observable bool worked;
    @observable String job;
}
