import 'package:polymer/polymer.dart';
import '../ContextFactory.dart';
import '../Instance.dart';
import '../ViewController.dart';

@CustomTag("context-tester")
class ContextTesterController extends ViewController {
	ContextTesterController.created() : super.created() { initVars(); }
    void initVars() {
    	this.model = new Map<String,String>();
    }

	@observable Map<String, String> model;
	@observable int count;

    void pop() {
    	new SyagesInstance().popContext();
    }
    void push() {
    	new SyagesInstance().pushContext(ContextFactory.contextTester());
    }
    void replace() {
    	new SyagesInstance().replaceContext(ContextFactory.contextTester());
    }

    void deserializeState(Map<String, dynamic> state) {
    	initVars();
    	if (state == null) return;
    	if (state.containsKey("model") == false) return;
    	this.model = state["model"];
    }
    Map<String, dynamic> serializedState() => {"model": this.model, "count": this.count} as Map<String, dynamic>;
}