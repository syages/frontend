import 'package:polymer/polymer.dart';
import 'dart:async';
import '\$Cache.dart';
import 'ExprFilters.dart';
import 'Instance.dart';
import 'SerializableElement.dart';
import 'Session.dart';
import 'models/User.dart';

abstract class ViewController extends SerializableElement {
	ViewController.created() : super.created()
	{
		if (SyagesSession.sharedInstance.isConnected)
		{
			loggedUser = $Cache.sharedInstance.user;
		}
		SyagesSession.sharedInstance.onLogin.listen((session)
		{
			loggedUser = $Cache.sharedInstance.user;
		});
	}

	@observable User loggedUser;

	Function formatDate = filters.date;
	Function formatDateTime = filters.datetime;
	Function formatTime = filters.time;
	Function htmlEntities = filters.htmlentities;
	Function formatAverage = filters.average;
	final asInteger = new StringToInt();
	final asDouble = new StringToDouble();

	attached() {
		super.attached();
		SyagesInstance.shared.applicationController.controller = this;
	}
}