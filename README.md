# Syages Front-End

Ce repository contient le code source du front-end de l'application web Syages.  
Le front-end repose sur les nouvelles technologies du web: [CSS 3](http://www.w3.org/TR/CSS/#css3), [Dart](https://www.dartlang.org/), [HTML 5](http://www.w3.org/TR/html5/), [Polymer](https://www.polymer-project.org/), [Shadow DOM](http://www.w3.org/TR/shadow-dom/) et [webcomponents](http://webcomponents.org/).

## Best pratices et conventions d'écriture
---

De multiples personnes travaillent sur le projet. Afin de garder un code simple a lire et à maintenir, l'équipe de développement à pris le soin de se conformer aux règles suivantes.

#### Dart

* Les noms de fichier ne doivent contenir que des caractères alphanumériques minuscules ou underscores (azAZ09_).
* Les noms de fichier des controlleurs Polymer doivent respecter le nom de la balise choisi.
* Les noms de classes doivent être __capitalisés__ et représenter la vocation du controlleur de la manière la plus explicite possible.  
ex: `SyagesRouter`, `MessageController`

---
#### HTML <small>&</small> CSS

* Les noms de fichier ne doivent contenir que des caractères alphanumériques minuscules ou underscores (az09_).
* Les _attributs_ d'une balises doivent être __triés par ordre alphabétique__. ex: `<link href='main.css' rel='stylesheet'>`
* Les _valeurs des attributs_ doivent être __entre simple quotes__. ex: `align='center'`
* Les _noms de classes_, _id_ et _variables bindées_ doivent être formattés en camelCase. ex: `navBarController`

---
## Bugs connus:

- La balise `mark-down` ne support pas le `<content>`, il faut utiliser l'attribut `text`.
- Le préremplissage des balises `chart-*` en HTML n'est pas fonctionnel. Il faut utiliser du data-binding avec des JsArray/JsMap/JsObject. 

---

## État du développement:

|Fonctionalité|UserKinds|Responsable(s)|M|C|V|
|:---|:---:|:---:|---:|---:|---:|
| Context | — | Aurelien Bidon | — | ![ok] | — |
| Instance | — | Aurelien Bidon | — | ![ok] | — |
| Router | — | Aurelien Bidon | — | ![ok] | — |
| SerializableElement | — | Aurelien Bidon | — | ![ok] | — |
| Session | — | Jeremy Pichon / Romain Talleu | — | ![ok] | — |
| Stack | — | Aurelien Bidon | — | ![ok] | — |
| ViewController | — | Aurelien Bidon | — | ![ok] | — |
||
| syages-app | _tous_ | Aurelien Bidon | ![ok] | ![ok] | ![ok] |
| syd-error-reporter | _tous_ | Aurelien Bidon | ![ok] | ![ok] | ![ok] |
| sye-fullbleed-spinner | _tous_ | Aurelien Bidon | — | ![ok] | ![ok] |
| sye-navbar | _tous_ | Aurelien Bidon / Jeremy Pichon / Romain Talleu | ![p] | ![p] | ![p] |
| syv-error404 | _tous_ | Aurelien Bidon | — | ![ok] | ![ok] |
| syv-login | _tous_ | Aurelien Bidon | ![ok] | ![ok] | ![ok] |
||
| syv-dashboard-admin | A | _non applicable_ | ![wip] | ![wip] | ![wip] |
| syv-dashboard-intern | I | _non applicable_ | ![ni] | ![ni] | ![ni] |
| syv-dashboard-president | P | _non applicable_ | ![ni] | ![ni] | ![ni] |
| syv-dashboard-staff | S | _non applicable_ | ![ni] | ![ni] | ![ni] |
| syv-dashboard-teacher | T | _non applicable_ | ![ni] | ![ni] | ![ni] |
||
| syv-profile-admin | A | _non applicable_ | ![p] | ![p] | ![p] |
| syv-profile-intern | I | _non applicable_ | ![p] | ![p] | ![p] |
| syv-profile-president | P | _non applicable_ | ![p] | ![p] | ![p] |
| syv-profile-staff | S | _non applicable_ | ![p] | ![p] | ![p] |
| syv-profile-teacher | T | _non applicable_ | ![p] | ![p] | ![p] |
||

* UserKinds:
	* __A__dministratives: les administrateurs du système.
	* __I__nterns: les stagiaires candidats à l'obtention du DAEU
	* __P__resident: les responsables de la formation DAEU
	* __S__taff: le personnel administratif
	* __T__eachers: les enseignants

[ok]: https://raw.githubusercontent.com/twitter/twemoji/gh-pages/16x16/2705.png
[ni]: https://raw.githubusercontent.com/twitter/twemoji/gh-pages/16x16/274c.png
[p]: https://raw.githubusercontent.com/twitter/twemoji/gh-pages/16x16/1f554.png
[wip]: https://raw.githubusercontent.com/twitter/twemoji/gh-pages/16x16/1f6a7.png